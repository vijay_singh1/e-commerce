<?php

namespace App\Repositories;

use App\Model\Orders;

class OrderRepository
{
    protected $order_model;

    public function __construct(Orders $order_model) {
        $this->order_model = $order_model;
    }

    public function ordersList($data) {
        return $this->order_model::select('*')->where('transaction_type','customer-order')->orderBy('id', 'asc')->get();
    }

    public function ordersDetailsByOrderID($data) {
        return $this->order_model::select('*')->where('id',$data['order_id'])->first();
    }
}