<?php

namespace App\Repositories;

use App\Model\BuyoneMenu;
use App\Model\MasterUserPermissions;
use App\Model\MenuPermissions;
use App\User;
use DemeterChain\B;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\Http\Controllers\PasswordResetController;
use Illuminate\Http\Request;

class UserRepository
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function createSystemUser($data)
    {

        $user = new User;
        $superuser = Auth::user();
        $password_temp = GenerateRandomString(6);
        //$superuser = User::find($data['superuser_id']);
        $data['password'] = bcrypt($password_temp);
        if ($superuser->role_id != 1) {
            $data['superuser_id'] = $superuser->superuser_id;
        } else {
            $data['superuser_id'] = $superuser->id;
        }
        foreach ($data as $key => $value) {
            if ($key != 'menu_permissions') {
                $user->{$key} = $value;
            }
        }

        $result = $user->save();
        $data['temp_password'] = $password_temp;
        Mail::to(['email' => $data['email']])->send(new SendEmail($data, MATER_INVITE_ADMIN));

        $response['user_detail'] = $user;
        $response['permission_detail'] = [];
        if ($data['menu_permissions']) {
            foreach ($data['menu_permissions'] as $permission_obj) {
                $master_user_permission = new MenuPermissions();
                $permission_obj['user_id'] = $user->id;
                $permission_obj['user_type'] = 'master';
                $permission_obj['superuser_id'] = $user->superuser_id;
                $permission_obj['role_id'] = $user->role_id;
                unset($permission_obj['id']);
                foreach ($permission_obj as $key => $value) {
                    $master_user_permission->{$key} = $value;
                }
                $master_user_permission->save();
                array_push($response['permission_detail'], $master_user_permission);
            }
        }
        return $response;
    }

    public function forgotPassword($data)
    {

        $request = new Request();
        $request->replace(['email' => $data['email'], 'type' => 'master']);
        $passwordReset = new PasswordResetController();
        $response = $passwordReset->create($request);

        return $response;
    }

    public function addPermissions(array $data)
    {
        $res = MenuPermissions::create($data);
        return $res;
    }

    public function getUsersList($filters)
    {

        $data = $this->user
        ->where('is_deleted', false)
        ->where('superuser_id', '!=', null)
        ->get();

        $formattedKey = null;
        
        if (isset($filters->sort_type)) {
            $sort_type = $filters->sort_type;
            $key = $filters->sort_by_key;
            
            $formattedKey = $key;
            
            if ($sort_type == 0) {
                $data = $data->sortBy($formattedKey)->values();
            } else if ($sort_type == 1) {
                $data = $data->sortByDesc($formattedKey)->values();
            }
        }

        return $data->paginate(30)
            ->toArray();
    }

    public function fetchUserDetail($data)
    {
        if (isset($data['id'])) {
            if (isset($data['role_id']) && $data['role_id'] == 1) {
                return $user_detail = $this->user
                    ->where('is_deleted', false)
                    ->where('id', $data)
                    ->first();
            } else {
                return $user_detail = $this->user
                    ->with('role', 'menu_permissions')
                    ->where('is_deleted', false)
                    ->where('id', $data['id'])
                    ->first();
            }
        }
        return null;
    }

    public function getUserProfile()
    {
      
        $user = Auth::user();

        if ($user) {
            $user = User::where('id', '=', $user->id)
                ->with(['role', 'menu_permissions'])
                ->first();

            if ($user->role_id == 1) {
                $data = MenuPermissions::where('user_id', $user->id)
                    ->where('role_id', $user->role_id)
                    ->get()->toArray();

                if (empty($data)) {
                    $user = User::where('id', '=', $user->id)
                        ->with(['role', 'menu_permissions'])
                        ->first();
                    $menus = BuyoneMenu::where('user_type', 'master')->get()->toArray();
                    foreach ($menus as $menu) {
                        $menu['menu_id'] = $menu['id'];
                        $menu['add'] = 1;
                        $menu['view'] = 1;
                        $menu['edit'] = 1;
                        $menu['delete'] = 1;
                        $user['menu_permissions']->push($menu);
                    };
                }
            }
        } else {
            $user = "User not found";
        }
        return $user;
    }

    public function updateSystemUser(array $data)
    {
        $res = User::where('id', '=', $data['id'])->update([
            'email' => $data['email'],
            'role_id' => $data['role_id'],
            'is_enable' => $data['is_enable']
        ]);
        return $res;
    }

    public function updatePermissions(array $data)
    {
        MenuPermissions::where('id', $data['id'])->update([
            'add' => $data['add'],
            'edit' => $data['edit'],
            'view' => $data['view'],
            'delete' => $data['delete']
        ]);
        return $data;
    }

    public function deletePermission(int $id)
    {
        $res = MenuPermissions::find($id)->delete();
        return $res;
    }

    public function deleteSystemUser($id)
    {
        MenuPermissions::where('user_id', '=', $id)->delete();
        $res = User::find($id)->delete();
        return $res;
    }
}
