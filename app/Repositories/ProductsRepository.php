<?php

namespace App\Repositories;

use App\Model\Products;

class ProductsRepository
{
    protected $product_model;

    public function __construct(Products $product_model) {
        $this->product_model = $product_model;
    }

    public function productList($data) {
        return $this->product_model::select('*')->whereNotNull('image_snapshot_url')->orderBy('id', 'asc')->get();
    }

    public function productDetailsByPid($data) {
        return $this->product_model::select('*')->where('pid',$data['pid'])->first();
    }
}