<?php

namespace App\Repositories;

use App\Model\Category;

class CategoryRepository
{
    protected $category_model;

    public function __construct(Category $category_model) {
        $this->category_model = $category_model;
    }

    public function categoryList($data) {
        return $this->category_model::select('*')->orderBy('id', 'asc')->get();
    }

    public function productDetailsByCatID($data) {
        return $this->category_model::select('*')->where('category_id',$data['catid'])->first();
    }
}