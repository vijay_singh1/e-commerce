<?php

namespace App\Repositories;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;

class UserRepository {
    
    protected $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    public function getUsers($data) {
       return  $this->user::select('id','name','email')->get();
    }

   
}