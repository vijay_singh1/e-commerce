<?php

namespace App\Repositories;

use App\Model\Banner;

class BannerRepository
{
    protected $banner_model;

    public function __construct(Banner $banner_model) {
        $this->banner_model = $banner_model;
    }

    public function bannerList($data) {
        return $this->banner_model::select('*')->orderBy('id', 'asc')->get();
    }

    public function bannerDetailsByCatID($data) {
        return $this->banner_model::select('*')->where('id',$data['id'])->first();
    }
}