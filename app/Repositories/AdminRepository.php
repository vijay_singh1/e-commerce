<?php

namespace App\Repositories;

use App\Jobs\ProcessStoreProductVariationData;
use App\Jobs\ReplaceDefaultImprintImages;
use App\Jobs\ProductPrintMethodUpdate;

use App\Model\Address;
use App\Model\Attributes;
use App\Model\Category;
use App\Model\CategoryQtyVolumeBreakup;
use App\Model\GlobalAttributes;
use App\Model\Customers;
use App\Model\MasterUserPermissions;
use App\Model\Media;
use App\Model\PersonalizationArea;
use App\Model\ProductAttributes;
use App\Model\ProductAttributeName;
use App\Model\Products;
use App\Model\ProductsFlagStatus;
use App\Model\ProductVariations;
use App\Model\QuantityVolumeDiscount;
use App\Model\SellerStoreProducts;
use App\Model\SellerStoreProductVariations;
use App\Model\SellerUser;
use App\Model\States;
use App\Model\Statuses;
use App\Model\UserRole;
use App\Model\Vendor;
use App\Model\VendorQtyVolumeBreakup;
use App\User;
use App\Mail\SendEmail;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminRepository
{

    protected $user, $seller, $vendor, $category, $products, $vendor_qty_volume_breakup,
    $category_qty_volume_breakup, $quantity_volume_discount, $product_variations,
    $status, $products_flag_status, $global_attributes, $attributes,
    $personalization_area, $adddress, $product_attributes, $product_attribute_name, $media;

    public function __construct(
        User $user,
        SellerUser $seller,
        Vendor $vendor,
        Category $category,
        Products $products,
        VendorQtyVolumeBreakup $vendor_qty_volume_breakup,
        CategoryQtyVolumeBreakup $category_qty_volume_breakup,
        QuantityVolumeDiscount $quantity_volume_discount,
        ProductVariations $product_variations,
        Statuses $status,
        ProductsFlagStatus $products_flag_status,
        GlobalAttributes $global_attributes,
        Attributes $attributes,
        PersonalizationArea $personalization_area,
        Address $address,
        ProductAttributes $product_attributes,
        ProductAttributeName $product_attribute_name,
        Media $media
    ) {
        $this->user = $user;
        $this->seller = $seller;
        $this->vendor = $vendor;
        $this->category = $category;
        $this->products = $products;
        $this->vendor_qty_volume_breakup = $vendor_qty_volume_breakup;
        $this->category_qty_volume_breakup = $category_qty_volume_breakup;
        $this->quantity_volume_discount = $quantity_volume_discount;
        $this->product_variations = $product_variations;
        $this->status = $status;
        $this->products_flag_status = $products_flag_status;
        $this->global_attributes = $global_attributes;
        $this->attributes = $attributes;
        $this->personalization_area = $personalization_area;
        $this->address = $address;
        $this->product_attributes = $product_attributes;
        $this->media = $media;
        $this->product_attribute_name = $product_attribute_name;
    }

    public function allSellers()
    {
        $sellers = $this->seller
            ->select('id', 'name', 'level_plan', 'created_at', 'status_id', 'seller_id')
            ->with([
                'status' => function ($query) {
                    $query->select('id', 'name');
                },
            ])
            ->get()
            ->toArray();

        return $sellers;
    }

    public function sellerDetail($seller_id)
    {
        $seller = $this->getSellerDetail('id', $seller_id);

        if ($seller) {
            return $seller[0];
        } else {
            return [];
        }

    }

    public function getSupplierCompanyRefNameAll(){

        return Products::where("is_enabled",1)->distinct()->orderBy('supplier_company_ref_name', 'ASC')->pluck('supplier_company_ref_name');
    }

    public function updateVendorDetail($data)
    {

        /**
         * update vendor detail and margin price
         */
        $update_vendor_detail_flag = $this->vendor
            ->where('vendor_id', $data['vendor_id'])
            ->update([
                'address_line_1' => $data['address_line_1'] ?? null,
                'address_line_2' => $data['address_line_2'] ?? null,
                'bank_acc_holder' => $data['bank_acc_holder'] ?? null,
                'bank_acc_no' => $data['bank_acc_no'] ?? null,
                'bank_acc_type' => $data['bank_acc_type'] ?? null,
                'bank_branch' => $data['bank_branch'] ?? null,
                'bank_iban_no' => $data['bank_iban_no'] ?? null,
                'bank_name' => $data['bank_name'] ?? null,
                'bank_routing_no' => $data['bank_routing_no'] ?? null,
                'business_EIN' => $data['business_EIN'] ?? null,
                'business_name' => $data['business_name'] ?? null,
                'city' => $data['city'] ?? null,
                'state' => $data['state'] ?? null,
                'country' => $data['country'] ?? null,
                'email' => $data['email'] ?? null,
                'first_name' => $data['first_name'] ?? null,
                'is_active_ach_acc' => $data['is_active_ach_acc'] ?? null,
                'last_name' => $data['last_name'] ?? null,
                'margin_markup_override_option' => $data['margin_markup_override_option'] ?? null,
                'margin_markup_override_type' => $data['margin_markup_override_type'] ?? null,
                'margin_markup_override_value' => $data['margin_markup_override_value'] ?? null,
                'phone' => $data['phone'] ?? null,
                'postal_code' => $data['postal_code'] ?? null,
                'time_zone' => $data['time_zone'] ?? null,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        /**
         * update category margin
         */
        $update_category_markup_flag = $this->category
            ->where('vendor_id', $data['vendor_id'])
            ->update([
                'margin_markup_override_option' => $data['margin_markup_override_option'],
                'margin_markup_override_type' => $data['margin_markup_override_type'],
                'margin_markup_override_value' => $data['margin_markup_override_value'],
            ]);

        /**
         * update product margin
         */
        $update_product_markup_flag = $this->products
            ->where('vendor_id', $data['vendor_id'])
            ->update([
                'margin_option' => $data['margin_markup_override_option'],
                'margin_type' => $data['margin_markup_override_type'],
                'margin' => $data['margin_markup_override_value'],
            ]);

        $category_ids = $this->category
            ->where('vendor_id', $data['vendor_id'])
            ->pluck('id')
            ->toArray();

        $product_pids = $this->products
            ->where('vendor_id', $data['vendor_id'])
            ->pluck('pid')
            ->toArray();

        if ($data['enable_volume_breakup']) {

            $previous_qty_blocks_ids = $this->vendor_qty_volume_breakup
                ->where('vendor_id', $data['vendor_id'])
                ->pluck('id')
                ->toArray();

            foreach ($data['qty_volume_breakup'] as $key => $qty_vol) {

                if (!sizeof($previous_qty_blocks_ids)) {

                    $inserted_data = $this->vendor_qty_volume_breakup
                        ->insert([
                            'vendor_id' => $data['vendor_id'],
                            'qty_volume_id' => $qty_vol['qty_volume_id'],
                            'discount' => $qty_vol['discount'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);

                } else {
                    $update_product_qty_breakup_flag = $this->vendor_qty_volume_breakup
                        ->where(
                            'id',
                            $previous_qty_blocks_ids[$key]
                        )
                        ->update(
                            [
                                'qty_volume_id' => $qty_vol['qty_volume_id'],
                                'discount' => $qty_vol['discount'],
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                }

            }

            $this->updateProductQtyVolume($data, $product_pids);

            $this->updateCategoryQtyUpdate($data, $category_ids);

        }

        if ($update_vendor_detail_flag && $update_category_markup_flag && $update_product_markup_flag) {
            $updated_info = $this->vendor
                ->with([
                    'vendorQtyBlocks' => function ($query) {
                        $query->select('vendor_id', 'qty_volume_id', 'discount');
                    },
                ])
                ->where('vendor_id', $data['vendor_id'])
                ->get()
                ->toArray();

            return $updated_info;
        } else {
            return false;
        }
    }

    protected function updateProductVariationMarkup($data, $pid)
    {

        $update_product_variations_markup_flag = $this->product_variations
            ->where('id', $pid)
            ->update([
                'margin_markup_override_option' => $data['margin_markup_override_option'],
                'margin_markup_override_type' => $data['margin_markup_override_type'],
                'margin_markup_override_value' => $data['margin_markup_override_value'],
            ]);
    }

    protected function updateCategoryQtyUpdate($data, $category_ids)
    {

        for ($j = 0; $j < sizeof($category_ids); $j++) {

            $category_qty_breakup_ids = $this->category_qty_volume_breakup
                ->where('category_id', $category_ids[$j])
                ->pluck('id')
                ->toArray();

            $qty_vol_breakup = $data['qty_volume_breakup'];

            for ($i = 0; $i < sizeof($qty_vol_breakup); $i++) {

                if (!sizeof($category_qty_breakup_ids)) {
                    $this->category_qty_volume_breakup
                        ->insert(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'category_id' => $category_ids[$j],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                } else {
                    $update_category_qty_breakup_flag = $this->category_qty_volume_breakup
                        ->where(
                            'id',
                            $category_qty_breakup_ids[$i]
                        )
                        ->update(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                }
            }
        }
    }

    protected function updateProductQtyVolume($data, $product_pids)
    {

        for ($j = 0; $j < sizeof($product_pids); $j++) {
            $product_qty_breakup_ids = $this->quantity_volume_discount
                ->where('pid', $product_pids[$j])
                ->delete();

            $qty_vol_breakup = $data['qty_volume_breakup'];

            for ($i = 0; $i < sizeof($qty_vol_breakup); $i++) {
                $this->quantity_volume_discount
                    ->insert(
                        [
                            'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                            'discount' => $qty_vol_breakup[$i]['discount'],
                            'pid' => $product_pids[$j],
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]
                    );
            }
            $this->updateProductVariationMarkup($data, $product_pids[$j]);
        }
    }

    public function updateProductPriceDetail($data)
    {

        $pid = $this->products
            ->where('pid', $data['pid'])
            ->pluck('pid')
            ->toArray();

        /**
         * update product margin
         */
        $update_product_markup_flag = $this->products
            ->where('pid', $data['pid'])
            ->update([
                'margin_option' => $data['margin_markup_override_option'],
                'margin_type' => $data['margin_markup_override_type'],
                'margin' => $data['margin_markup_override_value'],
                'sale_price' =>$data['sale_price'],
                'is_enable_volume_breakup' => $data['enable_volume_breakup'],
            ]);

        $this->updateProductQtyVolume($data, $pid);

        $updated_prices = $this->products
            ->select('id', 'pid', 'supplier_sku', 'min_order_qty', 'tax_rate', 'margin', 'margin_type', 'margin_option', 'sale_price', 'is_enabled', 'is_enable_volume_breakup as enable_volume_breakup')
            ->with([
                'quantity_volume' => function ($query) {
                    $query->select('pid', 'qty_volume_id', 'discount');
                },
                'price' => function ($query) {
                    $query->select('sku', 'master_profit', 'master_selling_price', 'tier_price', 'tier_price_carriage', 'tier_price_qty', 'tier_price_rrp');
                },
            ])
            ->where('pid', $data['pid'])
            ->get()
            ->toArray();

        if (sizeof($updated_prices)) {
            return $updated_prices[0];
        }

        return [];
    }

    public function updateProductStatus($data)
    {
        $update_status_flag = MasterProductStatusUpdate($data['pid'],array('is_enabled' => $data['is_enabled']));
        //  $this->products
        //     ->where('id', $data['pid'])
        //     ->update([
        //         'is_enabled' => $data['is_enabled'],
        //     ]);

        if ($update_status_flag == 1) {

            $updated_detail = $this->products
                ->select('id', 'pid', 'category_id', 'name as product_name', 'supplier_sku', 'thumbnail_snapshot_url', 'image_snapshot_url', 'orders', 'views', 'margin_option', 'margin_type', 'margin', 'sale_price', 'promotion_ids', 'is_enabled')
                ->with([
                    'category' => function ($query) {
                        $query->select('id', 'name');
                    },
                ])
                ->where(
                    'id',
                    $data['pid']
                )
                ->get()
                ->toArray();
            if ($updated_detail) {
                return $updated_detail[0];
            } else {
                return [];
            }

        } else {
            return false;
        }

    }

    /**
     * This function find seller and send verification email
     * @param $id
     * @return |null
     */
    public function sendVerificationEmailToSeller($id)
    {
        try {
            $seller = $this->seller->find($id);
            $email_data = [
                'email' => $seller->email,
                'seller_id'=> $id,
                'link' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/images/mail/Register Successfully.svg',
            ];
            Mail::to($email_data['email'])->send(new SendEmail( $email_data, SELLER_SIGNUP));
            return $seller->id;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * This function find seller and verification manual
     * @param $id
     * @return |null
     */
    public function sellerVerificationManual($id)
    {
       
        try {
            $seller = $this->seller->where('id',$id)->update([
                'email_verified_at' => date("Y-m-d H:i:s"),
            ]);
           
            return $id;
        } catch (\Exception $e) {
            return null;
        }
    }

    

    public function updateProductInfo($data)
    {
        $msg = '';
        $update_product_info_flag=array();
       
        if(!empty($data->print_method)){
           
                $update_product_info_flag = $this->products
                ->where('id', $data->pid)
                ->update([
                    'category_id' => $data->category_id,
                    'show_size_chart' => $data->show_size_chart,
                    'promotion_ids' => json_encode($data->promotion_ids),
                    'print_method' => ($data->print_method),
                ]);
                ProductPrintMethodUpdate::dispatch($product_details->pid,$data->print_method);
            
        }
        
        if ($update_product_info_flag) {
            $product_info = $this->getProductInfo($data->pid);
            $product_info['msg'] = $msg;
            return $product_info;
        } else {
            return false;
        }
    }
    

    // public function updateProductInfo($data)
    // {
    //     $msg = '';
    //     $update_product_info_flag=array();
    //     $product_details = $this->products->find($data->pid);
    //     if(!empty($data->print_method)){
    //         $print_method_arr = explode(',',$data->print_method);
    //         if(in_array('2',$print_method_arr)){
    //             $PersonalizationArea =  PersonalizationArea::where('pid',$product_details->pid)->where('area_name','Embroidery')->first();
    //             if(isset($PersonalizationArea->id) && !empty($PersonalizationArea->id)){
    //                 $update_product_info_flag = $this->products
    //                 ->where('id', $data->pid)
    //                 ->update([
    //                     'category_id' => $data->category_id,
    //                     'show_size_chart' => $data->show_size_chart,
    //                     'promotion_ids' => json_encode($data->promotion_ids),
    //                     'print_method' => ($data->print_method),
    //                 ]);
    //                 ProductPrintMethodUpdate::dispatch($product_details->pid,$data->print_method);
    //             }else{
    //                 $update_product_info_flag = $this->products
    //                 ->where('id', $data->pid)
    //                 ->update([
    //                     'category_id' => $data->category_id,
    //                     'show_size_chart' => $data->show_size_chart,
    //                     'promotion_ids' => json_encode($data->promotion_ids),
    //                     'print_method' => 1,
    //                 ]);
    //                 ProductPrintMethodUpdate::dispatch($product_details->pid,$data->print_method);
    //                 $msg = "Embroidery printing location not found!";

    //             }
    //         }else{
    //             $update_product_info_flag = $this->products
    //             ->where('id', $data->pid)
    //             ->update([
    //                 'category_id' => $data->category_id,
    //                 'show_size_chart' => $data->show_size_chart,
    //                 'promotion_ids' => json_encode($data->promotion_ids),
    //                 'print_method' => 1,
    //             ]);
    //             ProductPrintMethodUpdate::dispatch($product_details->pid,$data->print_method);
    //         }
    //     }else{
    //         $update_product_info_flag = $this->products
    //                 ->where('id', $data->pid)
    //                 ->update([
    //                     'category_id' => $data->category_id,
    //                     'show_size_chart' => $data->show_size_chart,
    //                     'promotion_ids' => json_encode($data->promotion_ids),
    //                     'print_method' => 1,
    //                 ]);
    //         ProductPrintMethodUpdate::dispatch($product_details->pid,$data->print_method);
    //     }
       
       
    //     if ($update_product_info_flag) {
    //         $product_info = $this->getProductInfo($data->pid);
    //         $product_info['msg'] = $msg;
    //         return $product_info;
    //     } else {
    //         return false;
    //     }
    // }

    protected function getProductInfo($pid)
    {

        $productData = $this->products
            ->select('id', 'pid', 'category_id', 'name as product_name', 'supplier_sku', 'supplier_company_ref_name as supplier_name', 'thumbnail_snapshot_url', 'image_snapshot_url', 'orders', 'views', 'min_order_qty', 'is_enabled','print_method')
            ->with([
                'category' => function ($query) {
                    $query->select('id', 'name')
                        ->where('category_level', '<>', 1);
                },
            ])
            ->where('id', $pid)
            ->get()
            ->toArray();

        $id = $productData ? $productData[0]['id'] : 0;

        $categories = $this->category
            ->select('name', 'id')
            ->where('category_level', '<>', '1')
            ->get()
            ->toArray();

        $product_images = $this->media
            ->select('file_name', 'id')
            ->where('relation_id', $id)
            ->where('relation_type', 'product')
            ->get()
            ->toArray();

        $promotional_flags = $this->products_flag_status
            ->select('id', 'name')
            ->get()
            ->toArray();

        $product['basicInfo'] = $productData ? $productData[0] : [];
        $product['categories'] = $categories;
        $product['promotional_flags'] = $promotional_flags;
        $product['images'] = $product_images;

        return $product;
    }

    public function updateProductImagesData($data)
    {
        // if (isset($data['image_data'])) {
        $ids = [];
        foreach ($data['image_data'] as $key => $image_data) {
            if (isset($image_data['id'])) {
                array_push($ids, $image_data['id']);
                $updated_data = $this->media
                    ->where('id', $image_data['id'])
                    ->update([
                        'image_view' => $image_data['image_view'] ?? '',
                        'position_view' => $image_data['position_view'] ?? 1, // default set to 1
                        'image_option' => $image_data['image_option'] ?? '',
                    ]);
            } else {
                return false;
            }

        }

        return $this->media
            ->whereIn('id', $ids)
            ->get()
            ->toArray();
        // }
    }

    public function addGlobalAttributeName($data)
    {
        /**
         * insert new attribute with blank value and blank display value, which will be deleted after assigning one value to it
         */

        if (isset($data['name'])) {

            $affected_data = $this->global_attributes
                ->updateOrInsert(
                    ['name' => strtolower($data['name']), 'value' => null, 'display_value' => null],
                    ['created_at' => new \DateTime(), 'updated_at' => new \DateTime(), 'is_deleted' => false]
                );

            return $this->fetchGlobalAttributeData($data['name']);
        }

        return false;

    }

    public function addGlobalAttributeValue($data)
    {
        /**
         * for the given attribute name, delete an entry of which value is null
         */
        $global_attr_data = $this->global_attributes
            ->where('name', $data['name'])
            ->whereNull('value')
            ->whereNull('display_value')
            ->delete();

        if (isset($data['name']) && isset($data['display_value'])) {
            $data['value'] = $data['value'] ?? $data['display_value'];

            $affected_data = $this->global_attributes
                ->updateOrInsert(
                    [
                        'name' => strtolower($data['name']),
                        'display_value' => strtolower($data['display_value'])
                    ],
                    [
                        'value' => strtolower($data['value']),
                        'sort_order' => !empty($data['sort_order']) ? $data['sort_order'] : '1000',
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime(),
                        'is_deleted' => false
                    ]
                );

            return $this->fetchGlobalAttributeData($data['name']);

        }

        return false;
    }

    public function getGlobalAttrData($data)
    {
        if (isset($data['name'])) {
            return $this->fetchGlobalAttributeData($data['name']);
        }

        return false;
    }

    protected function fetchGlobalAttributeData($name)
    {
        return $global_attributes = $this->global_attributes
            ->whereRaw('lower(name) like (?)', ["%{$name}%"])
        // ->where('value','<>','')
            ->where('is_deleted', false)
        // ->whereNotNull('value')
            ->get()
            ->toArray();
    }

    public function updateGlobalAttributeName($data)
    {
        if (isset($data['old_name']) && isset($data['name'])) {
            $updated_global_attributes = $this->global_attributes
                ->where('name', $data['old_name'])
                ->update([
                    'name' => $data['name'],
                ]);
            return $updated_data = $this->global_attributes
                ->where('name', $data['name'])
                ->get()
                ->toArray();
        }
        return false;
    }

    public function updateGlobalAttributeValue($data)
    {
        if (isset($data['id']) && isset($data['display_value'])) {
            if (!isset($data['value'])) {
                $data['value'] = $data['display_value'];
            }
            $updated_global_attributes = $this->global_attributes
                ->where('id', $data['id'])
                ->update([
                    'value' => $data['value'],
                    'display_value' => $data['display_value'],
                    'sort_order' => !empty($data['sort_order']) ? $data['sort_order'] : null,
                ]);

            return $updated_data = $this->global_attributes
                ->where('id', $data['id'])
                ->get()
                ->toArray();
        }
        return false;
    }

    public function deleteGlobalAttributeName($data)
    {
        if (isset($data['names'])) {
            $names = explode(",", $data['names']);
            $updated_global_attributes = $this->global_attributes
                ->whereIn('name', $names)
                ->update([
                    'is_deleted' => true,
                ]);
            return true;
        }
        return false;
    }

    public function deleteGlobalAttributeValue($data)
    {
        if (isset($data['ids'])) {
            $ids = explode(",", $data['ids']);
            $updated_global_attributes = $this->global_attributes
                ->whereIn('id', $ids)
                ->update([
                    'is_deleted' => true,
                ]);
            return true;
        }
        return false;
    }

    public function allAttributes()
    {
        $attributes = [];

        $attributes = $this->global_attributes
            ->distinct()
            ->where('is_deleted', false)
        // ->whereNotNull('display_value')->where('display_value','<>','')
            ->with('global_attribute_data')
            ->get(['name'])
            ->toArray();

        return $attributes;
    }

    public function allSelectedAttributes($data)
    {

        $selcted_attributes = [];

        if (isset($data['pid'])) {
            $selcted_attribute_data = $this->product_attributes
                ->where('pid', $data['pid'])
            // ->pluck('global_attribute_id')
                ->get()
                ->toArray();
        }

        $selcted_attributes = [];
        foreach ($selcted_attribute_data as $attribute_data) {
            array_push($selcted_attributes, $attribute_data['global_attribute_id']);
        }

        $global_attribute_data = $this->global_attributes
            ->whereIn('id', $selcted_attributes)
            ->select('name')
            ->distinct()
            ->with(['global_attribute_data' => function ($query) use ($selcted_attributes) {
                $query->whereIn('id', $selcted_attributes)->where('is_deleted', false);
            },
            ])
            ->get()
            ->toArray();

        foreach ($global_attribute_data as $index => $global_data) {
            foreach ($global_data['global_attribute_data'] as $key => $global_attr) {
                $product_attribute_id = $this->product_attributes
                    ->where('global_attribute_id', $global_attr['id'])
                    ->where('pid', $data['pid'])
                    ->pluck('id')
                    ->first();

                $global_attribute_data[$index]['global_attribute_data'][$key]['attribute_id'] = $product_attribute_id;
            }

        }

        return $global_attribute_data;
    }

    public function getProductAttributeName($data)
    {
        return $this->product_attribute_name
            ->select('pid', 'attribute_name')
            ->where('pid', $data['pid'])
            ->get()
            ->toArray();
    }

    public function saveProductAttributeName($data)
    {
        $inputs = [
            'pid' => $data['pid'],
            'attribute_name' => $data['attribute_name']
        ];
        $this->product_attribute_name->firstOrCreate($inputs);
        return $this->getProductAttributeName($data);
    }

    public function removeProductAttributeName($data)
    {
        /**
         * fetch attribute ids with the same global attribute name
         */
        $attr_ids = $this->global_attributes
            ->where('name', $data['attribute_name'])
            ->pluck('id')
            ->toArray();

        /**
         * delete previously selected attributes from product attributes table
         */
        $this->product_attributes
            ->where('pid', $data['pid'])
            ->whereIn('global_attribute_id', $attr_ids)
            ->delete();

        /**
         * delete selected attribute name from 'product_attribute_name'
         */
        $this->product_attribute_name->where('pid',$data['pid'])
            ->where('attribute_name',$data['attribute_name'])->delete();

        return $this->getProductAttributeName($data);
    }

    public function selectAttributes($data)
    {
        /**
         * fetch attribute ids with the same global attribute name
         */
        $attr_ids = $this->global_attributes
            ->where('name', $data['attribute_name'])
            ->pluck('id')
            ->toArray();
        /**
         * delete previously selected attributes from product attributes table
         */
        $deleted_rows = $this->product_attributes
            ->where('pid', $data['pid'])
            ->whereIn('global_attribute_id', $attr_ids)
            ->whereNotIn('global_attribute_id', $data['attribute_ids'])
            ->delete();

        foreach ($data['attribute_ids'] as $attribute_id) {
            $affected_data = $this->product_attributes
                ->updateOrInsert(
                    ['pid' => $data['pid'], 'global_attribute_id' => $attribute_id],
                    ['product_sku' => $data['sku'], 'created_at' => new \DateTime(), 'updated_at' => new \DateTime()]
                );
        }

        /**
         * get saved attribute data
         */
        return $this->product_attributes
            ->where('pid', $data['pid'])
            ->get()
            ->toArray();

    }

    public function saveAttributes($data)
    {

        $last_attribute_id = $this->attributes
            ->orderByRaw('attribute_id DESC')
            ->pluck('attribute_id')
            ->first();

        $new_attributes = array();
        $attribute_ids = array();

        foreach ($data['attribute_value'] as $key => $attribute_value) {

            array_push($attribute_ids, (int) $last_attribute_id + $key + 1);

            $attribute = array(
                'attribute_id' => (int) $last_attribute_id + $key + 1,
                'attribute_name' => $data['attribute_name'],
                'attribute_value' => $attribute_value,
                'global_attribute_id' => $data['global_attribute_id'],
                'is_selected' => 0,
            );

            array_push($new_attributes, $attribute);
        }

        $inserted_data = $this->attributes->insert($new_attributes);

        if ($inserted_data) {
            $updated_data = $this->attributes
                ->select('id', 'attribute_id', 'attribute_name', 'attribute_value', 'global_attribute_id', 'is_selected')
                ->whereIn('attribute_id', $attribute_ids)
                ->get()->toArray();

            return $updated_data;

        }

        return false;
    }

    public function selectVariationImages($data)
    {
        if (isset($data['id']) && $data['media_ids']) {
            // $media_ids = implode(',',$data['media_ids']);
            $updated_data = $this->product_variations
                ->where('id', $data['id'])
                ->update(['media_ids' => json_encode($data['media_ids'])]);

            if (!$updated_data) {
                return false;
            }

            /**
             * return updated variation's data
             */
            $variation_data = $this->product_variations
                ->where('id', $data['id'])
                ->first()
                ->toArray();

            $storeVariationIds = SellerStoreProductVariations::where('attribute_ids', $variation_data['attribute_ids'])->pluck('id')->toArray();

            /**
             * update media ids in existing store variations
             */
            $updated_store_data = SellerStoreProductVariations::whereIn('id', $storeVariationIds)
                ->update(['media_ids' => json_encode($data['media_ids'])]);

            $ids = explode(',', $variation_data['attribute_ids']);
            $media_ids = $variation['media_ids'] ?? [];

            $related_global_data = $this->product_attributes
                ->select('id', 'global_attribute_id')
                ->whereIn('id', $ids)
                ->get()
                ->toArray();

            $related_selected_media_data = $this->media
                ->whereIn('id', $media_ids)
                ->get()
                ->toArray();

            $variation_data['attribute_data'] = $related_global_data;
            $variation_data['selected_media_data'] = $related_selected_media_data;

            return $variation_data;
        }
    }

    public function generateVariations($data)
    {

        /**
         * check if other variations already exist, then return their price from DB
         */
        $existing_variation_attribute_ids = $this->product_variations
            ->where('parent_pid', $data['parent_pid'])
            ->where('parent_sku', $data['parent_sku'])
            ->pluck('attribute_ids')
            ->toArray();

        $parent_product_price = $this->products
            ->select('id', 'pid', 'supplier_sku', 'margin', 'margin_type', 'margin_option', 'sale_price')
            ->with([
                'price' => function ($query) {
                    $query->select('sku', 'tier_price_qty', 'tier_price');
                },
            ])
            ->where('supplier_sku', $data['parent_sku'])
            ->get()
            ->toArray();

        $final_variations = array();

        /**
         * generate skus for image handling
         */
        $skus = [];
        foreach ($data['attribute_ids'] as $attribute_id) {
            $sku = '';
            foreach ($attribute_id as $attr_id) {
                /**
                 * find global attribute id of related product attribute id
                 */
                $global_attr_id = $this->product_attributes
                    ->where('id', $attr_id)
                    ->pluck('global_attribute_id')
                    ->first();

                $attr_name = $this->global_attributes
                    ->where('id', $global_attr_id)
                    ->pluck('display_value')
                    ->first();

                $sku = $sku . preg_replace('/\s+/', '', $attr_name);
            }
            array_push($skus, $sku);
        }

        foreach ($data['attribute_ids'] as $key => $attribute_id) {
            $attribute_id_str = implode(",", $attribute_id);
            /**
             * find related attribute and global attribute data
             */
            $current_attribute_data = $this->product_attributes
                ->select('id', 'global_attribute_id')
                ->whereIn('id', $attribute_id)
                ->get()
                ->toArray();

            foreach ($current_attribute_data as $index => $values) {
                $current_attribute_data[$index]['attribute_name'] = $values['global_data']['name'];
                $current_attribute_data[$index]['value'] = $values['global_data']['value'];
                $current_attribute_data[$index]['display_value'] = $values['global_data']['display_value'];
                unset($current_attribute_data[$index]['global_data']);
            }

            $affected_data = $this->product_variations
                ->updateOrInsert(
                    ['parent_pid' => $data['parent_pid'], 'attribute_ids' => $attribute_id_str, 'parent_sku' => $data['parent_sku']],
                    ['master_cost' => $parent_product_price[0]['price'][0]['master_cost'], 'sale_price' => $parent_product_price[0]['sale_price'],
                        'margin_markup_override_type' => $parent_product_price[0]['margin_type'], 'margin_markup_override_option' => $parent_product_price[0]['margin_option'],
                        'margin_markup_override_value' => $parent_product_price[0]['margin'], 'generated_sku' => $data['parent_sku'] . $skus[$key],
                        'created_at' => new \DateTime(), 'updated_at' => new \DateTime(), 'attributes_data' => json_encode($current_attribute_data)]
                );

            /**
             * insert variation for existing products of all the stores
             * fetch the store ids
             */
            $store_pro_data = SellerStoreProducts::select('store_id', 'seller_id')->where('pid', $data['parent_pid'])->get()->toArray();

            if (sizeof($store_pro_data)) {
                foreach ($store_pro_data as $index => $store_data) {
                    ProcessStoreProductVariationData::dispatch($data, $attribute_id_str, $store_data, $parent_product_price, $data['parent_sku'] . $skus[$key], json_encode($current_attribute_data));
                }
            }
        }

        /**
         * return variation details
         */
        // $variations = $this->fetchVariationData($data['parent_pid']);

        return true;
    }

    private function fetchVariationData($pid)
    {
        $variation_data = $this->product_variations
            ->where('parent_pid', $pid)
        // ->get()
        // ->toArray();
            ->paginate(30);
        // dd($variation_data);
        foreach ($variation_data as $index => $variation) {
            $ids = explode(',', $variation['attribute_ids']);
            $media_ids = $variation['media_ids'] ?? [];

            /**
             * Following is very illogical code line,
             * which I had to write
             * because 'attributes_data' column was added after I coded this and also was integreted on front end
             * That's why I have written logic to rearrange data in 'attribute_data' key as per my requirement, after removing 'attributes_data' key
             */
            unset($variation_data[$index]['attributes_data']);

            $related_global_data = $this->product_attributes
                ->select('id', 'global_attribute_id')
                ->whereIn('id', $ids)
                ->get()
                ->toArray();

            $related_selected_media_data = $this->media
                ->whereIn('id', $media_ids)
                ->get()
                ->toArray();

            $related_media_data = $this->media
                ->where('relation_id', $pid)
                ->get()
                ->toArray();

            $variation_data[$index]['attribute_data'] = $related_global_data;
            $variation_data[$index]['selected_media_data'] = $related_selected_media_data;
            $variation_data[$index]['media_data'] = $related_media_data;
        }
        return $variation_data;
    }

    public function updateProductMargin($data)
    {
        $updated_product_margin = $this->products
            ->where('supplier_sku', $data['sku'])
            ->update([
                'margin' => $data['profit'],
            ]);

        $updated_variation_margin = $this->product_variations
            ->where('parent_sku', $data['sku'])
            ->update([
                'margin_markup_override_value' => $data['profit'],
            ]);

        if ($updated_product_margin) {

            return $updated_product = $this->products
                ->where('supplier_sku', $data['sku'])
                ->first();

        } else {
            return false;
        }

    }

    public function getVariations($data)
    {
        if (isset($data['pid'])) {
            return $this->fetchVariationData($data['pid']);
        }

        return false;
    }

    public function getPromotions()
    {
        $promotion_data = $this->products_flag_status
            ->select('id', 'name','color')
            ->get()->toArray();

        return $promotion_data;
    }

    public function updateVariations($data)
    {
        $ids = array_column($data, 'id');

        $skus = array_column($data, 'sku');

        /**
         * check if two or more skus have same value in post req
         */
        $sku_len = sizeof($skus);

        $unique_skus = array_unique($skus);

        if (sizeof($unique_skus) < $sku_len) {
            $er = [
                'message' => 'You can\'t have skus for two or more variations',
                'status' => 'error',
            ];
            return $er;
        }

        /**
         * check if variations exist in db
         */
        $variations_db = $this->product_variations
            ->whereIn('id', $ids)
            ->get()
            ->toArray();

        if (sizeof($variations_db) < sizeof($ids)) {
            /**
             * if any of the variation is not present in db
             */
            $er = [
                'message' => 'Variations missing in db',
                'status' => 'error',
            ];
            return $er;
        }

        /**
         * check if any variation has same sku
         */

        foreach ($data as $variation) {
            $skus_db = $this->product_variations
                ->where('sku', $variation['sku'])
                ->where('id', '<>', $variation['id'])
                ->get()
                ->toArray();

            if (sizeof($skus_db)) {
                $er = [
                    'message' => 'You can\'t have same skus',
                    'status' => 'error',
                    'variations' => $skus_db,
                ];
                return $er;
            }
        }

        foreach ($data as $key => $variation) {
            $update_variation_flag = $this->product_variations
                ->where('id', $variation['id'])
                ->update([
                    'sku' => $variation['sku'],
                    'master_cost' => $variation['master_cost'],
                    'margin_markup_override_value' => $variation['margin_value'],
                    'sale_price' => $variation['sale_price'],
                ]);
        }

        $pid = $this->product_variations
            ->where('id', $data[0]['id'])
            ->pluck('parent_pid')
            ->first();

        return $data;
        // $updated_variations = $this->fetchVariationData($pid);

    }

    public function deleteVariations($data)
    {
        $variation_ids = explode(',', $data['variationIds']);
        /**
         * fetch seller store product variations of given id and delete them
         */
        $variation_attribute_ids = $this->product_variations->whereIn('id', $variation_ids)->pluck('attribute_ids')->toArray();

        SellerStoreProductVariations::whereIn('attribute_ids', $variation_attribute_ids)->delete();

        /**
         * remove product variations
         */
        $deleted_variations = $this->product_variations->whereIn('id', $variation_ids)->delete();

        return $deleted_variations;
    }

    public function getProductDescription($data)
    {
        $description_data = $this->products
            ->select('name', 'pid', 'supplier_sku', 'sales_description as short_description', 'sales_description_long as long_description', 'notes', 'seo_page_title', 'seo_page_url', 'seo_page_keywords', 'seo_meta_Description')
            ->where('id', $data['pid'])
            ->get()
            ->toArray();

        return ($description_data ? $description_data[0] : []);
    }

    public function saveProductDescription($data)
    {

        $update_flag = $this->products
            ->where('id', $data['pid'])
            ->update([
                'name' => $data['name'],
                'sales_description' => $data['short_description'],
                'sales_description_long' => $data['long_description'],
                'notes' => $data['notes'],
                'seo_page_title' => $data['seo_page_title'],
                'seo_page_url' => $data['seo_page_url'],
                'seo_page_keywords' => json_encode($data['seo_page_keywords']),
                'seo_meta_Description' => $data['seo_meta_Description'],
            ]);

        if ($update_flag) {
            $description_data = $this->products
                ->select('name', 'pid', 'supplier_sku', 'sales_description as short_description', 'sales_description_long as long_description', 'notes', 'seo_page_title', 'seo_page_url', 'seo_page_keywords', 'seo_meta_Description')
                ->where('id', $data['pid'])
                ->get()
                ->toArray();

            return $description_data[0];
        }

        return [];

    }

    public function getImageViews($data)
    {
        $views = [];
        if (isset($data['pid'])) {
            $views = $this->media
                ->distinct()
                ->where('relation_id', $data['pid'])
                ->whereNotNull('image_view')
                ->pluck('image_view')
                ->toArray();
        }
        return $views;
    }

    public function saveProductPrintLocation($data)
    {
        $data['supplier_sku'] = $this->products
            ->where('id', $data['pid'])
            ->pluck('supplier_sku')
            ->first();

        $pid = $this->products
            ->where('id', $data['pid'])
            ->pluck('pid')
            ->first();
        unset($data['pid']);
        $data['pid'] = $pid;
        $personalization_area = new PersonalizationArea;

        foreach ($data as $key => $value) {
            $personalization_area->$key = $value;
        }

        $inserted_data = $personalization_area->save();

        $inserted_attributes = $personalization_area->getAttributes();

        /**
         * unset other printing location as default, if created printing location is to be set as default
         */

        if ($data['default_imprint_location']) {
            $update_flag = $this->personalization_area
                ->where('supplier_sku', $data['supplier_sku'])
                ->where('id', '<>', $inserted_attributes['id'])
                ->update(['default_imprint_location' => 0]);

            ReplaceDefaultImprintImages::dispatch($personalization_area->id);
        }

        return $inserted_attributes;
    }

    public function updateProductPrintLocation($data)
    {
        if (isset($data['id'])) {
            $updated_row = PersonalizationArea::where('id', $data['id'])->update($data);

            return $data;
        }

        return;
    }

    public function removeProductPrintLocation($data)
    {

        if (isset($data['id'])) {
            $ids = explode(',', $data['id']);
            $updated_row = PersonalizationArea::whereIn('id', $ids)->update([
                'is_deleted' => true,
            ]);

            return $data;
        }

        return;
    }

    public function setDefaultProductPrintLocation($data)
    {
        /**
         * fetch current default printing location
         */
        $product_id = $this->personalization_area
            ->where('id', $data['id'])
            ->pluck('pid')
            ->first();

        $default_print_id = $this->personalization_area
            ->where('pid', $product_id)
            ->where('default_imprint_location', true)
            ->pluck('id')
            ->first();

        /**
         * if current default imprint location is different than in the parameter data,
         * then only execute the following code
         */

        if ($default_print_id != $data['id']) {
            /**
             * set default printing location for given id
             */
            $update_flag = $this->personalization_area
                ->where('id', $data['id'])
                ->update(['default_imprint_location' => 1]);

            if ($update_flag) {
                /**
                 * unset other printing location as default, if created printing location is to be set as default
                 */
                $updated_data = $this->personalization_area
                    ->where('id', $data['id'])
                    ->first()
                    ->getAttributes();

                $this->personalization_area
                    ->where('supplier_sku', $updated_data['supplier_sku'])
                    ->where('id', '<>', $data['id'])
                    ->update(['default_imprint_location' => 0]);

                // $this->dispatch(new ReplaceDefaultImprintImages());
                ReplaceDefaultImprintImages::dispatch($data['id']);

                return $updated_data;

            }
        }

        return $this->personalization_area
            ->where('id', $data['id'])
            ->first()
            ->getAttributes();
    }

    public function getProductAdditionalnfo($data)
    {
        $additional_data = $this->products
            ->select('id', 'supplier_sku', 'warranty', 'material', 'instructions', 'lead_time_days', 'set_up_fees', 'price_stitch', 'price_color', 'length', 'width', 'height', 'weight')
            ->where('id', $data['pid'])
            ->first()
            ->getAttributes();

        return $additional_data;
    }

    public function saveProductAdditionalnfo($data)
    {
        $update_flag = $this->products
            ->where('id', $data['pid'])
            ->update([
                'warranty' => $data['warranty'],
                'material' => $data['material'],
                'instructions' => $data['instructions'],
                'lead_time_days' => $data['lead_time_days'],
                'set_up_fees' => $data['set_up_fees'],
                'price_stitch' => $data['price_stitch'],
                'price_color' => $data['price_color'],
                'length' => $data['length'],
                'width' => $data['width'],
                'height' => $data['height'],
                'weight' => $data['weight'],
            ]);

        if ($update_flag) {
            $additional_data = $this->products
                ->select('id', 'supplier_sku', 'warranty', 'material', 'instructions', 'lead_time_days', 'set_up_fees', 'price_stitch', 'price_color', 'length', 'width', 'height', 'weight')
                ->where('id', $data['pid'])
                ->first()
                ->getAttributes();

            return $additional_data;
        } else {
            return false;
        }
    }

    public function saveSellerDetail($data)
    {
        $country_data = getCountryArray();

        $country_name =$state_name= '';

        foreach ($country_data as $country) {
            if ($data['country'] == $country['id']) {
                $country_name = $country['name'];
                break;
            }
        }
        if(isset($data['state']) && !empty($data['state'])){
          $state_name = States::where('id', $data['state'])->pluck('name')->first();
        }

       // if ($seller->id) {
            if (isset($data['id'])) {
                if (isset($data['address_line_1'])) {

                    $address = $this->address
                        ->updateOrInsert(
                            [
                                'owner_id' => $data['id'],
                                 'address_type' => 'billing',
                                 'user_type' => 'seller',
                            ],
                            [
                                'name' => isset($data['first_name'])?$data['last_name']:"",
                                 'last_name' => isset($data['last_name'])?$data['last_name']:"",
                                'email' => isset($data['email'])?$data['email']:"",
                                'address_line_1' => isset($data['address_line_1'])?$data['address_line_1']:"",
                                 'address_line_2' => isset($data['address_line_2'])?$data['address_line_2']:"",
                                 'state' => $state_name,
                                 'city'=> isset($data['city'])?$data['city']:"",
                                'country' => $country_name,
                                 'zipcode' => isset($data['postal_code'])?$data['postal_code']:"",
                                'contact_no' => isset($data['contact'])?$data['contact']:"",
                                 'updated_at' => new \DateTime(),
                            ]
                        );
                }

                $update_seller_data = $this->seller
                    ->where('id', $data['id'])
                    ->update([
                        'first_name' => isset($data['first_name'])?$data['first_name']:"",
                        'last_name' => isset($data['last_name'])?$data['last_name']:"",
                        'acc_holder' => isset($data['acc_holder'])?$data['acc_holder']:"",
                        'acc_type' =>isset($data['acc_type'])?$data['acc_type']:"",
                        'bank' => isset($data['bank'])?$data['bank']:"",
                        'bank_acc_no' => isset($data['bank_acc_no'])?$data['bank_acc_no']:"",
                        'bank_branch' => isset($data['bank_branch'])?$data['bank_branch']:"",
                        'bank_iban' => isset($data['bank_iban'])?$data['bank_iban']:"",
                        'bank_routing_no' => isset($data['bank_routing_no'])?$data['bank_routing_no']:"",
                        'buisness_size' => isset($data['buisness_size'])?$data['buisness_size']:"",
                        'business_EIN_No' => isset($data['business_EIN_No'])?$data['business_EIN_No']:"",
                        'cc_no' => isset($data['cc_no'])?$data['cc_no']:"",
                        'cc_type' => isset($data['cc_type'])?$data['cc_type']:"",
                        'comments' =>isset( $data['comments'])?$data['comments']:"",
                        'company_name' => isset($data['company_name'])?$data['company_name']:"",
                        'created_at' => isset($data['created_at'])?$data['created_at']:"",
                        'email' => isset($data['email'])?$data['email']:"",
                        'master_comments' => isset($data['master_comments'])?$data['master_comments']:"",
                        'timezone' =>isset( $data['timezone'])?$data['timezone']:"",
                        'is_active_ach_acc' => $data['is_active_ach_acc'] ?? null,
                        'valid_thru' => isset($data['valid_thru'])?$data['valid_thru']:"",
                    ]);
            }
       // }

        if ($update_seller_data) {
            $seller = $this->getSellerDetail('id', $data['id']);
        }

        if ($seller) {
            return $seller[0];
        } else {
            return [];
        }
    }

    public function saveSellerStatus($data)
    {
        $update_flag = $this->seller
            ->where('seller_id', $data['seller_id'])
            ->update(['is_enable' => $data['is_enable']]);

        $updated_data = $this->getSellerDetail('seller_id', $data['seller_id']);

        if ($updated_data) {
            return $updated_data[0];
        } else {
            return [];
        }
    }

    protected function getSellerDetail($where_condn, $where_value)
    {
        $seller = $this->seller
            ->with('sellerAddress')
            ->with([
                'billAddress' => function ($query) {
                    $query->select('id', 'owner_id', 'state', 'city', 'country', 'zipcode', 'address_line_1', 'address_line_2', 'email');
                },
            ])
            ->where($where_condn, $where_value)
            ->get()->map->append('is_seller_status')
            ->toArray();
        return $seller;
    }

    public function getSellers()
    {

        return $this->seller::where('superuser_id',null)->with([
            'billAddress' => function ($query) {
                $query->select('*');
            },
        ])
            ->with([
                'sellerStatus' => function ($query) {
                    $query->select('*')->first();
                },
            ])->with('plan')->get();
    }

    public function sellerByID($seller_id)
    {
        return $this->seller::with([
            'billAddress' => function ($query) {
                $query->select('*');
            },
        ])->where('id', $seller_id)
            ->with([
                'sellerStatus' => function ($query) {
                    $query->select('*')->first();
                },
            ])->with('plan')->get()
            ->first();
    }

    public function setDefaultPermission(array $data)
    {
        return MasterUserPermissions::insert($data);
    }

    public function getRoles($filters, $mode)
    {
        $data = UserRole::where('role_mode', $mode)->get();

        $formattedKey = null;
        if (isset($filters->sort_type)) {
            $sort_type = $filters->sort_type;
            $key = $filters->sort_by_key;

            $formattedKey = $key;

            if ($sort_type == 0) {
                $data = $data->sortBy($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            } else if ($sort_type == 1) {
                $data = $data->sortByDesc($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            }
        }

        return $data;
    }

    public function getRoleWithPermissions($id, $mode)
    {
        $data = UserRole::where('id', $id)
            ->where('role_mode', $mode)
            ->with('defaultPermissions')
            ->get()
            ->toArray();
        return $data;
    }

    public function getCustomerData($filters)
    {
        $data = Customers::with('address')->get();
        $formattedKey = null;
        if (isset($filters->sort_type)) {
            $sort_type = $filters->sort_type;
            $key = $filters->sort_by_key;


            if($key == 'store_name' ||$key == 'store_name' ){
                $formattedKey = 'customer_store_details.'.$key;
            } else if ($key == 'contact_no' || $key == 'state_name' || $key == 'zipcode'){

                if($sort_type == 0){
                    $sorted = $data->sortBy(function ($data) use($key){
                        return !empty($data->address[0]->{$key}) ? $data->address[0]->{$key}
                        : '';
                    });
                } else if ($sort_type == 1){
                    $sorted = $data->sortByDesc(function ($data) use($key) {
                        return !empty($data->address[0]->{$key}) ?
                        $data->address[0]->{$key} : '';
                    });
                }
                return $sorted->values()->paginate(30);
            }
            else {
                $formattedKey = $key;
            }

            if ($sort_type == 0) {
                $data = $data->sortBy($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            } else if ($sort_type == 1) {
                $data = $data->sortByDesc($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            }
        }


        return $data->paginate(30)->toArray();
    }

    public function getCustomerDataByID($store_id,$customer_id)
    {
        $data = Customers::with('address')->get()->where('store_id',$store_id)->where('id',$customer_id)
            ->first();
        return $data;
    }


//    public function createMasterSystemUser(array $data)
    //    {
    //        $data = array_add($data,'password',$this->user->password);
    //        $res = $this->user->create($data);
    //        return $res->get()->last()->id;
    //    }
    //
    //    public function createSellerSystemUser(array $data)
    //    {
    //        $data = array_add($data,'password',$this->seller->password);
    //        $res = $this->seller->create($data);
    //        return $res->get()->last()->id;
    //    }
    //
    //    public function addPermissions(array $data)
    //    {
    //        $res = MasterUserPermissions::create($data);
    //        return $res;
    //    }
    //'master_comments' => $data['master_comments'] ?? null,
    //'company_name' => $data['company_name'] ?? null,
    //'business_EIN_No' => $data['business_EIN_No'] ?? null,
    //'name' => $data['name'] ?? null,
    //'contact' => $data['contact'] ?? null,
    //'timezone' => $data['timezone'] ?? null,
    //'acc_type' => $data['acc_type'] ?? null,
    //'acc_holder' => $data['acc_holder'] ?? null,
    //'bank' => $data['bank'] ?? null,
    //'bank_branch' => $data['bank_branch'] ?? null,
    //'bank_routing_no' => $data['bank_routing_no'] ?? null,
    //'bank_acc_no' => $data['bank_acc_no'] ?? null,
    //'is_active_ach_acc' => $data['is_active_ach_acc'] ?? null,
    //'bank_iban' => $data['bank_iban'] ?? null,
}
