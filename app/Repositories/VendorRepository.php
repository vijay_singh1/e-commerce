<?php

namespace App\Repositories;

use App\Model\Address;
use App\Model\Attributes;
use App\Model\Category;
use App\Model\CategoryQtyVolumeBreakup;
use App\Model\GlobalAttributes;
use App\Model\PersonalizationArea;
use App\Model\Products;
use App\Model\ProductsFlagStatus;
use App\Model\ProductVariations;
use App\Model\QuantityVolumeDiscount;
use App\Model\SellerUser;
use App\Model\Statuses;
use App\Model\Vendor;
use App\Model\VendorQtyVolumeBreakup;
use App\Jobs\VendorToUpdateMasterProduct;


class VendorRepository
{

    protected $seller, $vendor, $category, $products, $vendor_qty_volume_breakup,
        $category_qty_volume_breakup, $quantity_volume_discount, $product_variations,
        $status, $products_flag_status, $global_attributes, $attributes,
        $personalization_area, $adddress;

    public function __construct(
        SellerUser $seller,
        Vendor $vendor,
        Category $category,
        Products $products,
        VendorQtyVolumeBreakup $vendor_qty_volume_breakup,
        CategoryQtyVolumeBreakup $category_qty_volume_breakup,
        QuantityVolumeDiscount $quantity_volume_discount,
        ProductVariations $product_variations,
        Statuses $status,
        ProductsFlagStatus $products_flag_status,
        GlobalAttributes $global_attributes,
        Attributes $attributes,
        PersonalizationArea $personalization_area,
        Address $address
    ) {
        $this->seller = $seller;
        $this->vendor = $vendor;
        $this->category = $category;
        $this->products = $products;
        $this->vendor_qty_volume_breakup = $vendor_qty_volume_breakup;
        $this->category_qty_volume_breakup = $category_qty_volume_breakup;
        $this->quantity_volume_discount = $quantity_volume_discount;
        $this->product_variations = $product_variations;
        $this->status = $status;
        $this->products_flag_status = $products_flag_status;
        $this->global_attributes = $global_attributes;
        $this->attributes = $attributes;
        $this->personalization_area = $personalization_area;
        $this->address = $address;
    }

    public function getAllVendors()
    {
        $res = Vendor::all();
        return $res;
    }

    public function updateVendorDetail($data)
    {
       
        /**
         * update vendor detail and margin price
         */
        $vendor_detail = $data;
        unset($vendor_detail['qty_volume_breakup']);

        $update_vendor_detail_flag = $this->vendor
            ->where('vendor_id', $data['vendor_id'])
            ->update($vendor_detail);
         
        VendorToUpdateMasterProduct::dispatch($data);
      
        $updated_info = true;

        if ($update_vendor_detail_flag) {

            $updated_info = $this->vendor
                ->with([
                    'get_vendor_address' => function ($query) {
                        $query->select('*');
                    },
                ])
                ->with([
                    'vendorQtyBlocks' => function ($query) {
                        $query->select('vendor_id', 'qty_volume_id', 'discount');
                    },
                ])
                ->where('vendor_id', $data['vendor_id'])
                ->get()
                ->toArray();
               
            return $updated_info;
        } else {
            return false;
        }
    }

    protected function updateProductVariationMarkup($data, $pid)
    {

        $update_product_variations_markup_flag = $this->product_variations
            ->where('id', $pid)
            ->update([
                'margin_markup_override_option' => $data['margin_markup_override_option'],
                'margin_markup_override_type' => $data['margin_markup_override_type'],
                'margin_markup_override_value' => $data['margin_markup_override_value'],
            ]);
    }

    protected function updateProductQtyVolume($data, $product_pids)
    {

        for ($j = 0; $j < sizeof($product_pids); $j++) {
            $product_qty_breakup_ids = $this->quantity_volume_discount
                ->where('pid', $product_pids[$j])
                ->pluck('id')
                ->toArray();

            $qty_vol_breakup = $data['qty_volume_breakup'];

            for ($i = 0; $i < sizeof($qty_vol_breakup); $i++) {
                if (!sizeof($product_qty_breakup_ids)) {
                    $this->quantity_volume_discount
                        ->insert(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'pid' => $product_pids[$j],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                } else {
                    $update_product_qty_breakup_flag = $this->quantity_volume_discount
                        ->where(
                            'id',
                            $product_qty_breakup_ids[$i]
                        )
                        ->update(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                }
            }

            /**
             * update product variation margin - markup
             */

            $this->updateProductVariationMarkup($data, $product_pids[$j]);
        }
    }

    protected function updateCategoryQtyUpdate($data, $category_ids)
    {

        for ($j = 0; $j < sizeof($category_ids); $j++) {

            $category_qty_breakup_ids = $this->category_qty_volume_breakup
                ->where('category_id', $category_ids[$j])
                ->pluck('id')
                ->toArray();

            $qty_vol_breakup = $data['qty_volume_breakup'];

            for ($i = 0; $i < sizeof($qty_vol_breakup); $i++) {

                if (!sizeof($category_qty_breakup_ids)) {
                    $this->category_qty_volume_breakup
                        ->insert(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'category_id' => $category_ids[$j],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                } else {
                    $new_cat_v = isset($category_qty_breakup_ids[$i]) ? $category_qty_breakup_ids[$i] : "";
                    $update_category_qty_breakup_flag = $this->category_qty_volume_breakup
                        ->where(
                            'id',
                            $new_cat_v

                        )
                        ->update(
                            [
                                'qty_volume_id' => $qty_vol_breakup[$i]['qty_volume_id'],
                                'discount' => $qty_vol_breakup[$i]['discount'],
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]
                        );
                }
            }
        }
    }

    public function getVendorDetails($id)
    {
        $sellerstore = $this->vendor
            ->select('*')->where('vendor_id', $id)
            ->with([
                'get_vendor_address' => function ($query) {
                    $query->select('*');
                },
            ])
            ->with([
                'vendorQtyBlocks' => function ($query) {
                    $query->select('vendor_id', 'qty_volume_id', 'discount');
                },
            ])
            ->with([
                'get_vendor_category' => function ($query) {
                    $query->select('id', 'name', 'vendor_id');
                },
            ])
            ->get()
            ->first();
        return $sellerstore;
    }

    public function lastVendorPriceUpdate()
    {
        $sellerstore = $this->vendor
            ->select('*')->latest()->first();
        return $sellerstore;
    }

    public function vendorStatusUpdate($data)
    {
        return vendorStatusUpdate($data);
    }


    
}
