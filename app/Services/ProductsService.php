<?php

namespace App\Services;

use App\Filters\FilterableTrait;
use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;

class ProductsService
{
    use FilterableTrait;

    /**
     * The filter options. This property is used when associated model filters are used.
     * Keys:-
     * - `model` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     * - `resolve` : array : The list of filters that belong to a related model
     *        -    `relation` : string : The relation name with the existing model i.e. The name of function in which the model-model (has*, belongsTo) is written.
     *        -    `package` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     *        -    `class` : string : The class to which the filter belongs
     *        -    `args` : array : Key-value pair of the list of arguments to pass to the filter along with the value received
     * - `required` : array : Contains the list of filters that are to be executed at all times. Key-value pair, with the key being the filter name and the value to be provided to the filter.
     * @var array
     */
    private static $filterMap = [
        'model' => 'Products',
        'resolve' => [
            'sku' => [
                'package' => 'Products',
                'class' => 'Sku',
            ],
            'status' => [
                'package' => 'Products',
                'class' => 'Status',
            ],
            'cost' => [
                'package' => 'Products',
                'class' => 'CostRange',
            ],
            'category' => [
                'package' => 'Products',
                'class' => 'Products',
            ],
            'supplier_company_ref_name' => [
                'package' => 'Products',
                'class' => 'SupplierCompanyRefName',
            ],
        ],
        /*
         * The list of filters that always need to be executed
         */
        'required' => [
            'sort' => 'created',
            'allowed_roles' => '',
        ],
    ];

    public function __construct(ProductsRepository $products)
    {
        $this->products = $products;
    }

    public function productList(Request $data)
    {
        return $this->products->productList($data);
    }

    public function productDetailsByPid($data)
    {
        return $this->products->productDetailsByPid($data);
    }

}
