<?php

namespace App\Services;
use App\Model\OrderItems;
use App\Filters\FilterableTrait;

class OrderItemService
{
    use FilterableTrait;


    private static $filterMap = [
        'model' => 'OrderItems',
        'resolve' => [
            'id' => [
                'package' => 'Orders',
                'class' => 'Id'
            ],
            'date' => [
                'package' => 'Orders',
                'class' => 'Created'
            ],
            'sort'=>[
                'package' => 'Orders',
                'class' => 'SortBy'
            ],        
        ],
        /*
         * The list of filters that always need to be executed
         */
        'required' => [

        ]
    ];

    // filter is return QueryBuilder;

    public static function filter(Request $filters) {
        $query = (new OrderItems)->newQuery()->with(['user','address','items']);
        $query = static::applyFilters($filters, $query);
        return $query;
    }

}