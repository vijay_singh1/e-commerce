<?php

namespace App\Services;

use App\Model\GlobalQtyVolume;
use App\Model\CategoryQtyVolumeBreakup;
use App\Model\QuantityVolumeDiscount;
use App\Model\SellerStoreCategoryQtyVolumeBreakup;
use App\Model\SellerStoreProductQuantityVolumeDiscount;
use App\Model\VendorQtyVolumeBreakup;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;

class UserService
{

    use ValidatorTrait;

    protected $user;
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
        $this->setValidationRules();
    }

    protected function setValidationRules()
    {
        $this->validationRules = [
            'addSystemUser' => [
                'name' => ['required'],
                'email' => ['required'],
                'is_enable' => ['required'],
                'role_id' => ['required'],
                'menu_permissions' => ['required']
            ],
            'updateSystemUser' => [
                'id' => ['required'],
                'email' => ['required'],
                'is_enable' => ['required'],
                'role_id' => ['required'],
                'menu_permissions' => ['required']
            ],
            'getUserDetail' => [
                'id' => ['required']
            ],
            'addUpdateGlobalQtyVolume' => [
                'qty_discount' => ['required']
            ]
        ];
    }

    public function createSystemUser($data)
    {
        return $added_data = $this->user->createSystemUser($data);
    }

    public function addPermissionsToUser(array $data)
    {
        $res = $this->user->addPermissions($data);
        return $res;
    }

    public function getUsersList($request)
    {
        return $data = $this->user->getUsersList($request);
    }

    public function getUserProfile()
    {
        return $this->user->getUserProfile();
    }

    public function forgotPassword($data)
    {
        return $this->user->forgotPassword($data);
    }

    public function fetchUserDetail($data)
    {
        return $data = $this->user->fetchUserDetail($data);
    }

    public function updateSystemUser(array $data)
    {
        $res = $this->user->updateSystemUser($data);
        return $res;
    }

    public function updatePermissions(array $data)
    {
        $res = $this->user->updatePermissions($data);
        return $res;
    }

    public function deleteSystemUser(int $id)
    {
        $res = $this->user->deleteSystemUser($id);
        return $res;
    }

    public function deletePermission(int $id)
    {
        $res = $this->user->deletePermission($id);
        return $res;
    }

    public function addUpdateGlobalQtyVolume(array $datas = array(), string $type = '')
    {
        $data = [];
        $global_qty_volume = new GlobalQtyVolume;
        switch ($type) {
            case "add-update":
                if (isset($datas['qty_discount']) && !empty($datas)) {
                    foreach ($datas['qty_discount'] as $key => $values) {
                        if (isset($values['id']) && !empty($values['id'])) {
                            $update_global_qty_volume = $global_qty_volume->find($values['id']);
                            if (!empty($update_global_qty_volume)) {
                                foreach ($values as $key => $value) {
                                    $update_global_qty_volume->{$key} = $value;
                                }
                                $update_global_qty_volume->update();
                            }
                        } else {
                            $global_qty_volume->create($values);
                        }
                    }
                }
                $data = true;
                break;
            case "delete":
                CategoryQtyVolumeBreakup::where('qty_volume_id', $datas['id'])->delete();
                QuantityVolumeDiscount::where('qty_volume_id', $datas['id'])->delete();
                SellerStoreCategoryQtyVolumeBreakup::where('qty_volume_id', $datas['id'])->delete();
                SellerStoreProductQuantityVolumeDiscount::where('qty_volume_id', $datas['id'])->delete();
                VendorQtyVolumeBreakup::where('qty_volume_id', $datas['id'])->delete();
                $data = $global_qty_volume->where('id', $datas['id'])->delete();
                break;
            case "get-all":
                $data = $global_qty_volume->select('id', 'min_qty', 'max_qty')->orderBy('min_qty', 'asc')->get()->toArray();
                break;
            case "get-individual":
                $data = $global_qty_volume->select('id', 'min_qty', 'max_qty')->where('id', $datas['id'])->get()->first();
                break;
        }

        return $data;
    }
}
