<?php

namespace App\Services;

use App\Model\Category;
use App\Model\CmsBanner;
use App\Model\Orders;
use App\Model\Products;
use App\Model\SellerUser;
use App\Model\Vendor;
use App\Repositories\AdminRepository;
use App\User;
use Illuminate\Support\Collection;
use Stripe\Order;

class AdminService
{
    use ValidatorTrait;

    protected $admin;

    public function __construct(AdminRepository $admin)
    {
        $this->admin = $admin;
        $this->setValidationRules();
    }

    public function allSellers()
    {
        $sellers = $this->admin->allSellers();
        $final_sellers = [];

        foreach ($sellers as $seller) {
            $temp_seller = [];
            $temp_seller['id'] = $seller['id'];
            $temp_seller['seller_id'] = $seller['seller_id'];
            $temp_seller['name'] = $seller['name'];
            $temp_seller['level_plan'] = $seller['level_plan'];
            $temp_seller['created_at'] = $seller['created_at'];
            array_push($final_sellers, $temp_seller);
        }

        return $final_sellers;
    }

    public function getSellerDetail($seller_id)
    {
        $seller = $this->admin->sellerDetail($seller_id);

        $final_seller = $seller;

        if (isset($seller['bill_address'])) {
            $final_seller['state'] = $final_seller['bill_address']['state'];
            $final_seller['country'] = $final_seller['bill_address']['country'];
            $final_seller['city'] = $final_seller['bill_address']['city'];
            $final_seller['zipcode'] = $final_seller['bill_address']['zipcode'];
            $final_seller['address_line_1'] = $final_seller['bill_address']['address_line_1'];
            $final_seller['address_line_2'] = $final_seller['bill_address']['address_line_2'];
            $final_seller['email'] = $final_seller['bill_address']['email'];
            unset($final_seller['bill_address']);
        }

        return $final_seller;
    }

    public function updateVendorDetail($data)
    {
        return $updated_data = $this->admin->updateVendorDetail($data);
    }

    public function getSupplierCompanyRefNameAll()
    {
        return  $this->admin->getSupplierCompanyRefNameAll();
    }

    
    public function updateProductPriceDetail($data)
    {
        $prices = $this->admin->updateProductPriceDetail($data);

        $final_prices = [];
        if (sizeof($prices)) {
            $final_prices['id'] = $prices['id'];
            $final_prices['supplier_sku'] = $prices['supplier_sku'];
            $final_prices['min_order_qty'] = $prices['min_order_qty'];
            $final_prices['tax_rate'] = $prices['tax_rate'] ?? 0;
            $final_prices['margin'] = $prices['margin'] ?? 0;
            $final_prices['margin_type'] = $prices['margin_type'];
            $final_prices['quantity_volume'] = $prices['quantity_volume'] ?? [];
            $final_prices['is_enabled'] = $prices['is_enabled'];
            $final_prices['enable_volume_breakup'] = $prices['enable_volume_breakup'];
            $final_prices['margin_option'] = $prices['margin_option'];
            $final_prices['sale_price'] = $prices['sale_price'];
            $final_prices['pid'] = $prices['pid'];
            $final_prices['price'] = [];

            foreach ($prices['price'] as $product_price) {
                $product_qty_cost = [];

                $tier_price = $product_price['tier_price'] ?? 0;
                // $tier_price_rrp = $product_price['tier_price_rrp'] ?? 0;
                // $tier_price_carriage = $product_price['tier_price_carriage'] ?? 0;

                $product_qty_cost['master_cost'] = $tier_price;
                $product_qty_cost['tier_qty'] = $product_price['tier_price_qty'] ?? 0;

                array_push($final_prices['price'], $product_qty_cost);
            }

        }

        return $final_prices;
    }

    public function updateProductStatus($data)
    {
        $updated_product = $this->admin->updateProductStatus($data);
        $updated_product['status'] = !empty($updated_product)?1:false;
        $updated_product['orders'] = $updated_product['orders'] ?? 0;
        $updated_product['views'] = $updated_product['views'] ?? 0;
        $updated_product['sale_price'] = $updated_product['sale_price'] ?? 0;
        return $updated_product;
    }


    public function updateProductInfo($data)
    {
        return $product_info = $this->admin->updateProductInfo(json_decode($data));
    }

    /**
     * This function find seller and sent verification email
     * @param $id
     * @return |null
     */
    public function sendVerificationEmailToSeller($id)
    {
        return $this->admin->sendVerificationEmailToSeller($id);
    }

    /**
     * This function find seller verification manually
     * @param $id
     * @return |null
     */
    public function sellerVerificationManual($id)
    {
        return $this->admin->sellerVerificationManual($id);
    }

    
    public function getPromotions()
    {
        return $this->admin->getPromotions();
    }

    public function allAttributes()
    {
        $attributes = $this->admin->allAttributes();

        return $attributes;
    }

    public function allSelectedAttributes ($data) {
        $attributes = $this->admin->allSelectedAttributes($data);

        return $attributes;
    }

    protected function formatAttributeData ($attributes) {
        $final_attributes = [];

        foreach ($attributes as $attribute) {

            $final_attribute = [];

            $final_attribute['id'] = $attribute['id'];
            $final_attribute['attribute_id'] = $attribute['attribute_id'];
            $final_attribute['attribute_name'] = $attribute['attribute_name'];
            $final_attribute['attribute_value'] = $attribute['attribute_value'];
            $final_attribute['global_attribute_id'] = $attribute['global_attribute_id'];
            $final_attribute['product_sku'] = $attribute['sku']['sku'];
            $final_attribute['is_selected'] = $attribute['is_selected'];

            array_push($final_attributes, $final_attribute);
        }

        return $final_attributes;
    }

    public function selectAttributes($data) {
        $selected_attributes = $this->admin->selectAttributes($data);

        return $selected_attributes;
    }

    public function saveProductAttributeName($data) {
        $product_attribute_name = $this->admin->saveProductAttributeName($data);
        return $product_attribute_name;
    }

    public function removeProductAttributeName($data) {
        $product_attribute_name = $this->admin->removeProductAttributeName($data);
        return $product_attribute_name;
    }

    public function getProductAttributeName($data) {
        $product_attribute_name = $this->admin->getProductAttributeName($data);
        return $product_attribute_name;
    }

    public function saveAttributes($data)
    {
        return $updated_attributes = $this->admin->saveAttributes($data);
    }

    public function generateVariations($data)
    {
        return $updated_variations = $this->admin->generateVariations($data);
    }

    public function updateProductMargin($data)
    {
        return $updated_margin = $this->admin->updateProductMargin($data);
    }

    public function updateVariations($data)
    {
        return $updated_variations_margin = $this->admin->updateVariations($data);
    }

    public function deleteVariations($data)
    {
        return $deleted_variations = $this->admin->deleteVariations($data);
    }

    public function getProductDescription($data)
    {
        return $description_data = $this->admin->getProductDescription($data);
    }

    public function saveProductDescription($data)
    {
        return $description_data = $this->admin->saveProductDescription($data);
    }

    public function saveProductPrintLocation($data)
    {
        return $print_data = $this->admin->saveProductPrintLocation($data);
    }

    public function setDefaultProductPrintLocation($data)
    {
        return $print_data = $this->admin->setDefaultProductPrintLocation($data);
    }

    public function getProductAdditionalnfo($data)
    {
        return $additional_data = $this->admin->getProductAdditionalnfo($data);
    }

    public function saveProductAdditionalnfo($data)
    {
        return $additional_data = $this->admin->saveProductAdditionalnfo($data);
    }

    public function getVariations($data)
    {
        return $variations_data = $this->admin->getVariations($data);
    }

    public function updateSellerInfo($data, $update_type)
    {

        $seller = array();

        switch ($update_type) {
            case 'detail':
                $seller = $this->admin->saveSellerDetail($data);
                break;

            case 'status':
                $seller = $this->admin->saveSellerStatus($data);
                break;

            default:
                $seller = [];

        }
        $final_seller = $seller;
        if (isset($seller['bill_address'])) {
            $final_seller['state'] = $final_seller['bill_address']['state'];
            $final_seller['country'] = $final_seller['bill_address']['country'];
            $final_seller['city'] = $final_seller['bill_address']['city'];
            $final_seller['zipcode'] = $final_seller['bill_address']['zipcode'];
            $final_seller['address_line_1'] = $final_seller['bill_address']['address_line_1'];
            $final_seller['address_line_2'] = $final_seller['bill_address']['address_line_2'];
            $final_seller['email'] = $final_seller['bill_address']['email'];
            unset($final_seller['bill_address']);
        }

        return $final_seller;
    }

    public function getSellers($filters)
    {
        $orders_amount_till_month = isset($filters->orders_amount_till_month)?$filters->orders_amount_till_month:array();
        $orders_amount_till_date = isset($filters->orders_amount_till_date)?$filters->orders_amount_till_date:array();
        $date = isset($filters->date)?$filters->date:array();
        $no_stores_created = isset($filters->no_stores_created)?$filters->no_stores_created:array();
        $page_limit = isset($filters->page_limit)?$filters->page_limit:30;
        $seller_name = isset($filters->seller_name)?$filters->seller_name:'';
        $type = isset($filters->type)?$filters->type:'';

        if($type == 'seller'){
            $query = $this->admin->getSellers()->map->append('is_seller_status');
        }else{
            $query = $this->admin->getSellers();
        }


        $productData = collect($query);

        if (isset($date[1]) && !empty($date[1])) {
            $productData = $productData->whereBetween('created_at', [$date[0],$date[1]])->values();
        }

        if (isset($no_stores_created[1]) && $no_stores_created[1] > 0) {
            $productData = $productData->whereBetween('no_stores_created', [(int)$no_stores_created[0], (int)$no_stores_created[1]])->values();
        }

        if (isset($orders_amount_till_date[1]) && $orders_amount_till_date[1] > 0) {
            $productData = $productData->whereBetween('orders_amount_till_date', [(int)$orders_amount_till_date[0], (int)$orders_amount_till_date[1]])->values();
        }

        if (isset($orders_amount_till_month[1]) && $orders_amount_till_month[1] > 0) {
            $productData = $productData->whereBetween('orders_amount_till_month', [(int)$orders_amount_till_month[0], (int)$orders_amount_till_month[1]])->values();
        }


        if (isset($seller_name) && !empty($seller_name)) {
            $productData = $productData->filter(function($value) use ($seller_name) {
                return (stristr($value->first_name, $seller_name) || stristr($value->last_name, $seller_name));

            })->values();
        }

        $rangeValues=[];
        $rangeValues['min_no_stores_created'] = $productData->min('no_stores_created');
        $rangeValues['max_no_stores_created'] = $productData->max('no_stores_created');
        $rangeValues['min_orders_amount_till_date'] = $productData->min('orders_amount_till_date');
        $rangeValues['max_orders_amount_till_date'] = $productData->max('orders_amount_till_date');
        $rangeValues['min_orders_amount_till_month'] = $productData->min('orders_amount_till_month');
        $rangeValues['max_orders_amount_till_month'] = $productData->max('orders_amount_till_month');
        $formattedKey = null;


        //is_enabled is inverted i.e. 0 is for ON, and 1 is for OFF
        if (isset($filters->all()['sort_type'])) {
            $sort_type = $filters->all()['sort_type'];
            $key = $filters->all()['sort_by_key'];

            if($key == 'state_name' || $key == 'city'){
                $formattedKey = 'billAddress.'.$key;
            } else if($key == 'plan_name'){
                $formattedKey = 'plan.'.$key;
            }
            else {
                $formattedKey = $key;
            }

            if ($sort_type == 0) {
                $productData = $productData->sortBy($formattedKey)->values();
            } else if ($sort_type == 1) {
                $productData = $productData->sortByDesc($formattedKey)->values();
            }
        }

        $data = $productData->paginate($page_limit)->toArray();

        return  array($data,$rangeValues);
    }


    public function sellerByID($seller_id)
    {
        $seller = $this->admin->sellerByID($seller_id);
       return $seller;
    }

     public function getStoreArrCount($datas,$type='seller_store_status')
    {
            $results=array();
            foreach ($datas as $tag) {
               if(isset($tag[$type]['name']) && $tag[$type]['name'] =='Enabled'){
                  $results[] =  $tag[$type]['id'];
               }
            }

       return count($results);
    }

//    public function createMasterSystemUser(array $data)
//    {
//        $res = $this->admin->createMasterSystemUser($data);
//        return $res;
//    }
//
//    public function createSellerSystemUser(array $data)
//    {
//        $res = $this->admin->createSellerSystemUser($data);
//        return $res;
//    }

    public function getRoles($request, $mode)
    {
        return $this->admin->getRoles($request, $mode);
    }

    public function getRoleWithPermissions($id,$mode)
    {
        $data = $this->admin->getRoleWithPermissions($id,$mode);
        return $data;
    }

    public function setDefaultPermission(array $data,int $id)
    {
        foreach ($data as $key => $value)
        {
            $data[$key]['role_id'] = $id;
        }
        return $this->admin->setDefaultPermission($data);
    }

    public function addGlobalAttributeData ($data, $type) {
        switch ($type) {
            case 'name':
                return $this->admin->addGlobalAttributeName($data);
                break;

            case 'value':
                return $this->admin->addGlobalAttributeValue($data);
                break;

            default:
                break;
        }
    }

    public function updateGlobalAttributeData ($data, $type) {
        switch ($type) {
            case 'name':
                return $this->admin->updateGlobalAttributeName($data);
                break;

            case 'value':
                return $this->admin->updateGlobalAttributeValue($data);
                break;

            default:
                break;
        }
    }

    public function deleteGlobalAttributeData ($data, $type) {
        switch ($type) {
            case 'name':
                return $this->admin->deleteGlobalAttributeName($data);
                break;

            case 'value':
                return $this->admin->deleteGlobalAttributeValue($data);
                break;

            default:
                break;
        }
    }

    public function fetchGlobalAttributeData ($data) {
        return $this->admin->getGlobalAttrData($data);
    }

    public function selectVariationImages ($data) {
        return $this->admin->selectVariationImages($data);
    }

    public function getImageViews ($data) {
        return $this->admin->getImageViews($data);
    }

    public function updateProductImagesData ($data) {
        return $this->admin->updateProductImagesData($data);
    }

    public function updateProductPrintLocation ($data) {
        return $this->admin->updateProductPrintLocation($data);
    }

    public function removeProductPrintLocation ($data) {
        return $this->admin->removeProductPrintLocation($data);
    }

    public function getCustomerData($request) {
        return $this->admin->getCustomerData($request);
    }

    public function getCustomerDataByID($store_id,$customer_id) {
        return $this->admin->getCustomerDataByID($store_id,$customer_id);
    }

    public function applyFilters($filters,$slug)
    {
        $res = null;
        switch ($slug){
            case "products":
                $res = Products::filter($filters)->get();
                break;
            case "categories":
                $res = Category::filter($filters)->get();
                break;
            case "sellers":
                $res = SellerUser::filter($filters)->get();
                break;
            case "vendors":
                $res = Vendor::filter($filters)->get();
                break;
            case "admins":
                $res = User::filter($filters)->get();
                break;
            case "orders":
                $res = Orders::filter($filters)->get();
                break;
            case "banners":
                $res = CmsBanner::filter($filters)->get();
                break;
            default:
                $res = null;
        }
        return $res;
    }

    protected function setValidationRules() {
        $this->validationRules = [
            'addUser' => [
                'email' => ['required'],
                'is_enable' => ['required'],
                'role_id' => ['required'],
                'permissions' => ['required']
            ],
            'setPermission' => [
                'menu_id' => ['required']
            ],
            'saveGlobalAttribute' => [
                'name' => ['required'],
                'value' => ['nullable'],
                'display_value' => ['nullable'],
                'sort_order' => ['nullable']
            ],
            'getAttributeData' => [
                'name' => ['required']
            ],
            'updateGlobalAttributeName' => [
                'old_name' => ['required'],
                'name' => ['required']
            ],
            'updateGlobalAttributeValue' => [
                'id' => ['required'],
                'value' => ['nullable'],
                'display_value' => ['required'],
                'sort_order' => ['nullable']
            ],
            'deleteGlobalAttributeName' => [
                'names' => ['required']
            ],
            'deleteGlobalAttributeValue' => [
                'ids' => ['required']
            ],
            'fetchVariations' => [
                'pid' => ['required']
            ],
            'saveVariationImages' => [
                'id' => ['required'],
                'media_ids' => ['nullable']
            ],
            'fetchImageViews' => [
                'pid' => ['required']
            ],
            'updateProductImages' => [
                'image_data' => ['nullable']
            ],
            'updateProductPrintLocation' => [
                'id' => ['required'],
                'image_view' => ['required'],
                'area_name' => ['required'],
                'area_x' => ['required'],
                'area_y' => ['required'],
                'image_area_width' => ['required'],
                'image_area_height' => ['required']
            ],
            'removeProductPrintLocation' => [
                'id' => ['required']
            ]
        ];
    }
}
