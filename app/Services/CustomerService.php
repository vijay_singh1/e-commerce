<?php
/**
 * Created by PhpStorm.
 * User: surbhi
 * Date: 7/2/19
 * Time: 12:51 PM
 */

namespace App\Services;


use App\Filters\FilterableTrait;
use App\Model\SellerUser;
use App\Model\Startup;
use App\Model\User;
use App\Model\Customers;
use App\Repositories\SellerUserRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use test\Mockery\MagicParams;

class CustomerService
{
    use FilterableTrait;

    /**
     * The filter options. This property is used when associated model filters are used.
     * Keys:-
     * - `model` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     * - `resolve` : array : The list of filters that belong to a related model
     *        -    `relation` : string : The relation name with the existing model i.e. The name of function in which the model-model (has*, belongsTo) is written.
     *        -    `package` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     *        -    `class` : string : The class to which the filter belongs
     *        -    `args` : array : Key-value pair of the list of arguments to pass to the filter along with the value received
     * - `required` : array : Contains the list of filters that are to be executed at all times. Key-value pair, with the key being the filter name and the value to be provided to the filter.
     * @var array
     */
    private static $filterMap = [
        'model' => 'Customers',
        'resolve' => [
            'id' => [
                'package' => 'Customers',
                'class' => 'Id'
            ],
            'ids' => [
                'package' => 'Customers',
                'class' => 'Ids'
            ],
            'name' => [
                'package' => 'Customers',
                'class' => 'Name'
            ],
            'store_id' => [
                'package' => 'Customers',
                'class' => 'Store'
            ],
            'seller_id' => [
                'package' => 'Customers',
                'class' => 'Seller'
            ],

            
        ],
        /*
         * The list of filters that always need to be executed
         */
        'required' => [
//            'sort' => 'created',
//            'allowed_roles'=> ''
        ]
    ];

   
    public function __construct(SellerUserRepository $user) {
       
    }

    public static function filter(Request $filters) {
        $key_by_search = isset($filters->key_by_search)?$filters->key_by_search:'';
        $query = (new Customers)->newQuery()->with('address');
        $query = static::applyFilters($filters, $query)->get();

        $productData = collect($query);

        if (isset($key_by_search) && !empty($key_by_search)) {
            $productData = $productData->filter(function($value) use ($key_by_search) {
                $store_name = isset($value->customer_store_details['store_name'])?$value->customer_store_details['store_name']:'';
                $net_amount = isset($value->customer_store_details['net_amount'])?$value->customer_store_details['net_amount']:'';
                $city = isset($value->address[0]->city)?$value->address[0]->city:'';
                $state_name = isset($value->address[0]->state_name)?$value->address[0]->state_name:'';
                return (stristr($value->id, $key_by_search) || 
                stristr($store_name, $key_by_search) ||
                stristr($value->name, $key_by_search) ||
                stristr($value->email, $key_by_search))||
                stristr($city, $key_by_search) ||
                stristr($state_name, $key_by_search) ||
                stristr($net_amount, $key_by_search);
                   
            })->values();
        }

        $formattedKey = null;

        if (isset($filters->all()['sort_type'])) {
            $sort_type = $filters->all()['sort_type'];
            $key = $filters->all()['sort_by_key'];

            if($key == 'store_name' || $key == 'net_amount'){
                $formattedKey = 'customer_store_details.'.$key;
            } 
            else if($key == 'state_name' || $key == 'city'){

                if($sort_type == 0){
                    $sorted = $productData->sortBy(function ($productData) {
                        return $productData->address[0]->city;
                    });
        
                } else if ($sort_type == 1){
                    $sorted = $productData->sortByDesc(function ($productData) {
                        return $productData->address[0]->city;
                    });
                }
                return $sorted->values()->paginate(30);
            } 
            else {
                $formattedKey = $key;
            }

            if ($sort_type == 0) {
                $productData = $productData->sortBy($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            } else if ($sort_type == 1) {
                $productData = $productData->sortByDesc($formattedKey, SORT_NATURAL|SORT_FLAG_CASE)->values();
            }
        }

        return $productData->paginate(30);
    }

   
}