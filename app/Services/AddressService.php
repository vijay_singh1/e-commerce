<?php

namespace App\Services;
use App\Repositories\AddressRepository;

class AddressService {

    use ValidatorTrait;

    protected $default_repository;
    public function __construct(AddressRepository $default_repository) {
        $this->default_repository = $default_repository;
        $this->setValidationRules();
    }


    protected function setValidationRules() {
        $this->validationRules = [];
    }
    
    public function addressList($data) {
        return $this->default_repository->addressList($data);
    }

    public function addressAdd($data) {
        return $this->default_repository->addressAdd($data);
    }

    public function addressEdit($data) {
        return $this->default_repository->addressEdit($data);
    }

    public function addressDelete($id) {
        return $this->default_repository->addressDelete($id);
    }

}