<?php

namespace App\Services;

use App\Model\Media;
use App\Model\SellerStore;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic as ImageManagerStatic;
use Aws\S3\Exception\S3Exception as s3;
use Illuminate\Support\Facades\Log;
use const Bytes;

class MediaService
{

	use ValidatorTrait;

	/**
	 * MediaService constructor.
	 */
	public function __construct()
	{
		$this->setValidationRules();
	}

	/**
	 * Set the Validation Rules
	 */
	protected function setValidationRules()
	{
		$this->validationRules = [
			'media' => [
				//				'media' => ['required','image'],
				'resource_id' => ['required'],
				'resource_type' => ['required'],
				'tags' => ['required'],
				'image_view' => ['nullable'],
				'image_option' => ['nullable'],
				'media' => ['mimes:jpeg,jpg,png,dst']
			]
		];
	}

	/**
	 * Generates storage path
	 * @param string $folder
	 * @param int|null $resourceId
	 * @param string|null $subFolder
	 * @return string
	 */
	public function generateStoragePath(string $folder, int $resourceId = 0, string $subFolder = null): string
	{
		switch ($folder) {
			case 'seller-store-logo':
				$path = 'seller-store-logo' . DIRECTORY_SEPARATOR;
				break;

			case 'seller-store-product':
				$path = 'seller-store-product' . DIRECTORY_SEPARATOR;
				break;

			case 'seller-store-dst':
				$path = 'seller-store-dst' . DIRECTORY_SEPARATOR;
				break;
	

			case 'product':
				$path = 'product' . DIRECTORY_SEPARATOR;
				break;

			case 'category':
				$path = 'seller-category' . DIRECTORY_SEPARATOR;
				break;

			case 'seller-product':
				$path = 'product' . DIRECTORY_SEPARATOR;
				break;

			case 'seller-category':
				$path = 'seller-category' . DIRECTORY_SEPARATOR;
				break;

			case 'cms_banner':
				$path = 'cms_banner' . DIRECTORY_SEPARATOR;
				break;

			case 'seller_cms_banner':
				$path = 'seller_cms_banner' . DIRECTORY_SEPARATOR;
				break;

			case 'seller-user':
				$path = 'seller-user' . DIRECTORY_SEPARATOR;
				break;

			case 'promotional_ads_1':
				$path = 'promotional_ads_1' . DIRECTORY_SEPARATOR;
				break;

			case 'promotional_ads_2':
				$path = 'promotional_ads_2' . DIRECTORY_SEPARATOR;
				break;


			default:
				// A default path is added, in-case any resourceType not supported is used
				$path = 'unknown' . DIRECTORY_SEPARATOR;
		}

		if (!empty($resourceId)) {
			$path .= $resourceId . DIRECTORY_SEPARATOR;
		}

		if (!empty($subFolder)) {
			$path .= $subFolder . DIRECTORY_SEPARATOR;
		}

		return $path;
	}

	/**
	 * Uploads File to S3 and Database
	 * @param UploadedFile $mediaFile
	 * @param string $resourceType
	 * @param string $resource_id
	 * @param string $tag
	 * @return Media|bool Media Object on success, False on failure
	 */
	public function uploadMedia(UploadedFile $mediaFile, string $resourceType, int $resource_id, string $tag = 'image', string $position_view = null, String $image_view = null, String $image_option = null
	, $image_url = '')
	{
		//get media hash
		$mediaHash = CommonService::generateRandomToken(20, true);
		switch ($resourceType) {

			case 'seller-store-logo':
				if ($tag === 'image-square') {
					$type = 'logo';
				} else if ($tag === 'document') {
					$type = 'document';
				} else if ($tag === 'cover-image') {
					$type = 'cover';
				} else {
					return false;
				}

				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'seller-store-logo';

				$this->deleteStoreLogo($obj);

				$storagePath = $this->generateStoragePath('seller-store-logo', $resource_id, $type);
				break;

			case 'seller-store-product':
				if ($tag === 'image-square') {
					$type = 'logo';
				} else if ($tag === 'document') {
					$type = 'document';
				} else if ($tag === 'cover-image') {
					$type = 'cover';
				} else {
					return false;
				}

				/**
				 * remove existing artwork logo and upload a new one
				 */
				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'seller-store-product';

				$this->deleteStoreLogo($obj);

				$storagePath = $this->generateStoragePath('seller-store-product', $resource_id, $type);
				break;

			case 'seller-store-dst':
				if ($tag === 'image-square') {
					$type = 'logo';
				} else if ($tag === 'document') {
					$type = 'document';
				} else if ($tag === 'cover-image') {
					$type = 'cover';
				} else {
					return false;
				}

				/**
				 * remove existing artwork logo and upload a new one
				 */
				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'seller-store-dst';

				$this->deleteStoreLogo($obj);

				$storagePath = $this->generateStoragePath('seller-store-dst', $resource_id, $type);
				break;

			case 'seller-user':

				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'seller-user';

				$this->deleteStoreLogo($obj);

				$storagePath = $this->generateStoragePath('seller-user', $resource_id);
				break;

			case 'product':
				$storagePath = $this->generateStoragePath('product', $resource_id);
				break;

			case 'cms_banner':
				$storagePath = $this->generateStoragePath('cms_banner', $resource_id);
				break;

			case 'seller_cms_banner':
				$storagePath = $this->generateStoragePath('seller_cms_banner', $resource_id);
				break;

			case 'category':
				$storagePath = $this->generateStoragePath('category', $resource_id);
				break;

			case 'seller-product':
				$storagePath = $this->generateStoragePath('seller-product', $resource_id);
				break;

			case 'seller-category':
				$storagePath = $this->generateStoragePath('seller-category', $resource_id);
				break;

			case 'promotional_ads_1':
				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'promotional_ads_1';
				$this->deleteStoreLogo($obj);
				$storagePath = $this->generateStoragePath('promotional_ads_1', $resource_id);
				break;

			case 'promotional_ads_2':
				$obj['resource_id'] = $resource_id;
				$obj['resource_type'] = 'promotional_ads_2';
				$this->deleteStoreLogo($obj);
				$storagePath = $this->generateStoragePath('promotional_ads_2', $resource_id);

				break;

			default:
				return false;
		}

	    $isUploaded = $this->uploadMediaToBucket($storagePath, $mediaHash, $mediaFile, $tag, $resourceType,$image_url, $resource_id);

		return $isUploaded ? $this->storeMediaInDb($resource_id, $resourceType, $tag, $storagePath, $mediaHash, $mediaFile, $position_view, $image_view, $image_option) : false;
	}

	function range(int $int, int $min, int $max)
	{
		return ($min < $int && $int < $max);
	}

	/**
	 * This function compresses the images
	 * if image lies between 1-2 mb it compresses down to 30% quality
	 * if image is above 3 mb it compresses down to 20% quality
	 * @param UploadedFile $mediaFile
	 * @return bool|UploadedFile|mixed|string
	 */
	private function compressImage(UploadedFile $mediaFile)
	{
		$imageSize = $mediaFile->getSize();
		$resizedImage = $mediaFile;
		if ($this->range($imageSize, 1 * Bytes, 2 * Bytes)) {
			$resizedImage = $this->resizeImage($mediaFile, 30);
		} else if ($imageSize > 3 * Bytes) {
			$resizedImage = $this->resizeImage($mediaFile, 20);
		} else {
			return file_get_contents($resizedImage);
		};
		return $resizedImage;
	}

	/**
	 * This function reduces the dimensions of the cover image for now (can be used for logos if needed in future)
	 * Dimension given for card : 300px with auto height
	 * Dimension given for detail-page : 1300px with auto height
	 * @param UploadedFile $mediaFile
	 * @param int $width
	 * @return mixed
	 */
	private function reduceDimesions(UploadedFile $mediaFile, int $width)
	{
		list($widths, $height) = getimagesize($mediaFile);
		if ($widths > 1500) {
			$resizedImage = Image::make($mediaFile)->resize($width, null, function ($constraint) {
				$constraint->aspectRatio();
			})->encode('jpg');
			return $resizedImage->getEncoded();
		}
		return file_get_contents($mediaFile);
	}

	/**
	 * Stores Media on S3
	 * @IMPORTANT- add new Tags here in the case if introduced
	 * @param string $storagePath
	 * @param string $mediaHash
	 * @param UploadedFile $mediaFile
	 * @param string $tag
	 * @param string $resourceType - e.g. cms_banner
	 * @return bool
	 */
	private function uploadMediaToBucket(string $storagePath, string $mediaHash, UploadedFile $mediaFile, string $tag,
    $resourceType = null,$image_url,$resource_id=null)
    {

		$fileName = $mediaHash . '.' . $mediaFile->getClientOriginalExtension();

		try {
			switch ($tag) {
				case 'image-square':
				    $resizedImage = in_array($resourceType,['cms_banner','seller_cms_banner']) ?
                        $this->resizeAndCompressBannerImage($mediaFile,1400,460)
						: $this->compressImage($mediaFile);

				    if($resourceType == 'seller-store-product'){
						$obj['resource_id'] = $resource_id;
						$obj['resource_type'] = 'seller-store-dst';
						$this->deleteStoreLogo($obj);
						InactiveStoreDSTProducts($resource_id,0);
						if($image_url !=''){
							Storage::put($storagePath . $fileName,file_get_contents($image_url), 'public');
							Storage::put($storagePath . '/thumbnail/' . $fileName, file_get_contents($image_url), 'public');
						}else{
							Storage::put($storagePath . $fileName,$resizedImage, 'public');
							$url = Storage::url($storagePath.$fileName);

							// $remove_background_image = $this->removeBackgroundImage($url);

							// $fileName = $mediaHash . '.' . $mediaFile->getClientOriginalExtension();
                            // if(isset($remove_background_image->preview_image) && !empty($remove_background_image->preview_image)){
							// 	Storage::put($storagePath . $fileName,file_get_contents($remove_background_image->preview_image	), 'public');
							// }else{
								Storage::put($storagePath . $fileName,$resizedImage, 'public');
							// }
							Storage::put($storagePath . '/thumbnail/' . $fileName, $resizedImage, 'public');
							
						}

					}else{
						Storage::put($storagePath . $fileName, $resizedImage, 'public');
						Storage::put($storagePath . '/thumbnail/' . $fileName, $resizedImage, 'public');
					}

					break;
				case 'image-circle':
					//					$resizedImage = $this->resizeImage($mediaFile);
					//					Storage::put($storagePath . $fileName, file_get_contents($mediaFile));
					$resizedImage = $this->compressImage($mediaFile);
					Storage::put($storagePath . $fileName, $resizedImage, 'public');
					break;
				case 'pitch':
					Storage::putFileAs($storagePath, $mediaFile, $fileName);
					break;
				case 'cover-image':
					//					Storage::put($storagePath . $fileName, file_get_contents($mediaFile));
					$cover = $this->reduceDimesions($mediaFile, 1500);
					$thumbnail = $this->reduceDimesions($mediaFile, 300);
					Storage::put($storagePath . $fileName, $cover, 'public');
					Storage::put($storagePath . '/thumbnail/' . $fileName, $thumbnail, 'public');
					break;
				case 'document':
					if($resourceType == 'seller-store-product'){
						Storage::putFileAs($storagePath, $mediaFile, $fileName);
						InactiveStoreDSTProducts($resource_id,1);
					}else{
						Storage::putFileAs($storagePath, $mediaFile, $fileName);
					}
					
					break;
				case 'document-clinic':
					Storage::putFileAs($storagePath, $mediaFile, $fileName);
					break;
				case 'flyer':
					Storage::putFileAs($storagePath, $mediaFile, $fileName);
					break;
				case 'presentation':
					Storage::putFileAs($storagePath, $mediaFile, $fileName);
					break;
			}
		} catch (s3 $exception) {
			Log::error($exception);
			return false;
		}
		return true;
	}

	/**
     * This function resize banner images for custom dimensions.
     * Image quality: 0=poor(small size), 60=medium(medium size) and 100 = best (big size)
     * The default value for quality is 90 if we do not specify it.Here we set quality=60
     * @param UploadedFile $mediaFile
     * @param int $width
     * @param int $height
     * @param int $quality
     * @return string
     */
    private function resizeAndCompressBannerImage(UploadedFile $mediaFile, $width=1400, $height=460, $quality=60)
    {
        $imageSize = $mediaFile->getSize();
        if ($this->range($imageSize, 1 * Bytes, 2 * Bytes)) {
            $quality = 30;
        } else if ($imageSize > 3 * Bytes) {
            $quality = 50;
        }

        // configure with favored image driver (gd by default)
        ImageManagerStatic::configure(array('driver' => 'imagick'));

        ini_set('memory_limit','-1'); // increase memory limit for image processing
        $resizedImage = ImageManagerStatic::make($mediaFile)->resize($width,$height)->encode('jpg', $quality);
        return $resizedImage->getEncoded();
    }

    /**
     * Resize Image Using Intervention
     * @param UploadedFile $data
     * @return mixed
     */
    private function resizeImage(UploadedFile $data, int $quality = 50, int $width = null, int $height = null)
    {
		if (!empty($width) && !empty($height)) {
			$resizedImage = Image::make($data)->resize($width, $height);
		} else {
			$resizedImage = Image::make($data)->encode('jpg', $quality);
		}
		return $resizedImage->getEncoded();
	}


   /**
	 * remove Background image
     * @param url $url
     * @return url
    */
    public function removeBackgroundImage(string $url,int $condition)
    {
		 $curl = curl_init();
		 $imageUrl = urlencode($url);
		 $hexcode = urlencode("#ffffff");
		 $remove_background = urlencode($condition);
		  $api_key=env("BACKGROUND_IMAGE_REMOVAL_API_URL");
		  curl_setopt_array($curl, array(
		  CURLOPT_URL => env("BACKGROUND_IMAGE_REMOVAL_API_URL"),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "imageUrl=$imageUrl&hexcode=$hexcode&remove_background=$remove_background",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Authorization:basic SU12ak1abmh1KlIjOm4kQnpwRm1FZFMwYQ==",
		  ),
		)
	   );
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return false;
		} else {
		  return json_decode($response);
		}
	}

	/**
	 * Stores Data in DB once successfully stored on S3
	 * @param int $resource_id
	 * @param string $resource_type
	 * @param string $tag
	 * @param string $storagePath
	 * @param string $mediaHash
	 * @param UploadedFile $mediaFile
	 * @return Media|bool
	 */
	private function storeMediaInDb(int $resource_id, string $resource_type, string $tag, string $storagePath, string $mediaHash, UploadedFile $mediaFile, string $position_view = null, String $image_view = null, String $image_option = null)
	{
		$meta = [
			'size' => $mediaFile->getSize(),
			'mime' => $mediaFile->getMimeType(),
			'extension' => $mediaFile->getClientOriginalExtension()
		];
		//upload image in image table || meta = {mime_type,extension,size}

		$media = new Media();
		//$media->where('relation_id',$resource_id)->where('relation_type',$resource_type)->delete();
		$media->file_name = $mediaFile->getClientOriginalName();
		$media->relation_id = $resource_id;
		$media->relation_type = $resource_type;
		$media->meta = $meta;
		$media->media_hash = $mediaHash;
		$media->storage_path = $storagePath;
		$media->tags = $tag;
		$media->position_view = $position_view;
		$media->status = 'pending';
		$media->image_view = $image_view;
		$media->image_option = $image_option;
		$saved = $media->save();
		return $saved ? $media : false;
	}

	public function getProductImages($pid, $view)
	{
		$res = Media::where('relation_id', $pid)
			->where('relation_type', 'product')
			->where('image_view', strtolower($view))
			->get()->first();
		return $res;
	}

	public function getSpecificMedia()
	{
		$conditions = [
			'seller-store-logo',
			'seller-store-product'
		];


		$ids = SellerStore::where('logo_arts', '=', '1')
			->orWhere('product_art_id', 1)
			->pluck('id');
		$res = Media::whereIn('relation_type', $conditions)
			->whereIn('relation_id', $ids)->get();
		return $res;
	}

	public function getMedia($type, $id)
	{
		$res = Media::where('relation_type', $type)
			->where('relation_id', $id)
			->get()->toArray();

		return $res;
	}

	public function updateMediaStatus($id, $status)
	{
		$diff = null;
		if ($status == 'completed') {
			$time = Media::where('id', $id)->pluck('requested_date')->toArray();
			$nowtime = Carbon::now();
			$diff = $nowtime->diffInSeconds($time[0]);
			$diff = gmdate('H:i:s', $diff);
		}
		$res = Media::where('id', $id)->update([
			'status' => $status,
			'turn_around_time' => $diff
		]);
		return $res;
	}

	public function deleteStoreLogo($data)
	{
		$res = Media::where('relation_type', $data['resource_type'])
			->where('relation_id', $data['resource_id'])
			->delete();
		return $res;
	}
}
