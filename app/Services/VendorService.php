<?php

namespace App\Services;

use App\Filters\FilterableTrait;
use App\Model\Category;
use App\Model\Vendor;
use App\Model\Address;
use App\Model\Statuses;
use App\Repositories\VendorRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class VendorService
{

    use ValidatorTrait,
        FilterableTrait;

    /**
     * The filter options. This property is used when associated model filters are used.
     * Keys:-
     * - `model` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     * - `resolve` : array : The list of filters that belong to a related model
     *        -    `relation` : string : The relation name with the existing model i.e. The name of function in which the model-model (has*, belongsTo) is written.
     *        -    `package` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     *        -    `class` : string : The class to which the filter belongs
     *        -    `args` : array : Key-value pair of the list of arguments to pass to the filter along with the value received
     * - `required` : array : Contains the list of filters that are to be executed at all times. Key-value pair, with the key being the filter name and the value to be provided to the filter.
     * @var array
     */
    private static $filterMap = [
        'model' => 'Vendor',
        'resolve' => [
            'id' => [
                'package' => 'Vendor',
                'class' => 'Id'
            ],
            'payment' => [
                'relation' => 'payments',
                'package' => 'Payments',
                'class' => 'Payment',
                'args' => ['relation_type' => 'vendor']
            ],
            'payment_status' => [
                'relation' => 'payments',
                'package' => 'Payments',
                'class' => 'PaymentStatus',
                'args' => ['relation_type' => 'vendor']
            ],
            'name' => [
                'package' => 'Vendor',
                'class' => 'Name'
            ],
            'status' => [
                'package' => 'Vendor',
                'class' => 'Status'
            ]
        ],
        /*
         * The list of filters that always need to be executed
         */
        'required' => [
            'sort' => 'created'
        ]
    ];

    protected $address, $vendorRepository;

    public function __construct(VendorRepository $vendorrepository, Address $address)
    {
        $this->setValidationRules();
        $this->vendorRepository = $vendorrepository;
        $this->address = $address;
    }

    public static function filter(Request $filters)
    {
        $query = (new Vendor)->newQuery()->with(['payments', 'payments.order']);
        $query = static::applyFilters($filters, $query);
        return $query->get();
    }

    public function addVendorAddresses(array $data)
    {
        $result = $this->address->create($data);
        return $result;
    }

    public function updateVendorAddresses(array $data)
    {
        $this->address->where('id', $data['id'])->delete();
        $result = $this->address->create($data);
        return $result;
    }

    public function getCategoryProductList($id)
    {
        $seller_store = $this->seller_store->getCategoryProductList($id);
        return $seller_store;
    }

    public function updateVendorDetail($data)
    {
        return  $this->vendorRepository->updateVendorDetail($data);
    }

    public function getVendorDetails($data)
    {
        $vendor_details =  $this->vendorRepository->getVendorDetails($data);
        if (isset($vendor_details->get_vendor_category) && !empty($vendor_details->get_vendor_category)) {
            $collection = new Collection($vendor_details->get_vendor_category);
            $vendor_details['total_eanble_product'] = $collection->sum('product_enable_count');
            $vendor_details['total_product'] = $collection->sum('product_count');
        } else {
            $vendor_details['total_eanble_product'] = 0;
            $vendor_details['total_product'] = 0;
        }
        return $vendor_details;
    }

    public function lastVendorPriceUpdate()
    {
        return  $this->vendorRepository->lastVendorPriceUpdate();
    }

    public function vendorStatusUpdate($data)
    {
        return  $this->vendorRepository->vendorStatusUpdate($data);
    }


    public function addUpdateStatus(array $data)
    {
        $statuses = new Statuses;
        if (isset($data['type']) && $data['type'] == 'insert') {
            unset($data['type']);
            return $statuses->create($data);
        } else {
            $sellerstore = $statuses::find($data['status_id']);
            if (!empty($sellerstore)) {
                if (isset($data['name']) && $data['name'] != '') {
                    $sellerstore->name = isset($data['name']) ? $data['name'] : 'Pending';
                } else {
                    $sellerstore->name = isset($sellerstore->name) ? $sellerstore->name : 'Pending';
                }
                $sellerstore->status_type = isset($sellerstore->status_type) ? $sellerstore->status_type : 'Store';
                return $sellerstore->update();
            }
        }
    }


    public function getAllVendors($filters)
    {
        $data = $this->vendorRepository->getAllVendors($filters);
        $open_orders = isset($filters->open_orders) ? $filters->open_orders : array();
        $sales_to_date = isset($filters->sales_to_date) ? $filters->sales_to_date : array();
        $sales_to_month = isset($filters->sales_to_month) ? $filters->sales_to_month : array();

        $vendor_name = isset($filters->vendor_name) ? $filters->vendor_name : '';
        $status = isset($filters->status) ? $filters->status : '';

        $page_limit = isset($filters->page_limit) ? $filters->page_limit : 30;
        $productData = collect($data);

        // dd($filters->vendor_name, $filters->open_orders, $filters->sales_to_date);

        if (isset($open_orders[1]) && !empty($open_orders[1])) {
            $productData = $productData->whereBetween('open_orders', [$open_orders[0],  $open_orders[1]])->values();
        }

        if (isset($sales_to_date[1]) && !empty($sales_to_date[1])) {
            $productData = $productData->whereBetween('orders_amount_till_date', [$sales_to_date[0],  $sales_to_date[1]])->values();
        }

        if (isset($sales_to_month[1]) && !empty($sales_to_month[1])) {
            $productData = $productData->whereBetween('orders_amount_till_month', [$sales_to_month[0],  $sales_to_month[1]])->values();
        }

        if (isset($status) && $status != '') {
            $productData = $productData->where('is_enabled', $status)->values();
        }

        if (isset($vendor_name) && !empty($vendor_name)) {
            $productData = $productData->filter(function ($value) use ($vendor_name) {
                return (stristr($value->vendor_name, $vendor_name));
            })->values();
        }

        $formattedKey = null;

        if (isset($filters->all()['sort_type'])) {
            $sort_type = $filters->all()['sort_type'];
            $key = $filters->all()['sort_by_key'];

            $formattedKey = $key;

            if ($sort_type == 0) {
                $productData = $productData->sortBy($formattedKey)->values();
            } else if ($sort_type == 1) {
                $productData = $productData->sortByDesc($formattedKey)->values();
            }
        }

        $filtered_data = $productData->paginate($page_limit)->toArray();

        $rangeValues = [];
        $rangeValues['min_open_orders'] =  number_format($productData->min('open_orders'), 2, '.', "");
        $rangeValues['max_open_orders'] = number_format($productData->max('open_orders'), 2, '.', "");

        $rangeValues['min_sales_to_date'] = number_format($productData->min('orders_amount_till_date'), 2, '.', "");
        $rangeValues['max_sales_to_date'] = number_format($productData->max('orders_amount_till_date'), 2, '.', "");

        $rangeValues['min_sales_to_month'] = number_format($productData->min('orders_amount_till_month'), 2, '.', "");
        $rangeValues['max_sales_to_month'] = number_format($productData->max('orders_amount_till_month'), 2, '.', "");

        return array($filtered_data, $rangeValues);
    }


    /**
     * Override the trait method
     */
    protected function setValidationRules()
    {
        $this->validationRules = [
            'updateVendorInfo' => [
                'vendor_id' => ['required'],
                'business_name' => ['required'],
                'business_EIN' => [''],
                'first_name' => ['required'],
                'last_name' => [''],
                'email' => ['required'],
                'contact' => [''],
                'vendor_payout_type' => ['required'],
                'vendor_payout_value' => ['nullable'],
                'country' => ['required'],
                'city' => ['required'],
                'state' => ['required'],
                'zipcode' => ['required'],
                'acc_type' => [''],
                'acc_holder' => [''],
                'bank' => [''],
                'bank_branch' => [''],
                'bank_acc_no' => [''],
                'bank_routing_no' => [''],
                'is_active_ach_acc' => [''],
                'address_line1' => ['required'],
                'address_line2' => [''],
                'time_zone' => ['required'],
                'bank_iban' => [''],
                'margin_markup_override_option' => ['required'],
                'margin_markup_override_type' => ['required'],
                'margin_markup_override_value' => ['required'],
                'enable_volume_breakup' => ['required'],
                'qty_volume_breakup' => ['required']
            ],

        ];
    }
}
