<?php

namespace App\Services;

use App\Filters\FilterableTrait;
use App\Repositories\BannerRepository;
use Illuminate\Http\Request;

class BannerService
{
    use FilterableTrait;

    /**
     * The filter options. This property is used when associated model filters are used.
     * Keys:-
     * - `model` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     * - `resolve` : array : The list of filters that belong to a related model
     *        -    `relation` : string : The relation name with the existing model i.e. The name of function in which the model-model (has*, belongsTo) is written.
     *        -    `package` : string : The name of the model to which the filters primarily belong i.e. the name provided would be used as the package name in the filter namespace to look for the filter class.
     *        -    `class` : string : The class to which the filter belongs
     *        -    `args` : array : Key-value pair of the list of arguments to pass to the filter along with the value received
     * - `required` : array : Contains the list of filters that are to be executed at all times. Key-value pair, with the key being the filter name and the value to be provided to the filter.
     * @var array
     */
    private static $filterMap = [
        'model' => 'Category',
        'resolve' => [
            'sku' => [
                'package' => 'Category',
                'class' => 'Sku',
            ],
            'status' => [
                'package' => 'Category',
                'class' => 'Status',
            ],
            'cost' => [
                'package' => 'Category',
                'class' => 'CostRange',
            ],
            'category' => [
                'package' => 'Category',
                'class' => 'Category',
            ],
            'supplier_company_ref_name' => [
                'package' => 'Category',
                'class' => 'SupplierCompanyRefName',
            ],
        ],
        /*
         * The list of filters that always need to be executed
         */
        'required' => [
            'sort' => 'created',
            'allowed_roles' => '',
        ],
    ];

    public function __construct(BannerRepository $banner)
    {
        $this->banner = $banner;
    }

    public function bannerList(Request $data)
    {
        return $this->banner->bannerList($data);
    }

    public function bannerDetailsByID($data)
    {
        return $this->banner->bannerDetailsByID($data);
    }

}
