<?php

namespace App\Services;

use App\User;
use App\Repositories\UserRepository;


class UserService {

    use ValidatorTrait;

    protected $user;
    public function __construct(UserRepository $user_repository,User $user) {
        $this->user = $user;
        $this->user_repository = $user_repository;
        $this->setValidationRules();
    }

    protected function setValidationRules() {
        $this->validationRules = [
            'loginValidation' => [
                'email' => ['required','email'],
                'password' => ['required','']
            ],
            'signupValidation' => [
                'name' => ['required','string'],
                'email' => ['required','email'],
                'password' => ['required','']
            ]
        ];
    }

    public function filter($filter) {
        return $this->user_repository->getUsers($filter);
    }


}
