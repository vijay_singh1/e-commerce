<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use LogicException;

trait ValidatorTrait {
        
	/**
	 * The rules to be used for data validation. It is a list of rules each grouped under a particular action
	 * @var type
	 */
	protected $validationRules;

	/**
	 * Set the data validation rules
	 */
	protected function setValidationRules() {
		$this->validationRules = [];
	}

	/**
	 * Get the list of validation rules for a given action
	 * @param string $action
	 * @return array
	 * @throws LogicException
	 */
	public function getValidationRules(string $action): array {
		if (!isset($this->validationRules[$action])) {
			throw new LogicException("Action {$action} key not found in the rule-list");
		}
		return $this->validationRules[$action];
	}

	/**
	 * Get the list of keys present in the validation action
	 * @param string $action
	 * @param int $level The nesting level of keys needed
	 * @return array
	 */
	public function getValidationKeys(string $action, int $level = 0): array {
		$keys = array_keys($this->getValidationRules($action));
		$keys = array_filter($keys, function($value) use ($level) {
			return substr_count($value, '.') === $level;
		});
		return $keys;
	}

	/**
	 * Validate the data provided
	 * @param string $action
	 * @param array $data
	 */
	public function validate(string $action, array $data) {
		$rules = $this->getValidationRules($action);
		Validator::make($data, $rules)->validate();
	}

}
