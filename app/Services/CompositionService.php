<?php

namespace App\Services;
use App\Repositories\CompositionRepository;

class CompositionService {

    use ValidatorTrait;

    protected $default_repository;
    public function __construct(CompositionRepository $default_repository) {
        $this->default_repository = $default_repository;
        $this->setValidationRules();
    }

    protected function setValidationRules() {
        $this->validationRules = [
            'addValidation' => [
            'name' => ['required'],
         ],
        'editValidation' => [
            'id' => ['required'],
            'name' => ['required'],
        ]
      ];
    }
    
    public function compositionList($data) {
        return $this->default_repository->compositionList($data);
    }

    public function compositionAdd($data) {
        return $this->default_repository->compositionAdd($data);
    }

    public function compositionEdit($data) {
        return $this->default_repository->compositionEdit($data);
    }

    public function compositionDelete($id) {
        return $this->default_repository->compositionDelete($id);
    }

}