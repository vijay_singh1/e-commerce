<?php

namespace App\Services;
use App\Repositories\CompanyRepository;

class CompanyService {

    use ValidatorTrait;

    protected $default_repository;
    public function __construct(CompanyRepository $default_repository) {
        $this->default_repository = $default_repository;
        $this->setValidationRules();
    }


    protected function setValidationRules() {
        $this->validationRules = [
            'addValidation' => [
            'company_address.address_1' => ['required'],
            'company_address.address_2' => [''],
            'company_address.email' => ['required'],
            'company_address.contact_no' => ['required'],
            'company_address.country_id' => ['required'],
            'basic.company_name' => ['required'],
            'basic.licence' => ['required'],
            'basic.gst_no' => [''],
            'basic.status' => ['required'],
            'company_address.address_type'=>['']
         ],
        'editValidation' => [
            'company_address.address_1' => ['required'],
            'company_address.address_2' => [''],
            'company_address.email' => ['required'],
            'company_address.contact_no' => ['required'],
            'company_address.country_id' => ['required'],
            'company_address.state_id' => ['required'],
            'company_address.city_id' => ['required'],
            'basic.company_name' => ['required'],
            'basic.licence' => ['required'],
            'basic.gst_no' => [''],
            'basic.status' => ['required'],
            'basic.id' => ['required'],
            'company_address.address_type'=>[''],
            'company_address.id'=>['required']
            
        ]
      ];
    }
    
    public function companyList($data) {
        return $this->default_repository->companyList($data);
    }

    public function companyAdd($data) {
        return $this->default_repository->companyAdd($data);
    }

    public function companyEdit($data) {
        return $this->default_repository->companyEdit($data);
    }

    public function companyDelete($id) {
        return $this->default_repository->companyDelete($id);
    }

    public function companyAddressDelete($id) {
        return $this->default_repository->companyAddressDelete($id);
    }

}