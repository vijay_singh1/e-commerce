<?php

namespace App\Filters;

use App\Filters\FilterContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation as EloquentRelation;

class Relation implements FilterContract {

	/**
	 * Stores instances of related queries as a map, which are used for building the final queries. These
	 * queries are then finally applied all at once.
	 * @var array
	 */
	private static $relationQueries = [];

	/**
	 * Returns an instance of a query for the relation specified with the existing model. It resolves and returns the same
	 * only once.
	 *
	 * @param Builder $query The current model query from which the new query should be resolved
	 * @param string $relation The relation
	 * @return Builder Instance of the related query
	 */
	private static function getRelationQuery(Builder $query, string $relation): Builder {

		if (isset(static::$relationQueries[$relation])) {
			return static::$relationQueries[$relation];
		}

		// Get a raw query without an constaraints on it.
		static::$relationQueries[$relation] = EloquentRelation::noConstraints(function () use ($query, $relation) {
				return $query->getModel()->{$relation}()->getQuery();
			});
		return static::$relationQueries[$relation];
	}

	/**
	 * Add the query in the map
	 * @param Builder $query
	 * @param string $relation
	 */
	private static function updateRelationQuery(Builder $query, string $relation) {
		static::$relationQueries[$relation] = $query;
	}

	/**
	 * Apply a given search value to the builder instance.
	 *
	 * @param Builder $query
	 * @param mixed $value
	 * @return Builder
	 */
	public static function apply(Builder $query, $value, ...$args): Builder {
		list($relation, $filterClass) = $args;
		$relatedQuery = self::getRelationQuery($query, $relation);
		$relatedQuery = $filterClass::apply($relatedQuery, $value, ...$args);
		self::updateRelationQuery($relatedQuery, $relation);
		return $query;
	}

	/**
	 * Adds all the queries present in the filter map to the main query provided. This is done, since for each instance when whereHas is called
	 * a new EXISTS query is created in the final query, resulting in several disjoint queries for the same related model, each containing an
	 * individual condition. Each of these individual conditions standing alone would act individually and not collectively and hence
	 * return in-correct results. To prevent it, the individual conditions are added to the instances of queries in the $relationQueries map
	 * which is then added to the whereHas of the original model, thereby creating a single exists query for multiple filters on the same relation
	 * for the model
	 *
	 * @param Builder $query
	 * @return Builder
	 */
	public static function applyRelatedFilters(Builder $query): Builder {
		foreach (static::$relationQueries as $relation => $relationQuery) {
			$query->whereHas($relation, function (Builder $query) use ($relationQuery) {
				return $query->mergeConstraintsFrom($relationQuery);
			});
		}
		return $query;
	}

}
