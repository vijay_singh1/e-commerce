<?php

namespace App\Filters;

use App\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

abstract class Count extends Filter {

	/**
	 * List of operators that are allowed to be used in the count comparison
	 * @var array
	 */
	protected static $operators = ['<', '>', '=', '<=', '>='];

	/**
	 * Determine if the given operator is supported.
	 *
	 * @param  string $operator
	 * @return bool
	 */
	protected static function invalidOperator($operator): bool {
		return !in_array(strtolower($operator), self::$operators, true);
	}

	/**
	 * Extract the column and the operator from the value provided
	 * @param type $value
	 * @return array
	 */
	protected static function getColumnAndOperator($value): array {
		$parts = explode(',', $value);
		// If the given operator is not found in the list of valid operators
		// we will set the operators to '=' and set the values appropriately.
		if (self::invalidOperator($parts[0])) {
			return ['=', $parts[0]];
		}
		return $parts;
	}

	/**
	 * The list of operators for which need to be negated and passed to the query builder
	 * Instead of whereHas, a whereDoesnotHave would be applied
	 */
	const NEGATE_OPERATORS = ['<', '<='];

	/**
	 * List of possible values edge values for which need to be negated and passed to the query builder
	 * Instead of whereHas, a whereDoesnotHave would be applied
	 * @var array
	 */
	const NEGATE_EDGE_VALUE = ['=,0', '0'];

	/**
	 * The negate map, is used to get the negative of the operator
	 * @var array
	 */
	const NEGATE_OPERATOR_MAP = [
		'<' => '>=',
		'<=' => '>',
		'=' => '>',
		'>' => '<=',
		'>=' => '<'
	];

	/**
	 * Checks if the value provided is a negating one or not.
	 * @param string $value
	 * @return bool
	 */
	public static function isNegatingValue($value): bool {
		if (in_array($value, static::NEGATE_EDGE_VALUE)) {
			return true;
		}
		list($operator, $value) = self::getColumnAndOperator($value);
		return in_array($operator, static::NEGATE_OPERATORS);
	}

	/**
	 * Negates the given operator and value using the NEGATE_OPERATOR_MAP
	 * @param string $value
	 * @return string
	 */
	public static function negateColumnAndOperator($value) {
		list($operator, $value) = self::getColumnAndOperator($value);
		return implode(',', [static::NEGATE_OPERATOR_MAP[$operator], $value]);
	}

	/**
	 * Apply a given search value to the builder instance.
	 *
	 * @param Builder $query
	 * @param mixed $value
	 * @return Builder
	 */
	abstract public static function apply(Builder $query, $value, ...$args): Builder;
}
