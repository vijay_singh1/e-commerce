<?php

namespace App\Filters;

/**
 * This contract simply states the fact the filter implemented is dependent on another filter
 */

interface DependentFilter {

}
