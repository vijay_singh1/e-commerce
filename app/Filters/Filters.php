<?php


namespace App\Filters;


use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Filters extends QueryFilters
{

    protected $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function filterQuery($key,$term) {
        return $this->builder->where($key, 'LIKE', "%$term%");
    }

    public function filterBetweenQuery($key,$value)
    {
        return $this->builder->whereBetween($key,$value);
    }

    public function filterRelatedQuery($key,$value,$slug)
    {
        return $this->builder->where($key, 'LIKE', "%$value%")->with($slug);
    }

    public function keywordQuery($value)
    {
        $res = null;
        $model = $this->builder->getModel()->getTable();
        $columns = DB::connection()->getSchemaBuilder()->getColumnListing($model);

        foreach ($columns as $column)
        {
            $res = $this->builder->orWhere($column,'LIKE',"%$value%");
        }
        return $res;
    }
}