<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface FilterContract {

	/**
	 * Apply a given search value to the builder instance.
	 *
	 * @param Builder $query
	 * @param mixed $value
	 * @return Builder
	 */
	public static function apply(Builder $query, $value): Builder;

}
