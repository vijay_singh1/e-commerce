<?php

namespace App\Filters;

use App\Filters\FilterContract;
use App\Model\Startup;
use App\Services\ApplicationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use function config;

trait FilterableCustomTrait {

	/**
	 * Check if the filter provided is present in the resolve list i.e. it is manually resolved by the user.
	 * @param string $name
	 * @return bool
	 */
	private static function isResolvable(string $name): bool {
		return isset(self::$filterMap['resolve'][$name]);
	}

	/**
	 * Check if the filter provided belongs to a related model
	 * @param string $name
	 * @return bool
	 */
	private static function isRelationFilter(string $name): bool {
		return static::isResolvable($name) && isset(self::$filterMap['resolve'][$name]['relation']) && isset(self::$filterMap['resolve'][$name]['package']);
	}

	/**
	 * Check if the filter provided has mentioned a custom class.
	 * @param string $name
	 * @return bool
	 */
	private static function hasCustomClass(string $name): bool {
		return static::isResolvable($name) && isset(self::$filterMap['resolve'][$name]['class']);
	}

	/**
	 * Check if the filter provided has custom list of default arguments that are to be passed down to the filter.
	 * @param string $name
	 * @return bool
	 */
	private static function hasDefaultArguments(string $name): bool {
		return static::isResolvable($name) && isset(self::$filterMap['resolve'][$name]['args']) && is_array(self::$filterMap['resolve'][$name]['args']);
	}

	/**
	 * Returns the package to which the filter belongs. It returns the appropriate package name.
	 * @param string $name
	 * @return string
	 */
	private static function getFilterPackage(string $name): string {
		if (self::isRelationFilter($name)) {
			$package = self::$filterMap['resolve'][$name]['package'];
		} else if (isset(self::$filterMap['resolve'][$name]['package'])) {
			$package = self::$filterMap['resolve'][$name]['package'];
		} else {
			$package = substr(__CLASS__, (strrpos(__CLASS__, '\\') + 1));
		}
		return $package;
	}

	/**
	 * Returns the name of the class to be used for that filter
	 * @param string $name
	 * @return string
	 */
	private static function getFilterClass(string $name): string {
		$package = self::getFilterPackage($name);
		$className = self::hasCustomClass($name) ? self::$filterMap['resolve'][$name]['class'] : $name;

		return config('filter.namespace') . $package . '\\' . ucfirst($className);
	}

	/**
	 * Returns the list of arguments that are provided by default for the filter, which need to be passed down, when the filter is applied
	 * @param string $name
	 * @return array
	 */
	private static function getFilterArguments(string $name): array {
		$args = [];
		if (self::hasDefaultArguments($name)) {
			$args = self::$filterMap['resolve'][$name]['args'];
		}
		return $args;
	}

	/**
	 * Checks if the name provided is a valid filter class or not
	 * @param string $name The name of the filter class
	 * @return bool
	 */
	private static function isValidFilter(string $name): bool {
		return class_exists($name) && is_subclass_of($name, FilterContract::class);
	}

	/**
	 * Apply any relation filters that were found and added to individual query references
	 * @param Builder $query
	 * @return Builder
	 */
	private static function applyRelatedFilters(Builder $query): Builder {
		// Apply all the relation queries that were found that are stored
		$class = config('filter.namespace') . 'Relation';
		return $class::applyRelatedFilters($query);
	}

	/**
	 * Apply the filter
	 * @param Builder $query
	 * @param string $filterName
	 * @param string $filterClass
	 * @param mixed $value
	 * @param array $filters The list of filters used
	 * @return Builder
	 */
	private static function apply(Builder $query, string $filterName, string $filterClass, $value, array $filters): Builder {
		$class = $filterClass;
		$relation = null;
		if (self::isRelationFilter($filterName)) {
			$relation = self::$filterMap['resolve'][$filterName]['relation'];
			$class = config('filter.namespace') . 'Relation';
		}
		$args = self::getFilterArguments($filterName);
		return $class::apply($query, $value, $relation, $filterClass, $args, $filters);
	}

	/**
	 * Get the complete list of filters that are to be applied to the query
	 * @param Request $filters
	 * @return array
	 */	
	private static function getAllFilters(Request $filters): array {
		return ($filters->all()) + (isset(self::$filterMap['required']) ? self::$filterMap['required'] : []);
	}

	/**
	 * Apply all the filters provided in the filters object
	 * @param Request $filters
	 * @param Builder $query
	 */
	protected static function applyFilters(Request $filters, Builder $query): Builder {
		//getAllFilters method gets all the filters from the filter map written in service class
		//getFilterClass gets all the classes using the filter name
		//isValidFilter checks if that filter class exists
		//apply function calls the respective apply functions written in each classes to be applied
		foreach (self::getAllFilters($filters) as $filterName => $value) {
			$filterClass = self::getFilterClass($filterName);
			if (self::isValidFilter($filterClass)) {
				self::apply($query, $filterName, $filterClass, $value, self::getAllFilters($filters));
			}
		}
		 $query = self::applyRelatedFilters($query);
		return $query;
	}

	/**
	 * Add a filter to the list of required filters
	 * @param string $name
	 * @param type $value
	 */
	protected static function addRequiredFilter(string $name, $value) {
		if (!isset(self::$filterMap['required'])) {
			self::$filterMap += ['required' => []];
		}
		self::$filterMap['required'] = array_merge(self::$filterMap['required'], [$name => $value]);
	}

	protected static function addRequiredFilterByArray(array $array) {
		if (!isset(self::$filterMap['required'])) {
			self::$filterMap += ['required' => []];
		}
		self::$filterMap['required'] = array_merge(self::$filterMap['required'], $array);
	}

	public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }
}
