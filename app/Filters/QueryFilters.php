<?php


namespace App\Filters;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class QueryFilters
{
    protected $request;
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $name => $value) {
//            if ( ! method_exists($this, $name)) {
//                continue;
//            }
            if($name == 'keyword')
            {
                if (Str::contains($value, '$')) {
                    $value = str_replace('$', ' ', $value);
                }
                $this->keywordQuery($value);
            }
            else {
                if(is_array($value))
                {
                    if(sizeof($value) == 1)
                    {
                        $value= array_add($value,'1',Carbon::now());
                    }
                    $this->filterBetweenQuery($name,$value);
                }
                else {
                    if (Str::contains($value, '$')) {
                        $value = str_replace('$', ' ', $value);
                    }
                    if (strlen($value)) {
                        $this->filterQuery($name, $value);
                    } else {
                        $this->filterQuery($name);
                    }
                }
            }
            //$slug = "products";
            //$this->filterRelatedQuery($name,$value,$slug);
        }
        return $this->builder;
    }

    public function filters()
    {
        return $this->request->all();
    }

}