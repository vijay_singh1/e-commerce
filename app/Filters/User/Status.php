<?php


namespace App\Filters\Vendor;


use App\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class Status extends Filter
{
    public static function apply(Builder $query, $value): Builder
    {
        switch ($value)
        {
            case 'disabled':
                $query->where('is_enabled',false);
                break;
            case 'enabled':
                $query->where('is_enabled',true);
                break;
            default:
                $query->getModel()->all();
        }
        return $query;
    }
}