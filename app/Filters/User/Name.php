<?php


namespace App\Filters\Vendor;


use App\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class Name extends Filter
{
    public static function apply(Builder $query, $value): Builder
    {
        $query->where('vendor_name','LIKE','%'.$value.'%');
        return $query;
    }
}