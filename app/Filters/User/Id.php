<?php


namespace App\Filters\Vendor;


use App\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class Id extends Filter
{
    public static function apply(Builder $query, $value): Builder
    {
        if(!empty($value)) {
            $query->where('id', $value);
        }
        return $query;
    }
}