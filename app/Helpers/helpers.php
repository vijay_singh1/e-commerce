<?php
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\CountryModel;
use App\Model\StateModel;
use App\Model\CityModel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;
  

if (!function_exists('CheckLoginUser')) {

    function CheckLoginUser($request_data)
    { 
        $store_user_data = $request_data->session()->all();
        return isset($store_user_data['store_user_data']['user_type'])?$store_user_data['store_user_data']['user_type']:'';
    }
}

if (!function_exists('UserLogOut')) {

    function UserLogOut($request_data)
    { 
        $store_user_data = $request_data->session()->all();
        if(isset($store_user_data['store_user_data']['user_type']) && isset($store_user_data['store_user_data']['user_type']) =='admin'){
            return ('/admin/login');
        }else{
            return ('/login');
        }
    }
}

if (!function_exists('GetCountryStateCity')) {

    function GetCountryStateCity($params)
    { 
        $relation_id = $params['relation_id'] ?? null;
        $type = $params['type'] ?? null;

        switch ($type) {
            case COUNTRY:
                $data = CountryModel::get(); 
                break;

            case STATE:
                $data = StateModel::where('country_id', $relation_id)->get();
                break;

            case CITY:
                $data = CityModel::where('state_id', $relation_id)->get();
                break;

            default:
                $data = array();
                break;
        }
        return $data;
    }
}



if (!function_exists('classActiveSegmentParent')) {

    function classActiveSegmentParent($segment, $values)
    {
        if(!empty($values)){
            return (in_array(request()->segment($segment),$values))? 'open' : '';
        }
        return '';
    }
}

if (!function_exists('classActiveOnlySegment')) {

    function classActiveOnlySegment($segment, $value)
    {
        if (!empty($value)) {
            return request()->segment($segment) == $value ? 'active' : '';
        }
        return '';
    }
}