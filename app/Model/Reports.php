<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Products extends Model
{
  
    protected $table = 'products';

    protected $appends = ['category_name'];

    public function getCategoryNameAttribute($key)
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id')->pluck('name')->first();
    }
    
}
