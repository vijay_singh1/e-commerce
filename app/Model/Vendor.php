<?php

namespace App\Model;

use App\Filters\FilterableTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use FilterableTrait;

    protected $table = 'vendor';
    protected $appends = [
        'products_count',
        'active_products_count',
        'orders_amount_till_date',
        'orders_amount_till_month',
        'open_orders',
        'rma_orders',
    ];

    public function vendorQtyBlocks()
    {
        return $this->hasMany('App\Model\VendorQtyVolumeBreakup', 'vendor_id', 'vendor_id');
    }

    public function get_vendor_address()
    {
        return $this->belongsTo('App\Model\Address', 'address_id', 'id')->where("address_type", "vendor");
    }

    public function get_vendor_category()
    {
        return $this->hasMany('App\Model\Category', 'vendor_id', 'vendor_id');
    }

    public function payments()
    {
        return $this->morphMany('App\Model\Payments', 'payment', 'payment_type', 'payment_id');
    }

    public function products()
    {
        return $this->hasMany('App\Model\Products', 'vendor_id', 'vendor_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Model\Orders', 'vendor_id', 'vendor_id');
    }

    public function getProductsCountAttribute()
    {
        $data = $this->products()->count();
        if ($data) {
            return $data;
        }
        return null;
    }

    public function getActiveProductsCountAttribute()
    {
        $data = $this->products()->where('is_enabled', true)->count();
        if ($data) {
            return $data;
        }
        return null;
    }

    public function getOpenOrdersAttribute()
    {
        $date = Carbon::today();
        $vid = $this->vendor_id;
        $orders = OrderItems::select('*', 'id', 'order_id')->has('products')->with(['products' => function ($q) use ($vid) {
            $q->select('vendor_id')->where('vendor_id', $vid);
            return $q;
        }])->groupBy('order_id')->pluck('order_id');

        return Orders::whereIn('id', $orders)
            ->where('status', 'pending')
            ->get()->count();
    }

    public function getRmaOrdersAttribute()
    {
        $date = Carbon::today();
        $vid = $this->vendor_id;
        $orders = OrderItems::select('*', 'id', 'order_id')->has('products')->with(['products' => function ($q) use ($vid) {
            $q->select('vendor_id')->where('vendor_id', $vid);
            return $q;
        }])->groupBy('order_id')->pluck('order_id');

        return Orders::whereIn('id', $orders)
            ->where('status', 'rma')
            ->get()->count();

    }

    public function getOrdersAmountTillDateAttribute()
    {
        $date = Carbon::today();
        $vid = $this->vendor_id;
        $orders = OrderItems::select('*', 'id', 'order_id')->has('products')->with(['products' => function ($q) use ($vid) {
            $q->select('vendor_id')->where('vendor_id', $vid);
            return $q;
        }])->groupBy('order_id')->pluck('order_id');

        $res = Orders::whereIn('id', $orders)
            ->whereDate('created_at', '<=', $date)
            ->get()->toArray();

        $amts = [];
        if (!empty($res)) {
            foreach ($res as $data) {
                array_push($amts, $data['vendor_earning']);
            }
            $sum = array_sum($amts);
            return $sum;
        } else {
            return null;
        }

    }

    public function getOrdersAmountTillMonthAttribute()
    {
        $currentMonth = date('m');
        $currentYear = date('Y');

        $vid = $this->vendor_id;
        $orders = OrderItems::select('*', 'id', 'order_id')->has('products')->with(['products' => function ($q) use ($vid) {
            $q->select('vendor_id')->where('vendor_id', $vid);
            return $q;
        }])->groupBy('order_id')
            ->pluck('order_id');

        $res = Orders::whereIn('id', $orders)
            ->whereRaw('MONTH(created_at) = ?', [$currentMonth])
            ->whereRaw('YEAR(created_at) = ?', [$currentYear])
            ->get()->toArray();

        $amts = [];
        if (!empty($res)) {
            foreach ($res as $data) {
                array_push($amts, $data['vendor_earning']);
            }
            $sum = array_sum($amts);
            return $sum;
        } else {
            return null;
        }

    }
}