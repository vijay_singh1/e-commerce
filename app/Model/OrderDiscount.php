<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDiscount extends Model
{
    use SoftDeletes;

    protected $appends = ['seller_name','store_name'];
    protected $table = 'order_discounts';

     protected $fillable = [
        'item_id',
        'order_id',
        'seller_id',
        'store_id',
        'vendor_disc_value',
        'master_disc_value',
        'seller_disc_value',
        'total_transaction_fees',
        'total_discount',
        'total_shipping',
        'total_gateway_fees',
        'product_price_per_qty',
        'product_qty',
        'seller_cost',
        'total_tax',
        'seller_qty_volume',
        'master_qty_volume',
    ];
    
     
    public function getSellerNameAttribute()
    {
        $seller = $this->seller()->first();
        if(!$seller)
        {
            $name = null;
        }
        else
        {
            $name = $seller->first_name . ' ' . $seller->last_name;
        }

        return isset($name) ? $name : null;
    }

    public function getStoreNameAttribute()
    {
        $store = $this->store()->first();
        if(!$store)
        {
            $name = null;
        }
        else
        {
            $name = $store->store_name;
        }
        return isset($name) ? $name : null;
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Products','item_id','id');
    }
    public function order()
    {
        return $this->belongsTo('App\Model\Orders','order_id','id');
    }
    public function seller()
    {
        return $this->belongsTo('App\Model\SellerUser','seller_id','id');
    }
    public function store()
    {
        return $this->belongsTo('App\Model\SellerStore','store_id','id');
    }

    public function orderGroup()
    {
        return $this->hasMany('App\Model\OrderDiscount','order_id','order_id');
    }
}
