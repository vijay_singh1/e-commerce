<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    //
    protected $table = 'attributes';
 
    public function sku()
    {
        return $this->hasOne('App\Model\GlobalAttributes', 'id', 'global_attribute_id');
    }
    
    public function scopeMediaProducts(){
        return $this->hasOne('App\Model\Media', 'relation_id', 'attribute_id')->where('relation_type','variation');
    }
}
