<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemsCart extends Model
{
    
    protected $table = 'items_cart';

    protected $fillable = [
        'customer_id',
        'seller_id',
        'shopping_cart_count',
        'shopping_cart_price',
        'shopping_cart_products',
        'store_id',
    ];

    protected $casts = ['shopping_cart_products' => 'array'];
    
}