<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItems extends Model
{
    use SoftDeletes;

    protected $table = 'order_items';

    protected $fillable = [
        'item_description',
        'item_id',
        'item_name',
        'item_sku',
        'order_id',
        'price',
        'qty',
        'seller_id',
        'store_id',
        'variation_id',
        'size_wise_qty',
        'material',
        'item_details',
        'pid',
    ];
    
    protected $casts = [
        'size_wise_qty' => 'array',
        'item_details' => 'array'
      ];
    
    public function getProductsAttribute()
    {
        return $this->products()->get();
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Orders','order_id','id');
    }

    public function products()
    {
        return $this->hasOne('App\Model\SellerStoreProducts','pid','pid');
    }

    public function variation()
    {
        return $this->belongsTo('App\Model\SellerStoreProductVariation','variation_id','id');
    }
}
