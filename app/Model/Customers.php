<?php

namespace App\Model;

use App\Filters\Filterable;
use App\Filters\FilterableTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customers extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use FilterableTrait;
    use SoftDeletes;

    protected $table = 'customers';

    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'payment_details'
    ];
    protected $casts = [
        'payment_details' => 'array',
    ];

    protected $appends = [
        'customer_store_details',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getCustomerStoreDetailsAttribute()
    {
        $order_details= $this->orders()->where('transaction_type','customer-order')->get();
        $total_rating = collect($order_details)->sum('net_amount');
        $no_of_orders = collect($order_details)->count();
        $rma = collect($order_details)->where('status','rma')->count();
        $cancelled = collect($order_details)->where('status','cancelled')->count();
        $store_details= SellerStore::where('id',$this->store_id)->get()->toArray();
        $data['store_name']= isset($store_details[0]['store_name'])?$store_details[0]['store_name']:"";
        
        if(isset($this->created_at) && !empty($this->created_at)){
        $data['life_time']= TwoDatesRecordAge($this->created_at,date('Y-m-d H:s:i'),'days');
        $data['age']= TwoDatesRecordAge($this->created_at,date('Y-m-d H:s:i'),'full');
        }else{
            $data['life_time']='0d'; 
            $data['life_time']='0d';   
        }
        
        $data['net_amount']= $total_rating;
        $data['no_of_orders']= $no_of_orders;
        $data['rma']= $rma;
        $data['cancelled']= $cancelled;
        return $data;
    }

    public function address()
    {
        return $this->hasMany('App\Model\Address', 'owner_id', 'id')->where('user_type', 'buyer');
    }
    public function orders()
    {
        return $this->hasMany('App\Model\Orders', 'user_id', 'id');
    }
    public function store()
    {
        return $this->hasOne('App\Model\SellerStore', 'id', 'store_id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value ?: str_random(8));
    }
}