<?php

namespace App\Model;

use App\Filters\FilterableTrait;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use FilterableTrait;
    protected $table = 'cms_banner';

    public function scopeMediaBanner(){
        return $this->belongsTo('App\Model\Media', 'id','relation_id')->where('relation_type','cms_banner');
    }
}
