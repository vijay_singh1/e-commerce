<?php

namespace App\Model;

use App\Services\CommonService;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Builder;
use App\Model\UserProfile;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use const HOUR;

class Media extends Model {


	use SoftDeletes;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'buyone_media';

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['media_url', 'download_url', 'cover_image_url','thumbnail_url','can_edit'];

	protected $fillable = ['file_name','media_hash','relation_id','relation_type','storage_path','tags','meta','position_view'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'meta' => 'array'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	// protected $hidden = ['storage_path'];


	/**
	 * The list of tags that can be used
	 *
	 * @var array
	 */
	public $validMediaTags = ['image-circle', 'image-square', 'pitch', 'document','document-clinic', 'cover-image', 'flyer','presentation'];

	/*
	 * Getters
	 */

	/**
	 * Get the Media preview Url
	 * @return null|string
	 */
	public function getMediaUrlAttribute() {
		return $this->id ? $this->getUrl() : null;
	}

	/**
	 * Get the Media preview Url
	 * @return null|string
	 */

	public function getCanEditAttribute() {
		//return $this->user_id === CommonService::guard()->user()->id;
		return true;
	}

	/**
	 * Get the Media Download Url
	 * @return null|string
	 */
	public function getDownloadUrlAttribute() {
		return $this->id ? $this->getDownloadUrl() : null;
	}

	public function getCoverImageUrlAttribute() {
		return $this->id ? $this->getUrl() : null;
	}

	public function getThumbnailUrlAttribute() {
		return $this->id ? $this->getThumbnailUrl() : null;
	}

	/**
	 * Get the Meta
	 * @return null|object
	 */
	public function getMetaAttribute($value) {
		return json_decode($value);
	}

	/*
	 * Setters
	 */

	/**
	 * Set the Meta
	 * @return void
	 */
	public function setMetaAttribute($value) {
		$this->attributes['meta'] = json_encode($value);
	}

	/*
	 *  Relations
	 */

	/**
	 * Morph Relation for Media
	 * @return MorphTo
	 */
	public function mediable(): MorphTo {
		return $this->morphTo('resource');
	}

	/*
	 * Model Subject
	 */

	/**
	 * Generate the url for the Media provided
	 * @return string
	 */
	public function getUrl(): string {
		$url = Storage::url($this->storage_path.$this->getHashedName());
		return $url;
	}

	/**
	 * Generate the thumnail url for the Media provided
	 * @return string
	 */
	public function getThumbnailUrl(): string {
		$url = Storage::url($this->storage_path.'thumbnail/'.$this->getHashedName());
		//$url = Storage::temporaryUrl($this->media_hash, Carbon::now()->addSeconds(HOUR), $options);
		return $url;
	}

	/**
	 * Generate the url for the Media provided
	 */
	public function getDownloadUrl(): string {
		$options = ['Key' => "{$this->storage_path}{$this->getHashedName()}", 'ResponseContentDisposition' => 'attachment; filename="' . $this->name . '"'];
		$url = Storage::temporaryUrl($this->media_hash, Carbon::now()->addSeconds(HOUR), $options);
		return $url;
	}

	/**
	 * Retrieve the file extension.
	 * @return string
	 */
	public function getHashedName(): string {
        $extention = !empty($this->meta->extension) ? $this->meta->extension : 'jpg';
		return $this->media_hash . '.' . $extention;
	}

	/**
	 * Checks if the tag provided is valid or not.
	 * @param string $tag
	 * @return bool
	 */
	public function isValidTag(string $tag): bool {
		// dd("media model",$tag);
		return in_array($tag, $this->validMediaTags, true);
	}

	public function canDelete() {
		$currentUser = Auth::guard()->user();
		return $this->user_id === $currentUser->id;
	}



}
