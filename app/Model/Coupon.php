<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    /**
     * =============
     * RELATIONSHIPS
     * =============
     */
    public function user()
    {
        return $this->belongsTo('App\Model\Customers', 'user_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Orders','order_id','id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Model\SellerUsers','seller_id','id');
    }
}
