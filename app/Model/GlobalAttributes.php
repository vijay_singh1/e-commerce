<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GlobalAttributes extends Model
{
    //
    protected $table = 'global_attributes';

    public function scopeProductAttributes($query)
    {
        return $this->hasOne('App\Model\Attributes', 'global_attribute_id', 'id');
    }
    
    public function global_attribute_data()
    {
        return $this->hasMany('App\Model\GlobalAttributes', 'name', 'name')->whereNotNull('display_value')->where('display_value','<>','')->where('is_deleted', false);
    }

    public function product_attribute_data() {
        return $this->hasMany('App\Model\ProductAttributes', 'global_attribute_id', 'id');
    }

}
