<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReviewRating extends Model
{
    // use SoftDeletes;
    protected $table = 'review_rating';

    protected $appends = ['customer', 'overall_rating_type','product_quality_type', 'shipping_quality_type', 'average_rating', 'product_image'];

    protected $fillable = [
        'customer_id',
        'pid',
        'heading',
        'comment',
        'status',
        'store_id',
        'seller_id',
        'overall_rating',
        'product_quality',
        'shipping_quality',
        'seen_status',
    ];

    public function getCustomerAttribute()
    {
        return $this->customer()->get()->first();
    }

    public function getOverallRatingTypeAttribute()
    {
        return $this->statusAllByCount($this->overall_rating);
    }

    public function getProductQualityTypeAttribute()
    {
        return $this->statusAllByCount($this->product_quality);
    }

    public function getShippingQualityTypeAttribute()
    {
        return $this->statusAllByCount($this->shipping_quality);
    }

    public function getAverageRatingAttribute()
    {
        $total_count = $this->overall_rating + $this->product_quality + $this->shipping_quality;
        return (int)$total_count / 3;
    }

    public function getProductImageAttribute()
    {

        $selected_media = Media::where('relation_id', $this->customer_id . $this->pid)->where('relation_type', 'store-product-rating')->get()->first();
        if (isset($selected_media->media_url) && !empty($selected_media->media_url)) {
            return $selected_media->media_url;
        } else {
            $selected_media = Media::where('relation_id', $this->pid)->where('relation_type', 'seller-store-product-logo')->get()->first();
            if (isset($selected_media->media_url) && !empty($selected_media->media_url)) {
                return isset($selected_media->media_url) ? $selected_media->media_url : "";
            } else {
                $selected_media = Media::where('relation_id', $this->pid)->where('relation_type', 'product')->get()->first();
                return isset($selected_media->media_url) ? $selected_media->media_url : ""; /*  */
            }
        }
    }

    public function scopeCustomer()
    {
        return $this->belongsTo('App\Model\Customers', 'customer_id', 'id');
    }

    

    public function statusAllByCount($count)
    {
        $case = "Bad";
        switch ($count) {
            case 1:
                $case = "Not Bad";
                break;
            case 2:
                $case = "Satisfactory";
                break;
            case 3:
                $case = "Good!";
                break;
            case 4:
                $case = "Very Good!";
                break;
            case 5:
                $case = "Excellent!!";
                break;
            default:
                $case = "Bad";
        }
        return $case;
    }

}
