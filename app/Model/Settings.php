<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';

    protected $casts = ['setting_value' => 'array',
        ];

    protected $fillable = [
        'setting_key',
        'setting_value'
    ];
}
