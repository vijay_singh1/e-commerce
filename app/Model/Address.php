<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'last_name',
        'address_line_1',
        'address_line_2',
        'country',
        'zipcode',
        'state',
        'city',
        'owner_id',
        'address_type',
        'email',
        'user_type',
        'contact_no',
        // 'email_token',
        // 'is_verify',
        // 'email_link_expired'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $appends = [
        'state_name',
        'country_name',
    ];

    public function state()
    {
        return $this->belongsTo('App\Model\States', 'state', 'id');
    }

    public function getStateNameAttribute()
    {
        $state = $this->state()->pluck('name')->first();
        if (!empty($state)) {
            $name = $state;
        } else {
            $name = null;
        }

        return $name;
    }

    public function getCountryNameAttribute()
    {
        $country = $this->country;
        $name = null;
        if ($country == 38) {
            $name = 'Canada';
        } else if ($country == 231) {
            $name = 'United States';
        } else {
            $name = null;
        }
        return $name;
    }

}