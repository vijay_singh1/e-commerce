<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class EndUser extends Model
{

    use HasApiTokens, Notifiable;

    protected $table = 'customers';
    protected $guards = 'customers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'password',
        'payment_details',
        'remember_token',
        'seller_id',
        'store_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    //relation ship

    public function seller()
    {
        return $this->hasOne('App\Model\SellerUser','id','seller_id');
    }
    public function media(){
        return $this->hasOne('App\Model\Media','relation_id','store_id')->where('relation_type','seller-store-logo');
    }
    public function store(){
        return $this->hasOne('App\Model\SellerStore','id','store_id');
    }
    /**
     * GETTERS
     */
    public function getSuperUserAttribute()
    {
        return $this->WithSuperUser();
    }

    /**
     * SCOPES
     */
    public function scopeWithSuperUser()
    {
        $name = $this->superuser()
            ->get()
            ->pluck('name');

        return $name;
    }

    public function setPasswordAttribute($value)
    {

        $this->attributes['password'] = bcrypt($value ?: str_random(8));
    }

}