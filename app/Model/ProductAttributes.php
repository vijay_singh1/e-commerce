<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAttributes extends Model
{
    //
    protected $table = 'product_attributes';

    // public $timestamps = true;

    protected  $appends = ['global_data'];

    public function getGlobalDataAttribute() {
        return $this->hasOne('App\Model\GlobalAttributes','id','global_attribute_id')->select('id','name','value','display_value','sort_order','is_deleted')->first();
    }


}
