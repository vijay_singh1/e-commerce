<?php

namespace App\Model\Abilities;

use App\Model\Media;
// use App\Model\UserProfile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Traits\Macroable;
use function collect;

/**
 * Mediable Trait.
 *
 * Provides functionality for attaching media to an eloquent model.
 * @author Manish M. Demblani
 */
trait Mediable {

	use Macroable {
		__call as mediaMacroCall;
		__callStatic as mediaMacroCallStatic;
	}

	/**
	 * Stores a list of relations that the current model shares with the media model. This can be basically mean that
	 * one model can have multiple types of media.
	 * @var array
	 */
	private $mediaRelations = [];

	/**
	 * Stores the name of the default relation with the Media model. The default relation is used to fetch the value for the media_url
	 * attribute
	 * @var string
	 */
	private $defaultRelation = 'media';

//	private $documentRelation = 'document';

	/**
	 * Construct the Mediable trait for a model.
	 */
	public function __constructMediable() {
		$this->addMediaRelation('media', ['image-circle','image-square'], ['hasMany' => FALSE]);
	}

	/*
	 * Getters
	 */

	/**
	 * Get the Media Url
	 * @return null|string
	 */
	public function getMediaUrlAttribute() {
		if (isset($this->relations[$this->defaultRelation])) {
			$media = $this->getMedia();
			if ($media instanceof Collection) {
				$media = $media->first(function ($value, $key) {
					return $value->tags === 'image-circle' ||$value->tags === 'image-square';
				});
			}
//			return ($media instanceof Media) ? $media->getUrl() : null;
			if ($media instanceof  Media){
				if (is_null($media->storage_path)){
					return $media->linkedin_image_url;
				}
				else{
					return $media->getUrl();
				}
			}
		}
		return null;
	}

	/**
	 * Get the Media Url of cover image
	 * @return null|string
	 */
	public function getCoverImageUrlAttribute() {
		if (isset($this->relations['cover'])) {
			$media = $this->getMedia('cover');
			if ($media instanceof Collection) {
				$media = $media->first(function ($value, $key) {
					return $value->tags === 'cover-image';
				});
			}
			return ($media instanceof Media) ? $media->getUrl() : null;
		}
		return null;
	}

	public function getThumbnailUrlAttribute() {
		if (isset($this->relations['cover'])) {
			$media = $this->getMedia('cover');
			if ($media instanceof Collection) {
				$media = $media->first(function ($value, $key) {
					return $value->tags === 'cover-image';
				});
			}
			return ($media instanceof Media) ? $media->getThumbnailUrl() : null;
		}
		return null;
	}

	/**
	 * Get the Media Url
	 * @return null|string
	 */
//	public function getDocumentUrlAttribute() {
//		if (isset($this->relations[$this->documentRelation])) {
//			$media = $this->getMedia($this->documentRelation);
//			if ($media instanceof Collection) {
//				$media = $media->first(function ($value, $key) {
//					return $value->tags === $this->documentRelation;
//				});
//			}
//			return ($media instanceof Media) ? $media->getUrl() : null;
//		}
//		return null;
//	}

	/**
	 * Appends can_upload_media attribute of the event
	 * @return bool
	 */
	public function getCanUploadMediaAttribute() {
		return $this->canUploadMedia();
	}

	/*
	 * Serializers
	 */

	/**
	 * Append the media_url attribute
	 */
	public function appendMediaUrl() {
		$this->append('media_url');
	}

		/**
	 * Append the media_url attribute
	 */
	public function appendCoverImageUrl() {
		$this->append('cover_image_url');
	}

	public function appendThumbnailUrl() {
		$this->append('thumbnail_url');
	}

	/**
	 * Append the media_url attribute
	 */
//	public function appendDocumentUrl() {
//		$this->append('document_url');
//	}

	/**
	 * Append the can_upload_media attribute
	 */
	public function appendCanUploadMedia() {
		$this->append('can_upload_media');
	}

	/**
	 * Hide the media key from the final response object
	 */
	public function hideMedia() {
		$this->makeHidden('media');
	}
	/**
	 * Hide the media key from the final response object
	 */
	public function hideMediaWithRelation($relation = 'document') {
		$this->makeHidden($relation);
	}

	/**
	 * Show the media key from the final response object
	 */
	public function showMedia() {
		$this->makeVisible('media');
	}

	/*
	 * Relations
	 */

	/**
	 * Checks if a relation of given type has been set or not.
	 * @param string $relation
	 * @return bool
	 */
	public function hasMediaRelation(string $relation): bool {
		return isset($this->mediaRelations[$relation]);
	}

	/**
	 * Returns a morphed relationship with the media model
	 * @param bool $hasMany Whether the relation is one-to-one or one-to-many
	 * @return MorphMany|MorphOne
	 */
	public function getMediaRelation(bool $hasMany = TRUE) {
		$type = $hasMany ? 'morphMany' : 'morphOne';
		// dump("tt",$type);
		// dd($this->{$type}(Media::class, 'mediable', 'resource_type', 'resource_id')->latest());
		return $this->{$type}(Media::class, 'mediable', 'resource_type', 'resource_id')->latest();
	}

	/**
	 * Allows the current model to add dynamic relations with media model
	 * @param string $name
	 * @param array $tags
	 * @param array $options
	 * @return $this
	 */
	public function addMediaRelation(string $name, array $tags = [], array $options = []) {
		// dump("media 2",$name);
		
		$options += [
			'hasMany' => TRUE
		];
		$this->mediaRelations[$name] = ['tags' => $tags, 'hasMany' => (bool) $options['hasMany']];
		// dump("1",$this->mediaRelations[$name]);
		// dd("doc",$this->mediaRelations[$name]);
		static::macro($name, function () use ($tags, $options) {
			$relation = $this->getMediaRelation((bool) $options['hasMany']);
			if (!empty($tags)) {
				$relation->whereIn('tags', $tags);
				}
				// dd($relation);
			return $relation;
		});
	}

	/**
	 * Removes the relation from the model
	 * @param string $name
	 * @return $this
	 */
	public function removeMediaRelation(string $name) {
		unset($this->mediaRelations[$name]);
		return $this;
	}

	/*
	 * Scope Subject
	 */

	/**
	 * Query scope to eager load attached media.
	 *
	 * @param  Builder $query
	 * @return Builder
	 */
	public function scopeWithMedia(Builder $query, string $relation = 'media',$withUser = true): Builder {
		if ($this->hasMediaRelation($relation) && $withUser ==false) {
			$query->with([
				$relation => function ($query) use ($relation) {
					return $query->whereIn('tags', (array) $this->mediaRelations[$relation]['tags']);
				}
			]);
		}
		else
		if($this->hasMediaRelation($relation) && $withUser) {
			$query->with([
				$relation => function ($query) use ($relation) {
					return $query->whereIn('tags', (array) $this->mediaRelations[$relation]['tags'])->with('user');
				}
			]);
		}

		return $query;
	}

	/**
	 *  Lazy Eager Loaders
	 */

	/**
	 * Lazy eager load attached media relationships.
	 * @return $this
	 */
	public function loadMedia(string $mediaRelation = 'media') {
		return $this->load($mediaRelation);
	}

	/*
	 *  Model Subject
	 */

	/**
	 * Retrieve media attached to the model.
	 * @param string $relation
	 * @return Collection|Media
	 */
	public function getMedia(string $relation = 'media') {
		if (isset($this->{$relation})) {
			return $this->{$relation};
		}
		return $this->mediaRelations[$relation]['hasMany'] ? collect([]) : null;
	}

	/**
	 * Decides whether a media can be uploaded for the current model or not
	 * @return bool
	 */
	public function canUploadMedia(): bool {
		return TRUE;
	}

	/**
	 * Dynamically handle calls to the class.
	 *
	 * @param string $method
	 * @param array $parameters
	 * @return mixed
	 */
	public function __call($method, $parameters) {
		if (static::hasMacro($method)) {
			return $this->mediaMacroCall($method, $parameters);
		}
		return parent::__call($method, $parameters);
	}

	/**
	 * Dynamically handle calls to the class.
	 *
	 * @param string $method
	 * @param array $parameters
	 * @return mixed
	 */
	public static function __callStatic($method, $parameters) {
		if (static::hasMacro($method)) {
			return static::mediaMacroCallStatic($method, $parameters);
		}
		return parent::__callStatic($method, $parameters);
	}

}
