<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
  
    protected $table = 'category';

    protected $casts = ['promotion_ids' => 'array'];

    protected $appends = ['product_count','product_enable_count', 'category_parent_name'];

    /**
     *=======
     *GETTERS
     *=======
     */

    public function getProductCountAttribute($key)
    {
        return $this->products()->count();
    }
    
   public function getProductEnableCountAttribute($key)
    {
        return $this->productsEnable()->count();
    }

    public function getCategoryParentNameAttribute($key)
    {
        return $this->belongsTo('App\Model\Category', 'parent_id', 'id')->pluck('name')->first();
    }

    public function products()
    {
        return $this->hasMany('App\Model\Products');
    }
    
     public function productsEnable()
    {
        return $this->hasMany('App\Model\Products')->where("is_enabled",'1');
    }

  
}
