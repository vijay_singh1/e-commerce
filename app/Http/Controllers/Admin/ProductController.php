<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\ProductsService;
use DataTables;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    protected $productsservice;

    public function __construct(ProductsService $productsservice)
    {
        $this->productsservice = $productsservice;
    }

    /**
     * index function
     * This function to get product view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('admin.pages.products.productView');
        
    }

    /**
     * product ProductView
     * This function to get  product details
     * @param Request $request
     * @return json
     */
    public function productDetails(Request $request)
    {
        return view('admin.pages.products.productDetails');
        
    }


    /**
     * productList function
     * This function to get all product list with requested filter
     * @param Request $request
     * @return json
     */
    public function productList(Request $request)
    {
       
        $status = 200;
        $results= $this->productsservice->productList($request);
        return DataTables::of($results)->make(true);
    }
    

    /**
     * productDetailsByPid function
     * This function to  product details with requested filter
     * @param Request $request
     * @return json
     */
    public function productDetailsByPid(Request $request)
    {
        $data = $request->all();
        $status = 200;
        $response=array();
        if(isset($data['pid']) && !empty($data['pid'])){
            $output= $this->productsservice->productDetailsByPid($data);
            $response['data'] = $output;
            $response['status'] = 'success';
            $response['msg'] = 'Data found';
        }else{
            $response['data'] = [];
            $response['status'] = 'error';
            $response['msg'] = 'Data not found!';
        }
     
        return response(json_encode($response), $status);
    }
   
}
