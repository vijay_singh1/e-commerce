<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\OrderService;
use DataTables;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    protected $orderservice;

    public function __construct(OrderService $orderservice)
    {
        $this->orderservice = $orderservice;
    }

    /**
     * index function
     * This function to get product view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('admin.pages.orders.orderView');
        
    }

    /**
     * orderDetails function
     * This function to get  order details
     * @param Request $request
     * @return json
     */
    public function ordersDetails(Request $request)
    {
        return view('admin.pages.orders.orderDetails');
        
    }


    /**
     * orderList function
     * This function to get all order list with requested filter
     * @param Request $request
     * @return json
     */
    public function ordersList(Request $request)
    {
       
        $status = 200;
        $results= $this->orderservice->ordersList($request);
        return DataTables::of($results)->make(true);
    }
    

    /**
     * productDetailsByPid function
     * This function to  product details with requested filter
     * @param Request $request
     * @return json
     */
    public function ordersDetailsByOrderID(Request $request)
    {
        $data = $request->all();
        $status = 200;
        $response=array();
        if(isset($data['order_id']) && !empty($data['order_id'])){
            $output= $this->orderservice->ordersDetailsByOrderID($data);
            $response['data'] = $output;
            $response['status'] = 'success';
            $response['msg'] = 'Data found';
        }else{
            $response['data'] = [];
            $response['status'] = 'error';
            $response['msg'] = 'Data not found!';
        }
     
        return response(json_encode($response), $status);
    }
   
}
