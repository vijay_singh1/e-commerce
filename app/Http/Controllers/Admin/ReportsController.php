<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use App\Services\BannerService;
use DataTables;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    // protected $bannerservice;

    // public function __construct(BannerService $bannerservice)
    // {
    //     $this->bannerservice = $bannerservice;
    // }

    /**
     * index function
     * This function to get category view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('admin.pages.reports.reportsView');
        
    }

   
   
}
