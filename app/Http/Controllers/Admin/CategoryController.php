<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use DataTables;
use Illuminate\Http\Request;

class categoryController extends Controller
{

    protected $categoryservice;

    public function __construct(CategoryService $categoryservice)
    {
        $this->categoryservice = $categoryservice;
    }

    /**
     * index function
     * This function to get category view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('admin.pages.category.categoryView');
        
    }

    /**
     * category categoryView
     * This function to get  category details
     * @param Request $request
     * @return json
     */
    public function categoryDetails(Request $request)
    {
        return view('admin.pages.category.categoryDetails');
        
    }


    /**
     * categoryList function
     * This function to get all category list with requested filter
     * @param Request $request
     * @return json
     */
    public function categoryList(Request $request)
    {
       
        $status = 200;
        $results= $this->categoryservice->categoryList($request);
        return DataTables::of($results)->make(true);
    }
    

    /**
     * productDetailsByCatID function
     * This function to  category details with requested filter
     * @param Request $request
     * @return json
     */
    public function categoryDetailsByCatID(Request $request)
    {
        $data = $request->all();
        $status = 200;
        $response=array();
        if(isset($data['pid']) && !empty($data['pid'])){
            $output= $this->categoryservice->categoryDetailsByCatID($data);
            $response['data'] = $output;
            $response['status'] = 'success';
            $response['msg'] = 'Data found';
        }else{
            $response['data'] = [];
            $response['status'] = 'error';
            $response['msg'] = 'Data not found!';
        }
     
        return response(json_encode($response), $status);
    }
   
}
