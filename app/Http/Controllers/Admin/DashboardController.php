<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Services\UserService;
use Carbon\Carbon;
use Hash;
use Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
  
    protected $user;
    protected $user_service;

    public function __construct(User $user,UserService $user_service){
       $this->user = $user;
       $this->user_service = $user_service;
    }

    public function index(Request $request)
    {
        return view('admin.pages.dashboard.dashboard');
    }

    

}