<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\BannerService;
use DataTables;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    protected $bannerservice;

    public function __construct(BannerService $bannerservice)
    {
        $this->bannerservice = $bannerservice;
    }

    /**
     * index function
     * This function to get category view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('admin.pages.banner.bannerView');
        
    }

    /**
     * Banner BannerView
     * This function to get  category details
     * @param Request $request
     * @return json
     */
    public function bannerDetails(Request $request)
    {
        return view('admin.pages.banner.bannerDetails');
        
    }


    /**
     * bannerList function
     * This function to get all category list with requested filter
     * @param Request $request
     * @return json
     */
    public function bannerList(Request $request)
    {
       
        $status = 200;
        $results= $this->bannerservice->bannerList($request);
        return DataTables::of($results)->make(true);
    }
    

    /**
     * bannerDetailsByID function
     * This function to  category details with requested filter
     * @param Request $request
     * @return json
     */
    public function bannerDetailsByID(Request $request)
    {
        $data = $request->all();
        $status = 200;
        $response=array();
        if(isset($data['pid']) && !empty($data['pid'])){
            $output= $this->bannerservice->bannerDetailsByID($data);
            $response['data'] = $output;
            $response['status'] = 'success';
            $response['msg'] = 'Data found';
        }else{
            $response['data'] = [];
            $response['status'] = 'error';
            $response['msg'] = 'Data not found!';
        }
     
        return response(json_encode($response), $status);
    }
   
}
