<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
  
    
    public function __construct(){
    }

   
    public function getCountryStateCity(Request $request)
    {
        $status = 200;
        $result =array();
        $data = $request->all();
        try {
            $result = GetCountryStateCity($data);
        } catch(\Exception $e){
            $errorCode = isset($e->errorInfo[1])?$e->errorInfo[1]:"";
            $error = [
                'status'=> 200,
                'success' => false,
                'message' => isset($e->errorInfo[2])?$e->errorInfo[2]:"",
            ];
        }
        if ($result) {
                $response = [
                    'datas' => $result,
                    'success' => true,
                    'status'=> $status,
                    'message' => 'get list',
                ];
        } else {
            $response = $error;
        }
        return response(json_encode($response), $status);
    }


}