<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\UserService;
use Carbon\Carbon;
use Hash;
use Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
  
    protected $user;
    protected $user_service;

    public function __construct(User $user,UserService $user_service){
       $this->user = $user;
       $this->user_service = $user_service;
    }

    public function userLogin(Request $request)
    {
        $status = 200;
        $this->user_service->validate('loginValidation', $request->all());
        $data = $request->only($this->user_service->getValidationKeys('loginValidation'));

        $user = $this->user::where('email', $data['email'])->first();
        if (isset($data['password']) && !empty($data['password'])) {
            if (!\Hash::check($data['password'], $user->password)) {
                return response()->json([
                    'datas' => [],
                    'success' => false,
                    'message' => 'Invalid email or password',
                ], 401);
            }
        } else {
            return response()->json([
                'datas' => [],
                'success' => false,
                'message' => 'Invalid email or password',
            ], 401);
        }

        if ($user) {
            $response = [
                'datas' => $user,
                'success' => true,
                'status'=> $status,
                'message' => 'User has been successfully login.',
            ];
        } else {
            $status = 404;
            $response = [
                'datas' => [],
                'success' => false,
                'status'=> $status,
                'message' => 'something went wrong!',
            ];
        }
        return response(json_encode($response), $status);
    }

    public function userSignup(Request $request)
    {
        $user=$error=array();
        $status = 200;

        $this->user_service->validate('signupValidation', $request->all());
        $data = $request->only($this->user_service->getValidationKeys('signupValidation'));
        try {
            $user= $this->user::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'api_token' => Str::random(60),
            ]); 
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = isset($e->errorInfo[1])?$e->errorInfo[1]:"";
            if($errorCode == '1062'){
                $error = [
                    'status'=> $errorCode,
                    'success' => false,
                    'message' => isset($e->errorInfo[2])?$e->errorInfo[2]:"",
                ];
            }
            
        }
        if ($user) {
            $response = [
                'datas' => $user,
                'success' => true,
                'status'=> $status,
                'message' => 'User has been successfully created.',
            ];
        } else if ($error) {
            $response = $error;
        }
        else {
            $status = 404;
            $response = [
                'datas' => [],
                'success' => false,
                'status'=> $status,
                'message' => 'something went wrong!',
            ];
        }
        return response(json_encode($response), $status);
    } 
    
  
    public function getUsers(Request $request)
    {
        $status = 200;
        $getuser = $this->user_service->filter($request);
         if ($getuser) {
            $response = [
                'datas' => $getuser,
                'success' => true,
                'status'=> $status,
                'message' => 'All Users',
            ];
        } 
        else {
            $status = 404;
            $response = [
                'datas' => [],
                'success' => false,
                'status'=> $status,
                'message' => 'something went wrong!',
            ];
        }
        
        return response(json_encode($response), $status);
    }

    

}