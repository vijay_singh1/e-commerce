<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * index function
     * This function to get category view page
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('public.pages.home');
        
    }

     /**
     * about function
     * @param Request $request
     */
    public function about(Request $request)
    {
        return view('public.pages.about');
        
    }

     /**
     * checkout function
     * @param Request $request
     */
    public function checkout(Request $request)
    {
        return view('public.pages.checkout');
        
    }

    /**
     * contact function
     * @param Request $request
     */
    public function contact(Request $request)
    {
        return view('public.pages.contact');
        
    }

    /**
     * faqs function
     * @param Request $request
     */
    public function faqs(Request $request)
    {
        return view('public.pages.faqs');
        
    }

    /**
     * help function
     * @param Request $request
     */
    public function help(Request $request)
    {
        return view('public.pages.help');
        
    }

    /**
     * icons function
     * @param Request $request
     */
    public function icons(Request $request)
    {
        return view('public.pages.icons');
        
    }

    /**
     * payment function
     * @param Request $request
     */
    public function payment(Request $request)
    {
        return view('public.pages.payment');
        
    }

    /**
     * privacy function
     * @param Request $request
     */
    public function privacy(Request $request)
    {
        return view('public.pages.privacy');
        
    }

    /**
     * product function
     * @param Request $request
     */
    public function product(Request $request)
    {
        return view('public.pages.product');
        
    }

    /**
     * single function
     * @param Request $request
     */
    public function single(Request $request)
    {
        return view('public.pages.single');
        
    }

    /**
     * terms function
     * @param Request $request
     */
    public function terms(Request $request)
    {
        return view('public.pages.terms');
        
    }

    /**
     * typography function
     * @param Request $request
     */
    public function typography(Request $request)
    {
        return view('public.pages.typography');
        
    }
    
   
}
