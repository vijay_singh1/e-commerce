<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Front\HomeController@index');
Route::post('/admin/login-submit', 'Admin\UserController@loginSubmit');
Route::get('/admin/login', 'Admin\UserController@adminLoginView');

Route::group(['prefix' => 'admin',  'middleware' => 'guest'], function()
{
    
    Route::get('/logout', 'Admin\UserController@UserLogOutSubmit');
    Route::get('/dashboard', 'Admin\DashboardController@index');
    
    Route::get('/products', 'Admin\ProductController@index');
    Route::get('/product-lists', 'Admin\ProductController@productList');
    Route::get('/product-details', 'Admin\ProductController@productDetails');
    Route::get('/product-details-by-pid', 'Admin\ProductController@productDetailsByPid');
    
    
    Route::get('/category', 'Admin\CategoryController@index');
    Route::get('/category-lists', 'Admin\CategoryController@categoryList');
    Route::get('/category-details', 'Admin\CategoryController@categoryDetails');
    Route::get('/category-details-by-catid', 'Admin\CategoryController@categoryDetailsByCatID');

    Route::get('/banner', 'Admin\BannerController@index');
    Route::get('/banner-lists', 'Admin\BannerController@bannerList');
    Route::get('/banner-details', 'Admin\BannerController@bannerDetails');
    Route::get('/banner-details-by-id', 'Admin\BannerController@bannerDetailsByID');


    Route::get('/orders', 'Admin\OrdersController@index');
    Route::get('/order-lists', 'Admin\OrdersController@ordersList');
    Route::get('/order-details', 'Admin\OrdersController@ordersDetails');
    Route::get('/order-details-by-orderid', 'Admin\OrdersController@orderDetailsByOrderID');

    Route::get('/customers', 'Admin\ReportsController@index');
    Route::get('/payment', 'Admin\PaymentController@index');
    Route::get('/settings', 'Admin\SettingsController@index');
    
    
});


Route::group(['prefix' => ''], function()
{
    Route::get('/logout', 'Admin\UserController@UserLogOutSubmit');
    Route::get('/home', 'Front\HomeController@index');
    Route::get('/about', 'Front\HomeController@about');
    Route::get('/checkout', 'Front\HomeController@checkout');
    Route::get('/contact', 'Front\HomeController@contact');
    Route::get('/faqs', 'Front\HomeController@faqs');
    Route::get('/help', 'Front\HomeController@help');
    Route::get('/icons', 'Front\HomeController@icons');
    Route::get('/payment', 'Front\HomeController@payment');
    Route::get('/privacy', 'Front\HomeController@privacy');
    Route::get('/product', 'Front\HomeController@product');
    Route::get('/single', 'Front\HomeController@single');
    Route::get('/terms', 'Front\HomeController@terms');
    Route::get('/typography', 'Front\HomeController@typography');
   
    
    
});

Route::get('get-state', 'ApplicationController@getCountryStateCity');