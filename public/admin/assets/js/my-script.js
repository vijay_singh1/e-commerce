function CallToastMsg(msg='',title='',timeOut=5000,type='error',position="toast-top-right"){
      if(type == 'success'){
        toastr.success(msg, title,{
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": position,
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "100",
          "hideDuration": "1000",
          "timeOut": timeOut,
          "extendedTimeOut": "1000",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        });
      }else{
        toastr.error(msg, title,{
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": position,
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "100",
          "hideDuration": "1000",
          "timeOut": timeOut,
          "extendedTimeOut": "1000",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }); 
      }
}

function CallSweetalertDelete(msg='',action='',id=''){
  swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true
}).then(function(result){
    if (result.value) {
      jQuery.ajax({
          url: ajax_url+action+id,
          type: "DELETE",
          success: function (a) {
          var b = JSON.parse(a);
          if(b.success){
          jQuery('#table-init').dataTable().api().ajax.reload(function () {
                swal.fire(
                  'Deleted!',
                   msg+' has been deleted.',
                  'success'
              )
          }, true);
          }
          },
          error: function (data) {
          } 
      });
    } else if (result.dismiss === 'cancel') {
        swal.fire(
            'Cancelled',
             msg+' is safe :)',
            'error'
        )
    }
});
}


function isNullAndUndef(variable) {
  return (variable !== "" && variable !== null);
}