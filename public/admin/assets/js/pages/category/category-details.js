jQuery(document).ready(function() {
    $(function() {
        $("a[title]").tooltip();
    });

    $(".board-inner li").click(function() {
        $(".board-inner li").removeClass("active");
        $(this).addClass("active");
    });
    //table_data_load();
    get_product_detail_by_catid(jQuery("#catid-data").val());
});

function get_product_detail_by_catid(catid) {
    jQuery.ajax({
        url: ajax_url + "/admin/category-details-by-catid",
        type: "GET",
        data: {
            catid: catid
        },
        success: function(a) {
            var b = JSON.parse(a);
            if (b.status) {
                jQuery("#pic-1").html(
                    `<img src="` + b.data.image_snapshot_url + `">`
                );
                jQuery("#product-title").html(b.data.name);
                jQuery("#supplier-sku").html(b.data.supplier_sku);
                jQuery("#category").html(b.data.category_name);
            } else {
                KTApp.unblock(loader_id);
            }
        },
        error: function(data) {
            var b = jQuery.parseJSON(data.responseText);
            KTApp.unblock(loader_id);
        }
    });
}
