jQuery(document).ready(function() {
    $(function() {
        $("a[title]").tooltip();
    });

    $(".board-inner li").click(function() {
        $(".board-inner li").removeClass("active");
        $(this).addClass("active");
    });
    //table_data_load();
    get_product_detail_by_pid(jQuery("#pid-data").val());
});

function table_data_load() {
    var table_companies = jQuery("#table-init").dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        paging: true,
        ajax: ajax_url + "/admin/order-lists",

        columns: [
            { data: "image_snapshot_url" },
            { data: "category_name" },
            { data: "name" },
            { data: "margin_option" },
            { data: "is_enabled" }
        ],
        order: [],
        columnDefs: [
            {
                targets: 0,
                width: "10%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><img src="` +
                            val.image_snapshot_url +
                            `" width="60"/></div>`;
                    });

                    return composition_html;
                }
            },

            {
                targets: 2,
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><a href="` +
                            ajax_url +
                            `/admin/product-details">` +
                            full.name +
                            `</a></div>`;
                    });
                    return composition_html;
                }
            },

            {
                targets: 3,
                // width: "5%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html += ` <div class="kt-list-timeline__item">$10</div>`;
                    });

                    return composition_html;
                }
            },
            {
                targets: 4,
                // width: "7%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        var badge_name = "";
                        var badge_color = "";
                        if (val.is_enabled == 1) {
                            badge_color = "success";
                            badge_name = "Activate";
                        } else {
                            badge_color = "danger";
                            badge_name = "Deactivate";
                        }
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><span class="badge badge-` +
                            badge_color +
                            `">` +
                            badge_name +
                            `<span></div>`;
                    });

                    return composition_html;
                }
            }

            //  {
            //     targets: 6,
            //     orderable: false,
            //     render: function(data, type, full, meta) {
            //         var composition_ids=[];
            //         $(full.composition).each(function(i,val){
            //            composition_ids.push(val.composition.id);
            //         });

            //         return `
            //         <a href="javascript:void(0);" id="medicine-edit"
            //          data-id="`+full.id+`"
            //          data-name="`+full.name+`"
            //          data-description="`+full.description+`"
            //          data-company_id="`+full.company_id+`"
            //          data-composition_id="`+composition_ids+`"
            //          data-type_id="`+full.type_id+`"
            //          data-retail_price="`+full.retail_price+`"
            //          data-meta_data="`+full.meta_data+`"
            //          data-quantity="`+full.quantity+`"
            //          class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
            //             <i class="la la-edit"></i>
            //         </a>
            //         <a href="javascript:void(0);" id="medicine-delete" data-id="`+full.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
            //             <i class="la la-trash"></i>
            //         </a>`;
            //     },
            // },
        ]
    });
}

function get_product_detail_by_pid(pid) {
    jQuery.ajax({
        url: ajax_url + "/admin/order-details-by-orderid",
        type: "GET",
        data: {
            pid: pid
        },
        success: function(a) {
            var b = JSON.parse(a);
            if (b.status) {
                jQuery("#pic-1").html(
                    `<img src="` + b.data.image_snapshot_url + `">`
                );
                jQuery("#product-title").html(b.data.name);
                jQuery("#supplier-sku").html(b.data.supplier_sku);
                jQuery("#category").html(b.data.category_name);
            } else {
                KTApp.unblock(loader_id);
            }
        },
        error: function(data) {
            var b = jQuery.parseJSON(data.responseText);
            KTApp.unblock(loader_id);
        }
    });
}
