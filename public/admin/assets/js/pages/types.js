jQuery(document).ready(function() {

   table_data_load();

   jQuery("#form-submit").submit(function (a) {
    KTApp.block('#form-submit', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: 'Please wait...'
        });
        
    a.preventDefault();
    var b = new FormData(this);
    var method = "POST";
    var ajax_url_set = ajax_url+"/admin/type-add";
    if(isNullAndUndef(b.get("id"))){
        ajax_url_set = ajax_url+"/admin/type-edit";
    }
    jQuery.ajax({
        url: ajax_url_set,
        type: method,
        contentType: "json",
        data: b,
        contentType: !1,
        processData: !1,
        success: function (a) {
        var b = JSON.parse(a);
        if(b.success){

        jQuery('#table-init').dataTable().api().ajax.reload(function () {
            CallToastMsg(b.message,"Success",4000,'success');
            KTApp.unblock('#form-submit');
        }, true);
        }else{
            CallToastMsg(b.message,"Error",4000,'error')
            KTApp.unblock('#form-submit');
        }
        },
        error: function (data) {
               var b = jQuery.parseJSON(data.responseText);
               CallToastMsg(b.message,"Error",4000,'error')
               KTApp.unblock('#form-submit');
        }
            
    });
  });

});

function table_data_load() {
    var table_companies = jQuery('#table-init').dataTable({
        "processing": true,
        "serverSide": true,
        responsive: true,
        paging: true,
        ajax: ajax_url+"/admin/typeslist",
        
        columns: [
            {data: 'name'},
            {data: 'quantifying_unit'},
            {data: 'description'},
           ],
        columnDefs: [
            
            {
                targets: 0,
                render: function(data, type, full, meta) {
                        var stateNo = KTUtil.getRandomInt(0, 7);
                        var states = [
                            'success',
                            'brand',
                            'danger',
                            'success',
                            'warning',
                            'dark',
                            'primary',
                            'info'];
                        var state = states[stateNo];
    
                        output = `
                            <div class="kt-user-card-v2">
                                <div class="kt-user-card-v2__pic">
                                    <div class="kt-badge kt-badge--xl kt-badge--` + state + `"><span>` + full.name.substring(0, 1) + `</div>
                                </div>
                                <div class="kt-user-card-v2__details">
                                <span class="kt-user-card-v2__name">` + full.name + `</span>
                                </div>
                            </div>`;
                    return output;
                },
            },
            {
                targets: 3,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    return `
                    <a href="javascript:void(0);" id="type-edit" data-id="`+full.id+`" data-name="`+full.name+`" data-quantifying="`+full.quantifying_unit+`" data-description="`+full.description+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" id="type-delete" data-id="`+full.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
                        <i class="la la-trash"></i>
                    </a>`;
                },
            },
            
        ],
        
    });
}

jQuery(document).on("click","#type-edit",function(){
     $("#id").val($(this).attr('data-id'));
     $("#name").val($(this).attr('data-name'));
     $("#quantifying_unit").val($(this).attr('data-quantifying'));
     $("#description").val($(this).attr('data-description'));
     $("#hide-show-form").show(600);
});

jQuery(document).on("click","#type-delete",function(){
    CallSweetalertDelete("Type","/admin/type-delete/",$(this).attr('data-id'));
});


jQuery(document).on("click",".hide-show-button",function(){
    $("#form-submit")[0].reset();
    $("#id").val("");
    var isVisible = $("#hide-show-form").is(':visible');
    if(isVisible){
        $("#hide-show-form").hide(600);
    }else{
        $("#hide-show-form").show(600);
    }
   
   });