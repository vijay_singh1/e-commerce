jQuery(document).ready(function() {
    table_data_load();

    jQuery("#form-submit").submit(function(a) {
        KTApp.block("#form-submit", {
            overlayColor: "#000000",
            type: "v2",
            state: "success",
            message: "Please wait..."
        });

        a.preventDefault();
        var b = new FormData(this);
        var method = "POST";
        var ajax_url_set = ajax_url + "/admin/medicine-add";
        if (isNullAndUndef(jQuery("#id").val())) {
            ajax_url_set = ajax_url + "/admin/medicine-edit";
        }

        jQuery.ajax({
            url: ajax_url_set,
            type: method,
            contentType: "json",
            data: b,
            contentType: !1,
            processData: !1,
            success: function(a) {
                var b = JSON.parse(a);
                if (!isNullAndUndef(jQuery("#id").val())) {
                    $("#form-submit")[0].reset();
                }
                if (b.success) {
                    jQuery("#table-init")
                        .dataTable()
                        .api()
                        .ajax.reload(function() {
                            CallToastMsg(b.message, "Success", 4000, "success");
                            KTApp.unblock("#form-submit");
                        }, true);
                } else {
                    CallToastMsg(b.message, "Error", 4000, "error");
                    KTApp.unblock("#form-submit");
                }
            },
            error: function(data) {
                var b = jQuery.parseJSON(data.responseText);
                CallToastMsg(b.message, "Error", 4000, "error");
                KTApp.unblock("#form-submit");
            }
        });
    });
});

function table_data_load() {
    var table_companies = jQuery("#table-init").dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        paging: true,
        ajax: ajax_url + "/admin/product-lists",

        columns: [
            { data: "image_snapshot_url" },
            { data: "category_name" },
            { data: "name" },
            { data: "margin_option" },
            { data: "is_enabled" }
        ],
        order: [],
        columnDefs: [
            {
                targets: 0,
                width: "10%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><img src="` +
                            val.image_snapshot_url +
                            `" width="60"/></div>`;
                    });

                    return composition_html;
                }
            },

            {
                targets: 1,
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><a href="` +
                            ajax_url +
                            `/admin/product-details?pid=` +
                            full.pid +
                            `">` +
                            full.category_name +
                            `</a></div>`;
                    });
                    return composition_html;
                }
            },

            {
                targets: 2,
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><a href="` +
                            ajax_url +
                            `/admin/product-details?pid=` +
                            full.pid +
                            `">` +
                            full.name +
                            `</a></div>`;
                    });
                    return composition_html;
                }
            },

            {
                targets: 3,
                // width: "5%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        composition_html += ` <div class="kt-list-timeline__item">$10</div>`;
                    });

                    return composition_html;
                }
            },
            {
                targets: 4,
                // width: "7%",
                render: function(data, type, full, meta) {
                    var composition_html = ``;
                    $(full).each(function(i, val) {
                        var badge_name = "";
                        var badge_color = "";
                        if (val.is_enabled == 1) {
                            badge_color = "success";
                            badge_name = "Activate";
                        } else {
                            badge_color = "danger";
                            badge_name = "Deactivate";
                        }
                        composition_html +=
                            ` <div class="kt-list-timeline__item"><span class="badge badge-` +
                            badge_color +
                            `">` +
                            badge_name +
                            `<span></div>`;
                    });

                    return composition_html;
                }
            }

            //  {
            //     targets: 6,
            //     orderable: false,
            //     render: function(data, type, full, meta) {
            //         var composition_ids=[];
            //         $(full.composition).each(function(i,val){
            //            composition_ids.push(val.composition.id);
            //         });

            //         return `
            //         <a href="javascript:void(0);" id="medicine-edit"
            //          data-id="`+full.id+`"
            //          data-name="`+full.name+`"
            //          data-description="`+full.description+`"
            //          data-company_id="`+full.company_id+`"
            //          data-composition_id="`+composition_ids+`"
            //          data-type_id="`+full.type_id+`"
            //          data-retail_price="`+full.retail_price+`"
            //          data-meta_data="`+full.meta_data+`"
            //          data-quantity="`+full.quantity+`"
            //          class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
            //             <i class="la la-edit"></i>
            //         </a>
            //         <a href="javascript:void(0);" id="medicine-delete" data-id="`+full.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
            //             <i class="la la-trash"></i>
            //         </a>`;
            //     },
            // },
        ]
    });
}

jQuery(document).on("click", "#medicine-edit", function() {
    $("#id").val($(this).attr("data-id"));
    $("#name").val($(this).attr("data-name"));
    $("#description").val($(this).attr("data-description"));
    $("#company_id").val($(this).attr("data-company_id"));
    $("#type_id").val($(this).attr("data-type_id"));
    $("#quantity").val($(this).attr("data-quantity"));
    $("#retail_price").val($(this).attr("data-retail_price"));
    $("#meta_data").val($(this).attr("data-meta_data"));
    get_compositionlist(
        $(this)
            .attr("data-composition_id")
            .split(",")
    );
    get_types($("#type_id"));
    get_quantity($("#quantity"));
    $("#hide-show-form").show(600);
    $("html, body").animate(
        {
            scrollTop: 20 //#DIV_ID is an example. Use the id of your destination on the page
        },
        "slow"
    );
});

jQuery(document).on("click", "#medicine-delete", function() {
    CallSweetalertDelete(
        "Medicine",
        "/admin/medicine-delete/",
        $(this).attr("data-id")
    );
});

jQuery(document).on("click", ".hide-show-button", function() {
    $("#form-submit")[0].reset();
    $("#id").val("");
    var isVisible = $("#hide-show-form").is(":visible");
    if (isVisible) {
        // $("#hide-show-form").hide(600);
    } else {
        // $("#hide-show-form").show(600);
    }
    get_compositionlist();
});

jQuery(document).on("change", "#type_id", function() {
    get_types(this);
    get_quantity($("#quantity"));
});

$("#quantity").on("input", function() {
    get_quantity(this);
});

function get_quantity(obj) {
    var string = $("#type-set").html();
    var new_string = string.replace("%num%", $(obj).val());
    jQuery("#type-desc").html(new_string);
}

function get_types(obj) {
    jQuery("#type-qty").html(
        $(obj)
            .find(":selected")
            .data("quantifying_unit")
    );
    jQuery("#type-desc").html(
        $(obj)
            .find(":selected")
            .data("description")
    );
    jQuery("#type-set").html(
        $(obj)
            .find(":selected")
            .data("description")
    );
}

function get_compositionlist(datas = []) {
    jQuery("#composition_id").html("");
    jQuery("#composition_id").empty();
    jQuery.ajax({
        url: ajax_url + "/admin/compositionlist",
        type: "GET",
        data: {},
        success: function(a) {
            $.each(a.data, function(index, value) {
                $("#composition_id").append(
                    $("<option></option>")
                        .attr("value", value.id)
                        .text(value.name)
                );
            });

            if (datas) {
                $("#composition_id").val(datas);
                $("#composition_id").select2({
                    placeholder: "Add a tag",
                    tags: true,
                    allowClear: true
                });
            } else {
                $("#composition_id").select2({
                    placeholder: "Add a tag",
                    tags: true,
                    allowClear: true
                });
            }
        },
        error: function(data) {}
    });
}
