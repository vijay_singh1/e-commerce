jQuery(document).ready(function() {

   table_data_load();

   jQuery("#form-submit").submit(function (a) {
    KTApp.block('#form-submit', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: 'Please wait...'
        });
     
    a.preventDefault();
    var b = new FormData(this);
    var method = "POST";
    var ajax_url_set = ajax_url+"/admin/company-add";
    if(isNullAndUndef(jQuery("#id").val())){
        ajax_url_set = ajax_url+"/admin/company-edit";
    }
   
    jQuery.ajax({
        url: ajax_url_set,
        type: method,
        contentType: "json",
        data: b,
        contentType: !1,
        processData: !1,
        success: function (a) {
        var b = JSON.parse(a);
        if(!isNullAndUndef(jQuery("#id").val())){
            $("#form-submit")[0].reset();
        }
        if(b.success){

        jQuery('#table-init').dataTable().api().ajax.reload(function () {
            CallToastMsg(b.message,"Success",4000,'success');
            KTApp.unblock('#form-submit');
        }, true);
        }else{
            CallToastMsg(b.message,"Error",4000,'error')
            KTApp.unblock('#form-submit');
        }
        },
        error: function (data) {
               var b = jQuery.parseJSON(data.responseText);
               CallToastMsg(b.message,"Error",4000,'error')
               KTApp.unblock('#form-submit');
        }
            
    });
  });

});

function table_data_load() {
    var table_companies = jQuery('#table-init').dataTable({
        "processing": true,
        "serverSide": true,
        responsive: true,
        paging: true,
        ajax: ajax_url+"/admin/companylist",
        
        columns: [
            {data: 'company_name'},
            {data: 'licence'},
            {data: 'gst_no'},
            {data: 'address.address_1'},
            {data: 'status'}
           ],

        columnDefs: [
            {
                targets: 0,
                render: function(data, type, full, meta) {
                        var stateNo = KTUtil.getRandomInt(0, 7);
                        var states = [
                            'success',
                            'brand',
                            'danger',
                            'success',
                            'warning',
                            'dark',
                            'primary',
                            'info'];
                        var state = states[stateNo];
                        var city=states=country=company_name='';

                        if(typeof(full.address) != "undefined" && full.address !== null){
                            if(full.address.state){ states = full.address.state; }
                            if(full.address.city){ city = full.address.city; }
                            if(full.address.country){ country = full.address.country; }
                        }

                         if(typeof(full.company_name) != "undefined" && full.company_name !== null){
                            company_name = full.company_name;
                         }

                        output = `
                            <div class="kt-user-card-v2">
                                <div class="kt-user-card-v2__pic">
                                    <div class="kt-badge kt-badge--xl kt-badge--` + state + `"><span>` + company_name.substring(0, 1) + `</div>
                                </div>
                                <div class="kt-user-card-v2__details">
                                <span class="kt-user-card-v2__name">` + company_name + `</span>
                                    <a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">` + city     + `,</a>
                                    <a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">` + states + `,</a>
                                    <a href="javascript:void(0)" class="kt-user-card-v2__email kt-link">` + country + `</a>
                                </div>
                            </div>`;
                    return output;
                },
            },

            {
                targets: 3,
                render: function(data, type, full, meta) {
                    var address_1='';
                    if(typeof(full.address) != "undefined" && full.address !== null){
                        if(full.address.address_1){ address_1 = full.address.address_1; }
                    }
                    return address_1;
                },
            },

            {
                targets: 4,
                render: function(data, type, full, meta) {
                    var status = {
                        1: {'title': 'Active', 'class': ' kt-badge--success'},
                        2: {'title': 'Pending', 'class': 'kt-badge--brand'},
                        3: {'title': 'Deactive', 'class': ' kt-badge--danger'},
                    };
                    if (typeof status[data] === 'undefined') {
                        return data;
                    }
                    return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
                },
            },
            {
                targets: 5,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {

                    var address_type=city=states=country=address_1=address_2=email=contact_no='';

                    if(typeof(full.address) != "undefined" && full.address !== null){
                        if(full.address.address_1){ address_1 = full.address.address_1; }
                        if(full.address.address_2){ address_2 = full.address.address_2; }
                        if(full.address.email){ email = full.address.email; }
                        if(full.address.contact_no){ contact_no = full.address.contact_no; }
                        if(full.address.id){ address_id = full.address.id; }
                        if(full.address.state_id){ states = full.address.state_id; }
                        if(full.address.city_id){ city = full.address.city_id; }
                        if(full.address.country_id){ country = full.address.country_id; }
                        if(full.address.address_type){ address_type = full.address.address_type; }
                    }

                    return `
                    <a href="javascript:void(0);" id="company-edit"
                     data-id="`+full.id+`"
                     data-address_1="`+address_1+`"
                     data-address_2="`+address_2+`" 
                     data-email="`+email+`" 
                     data-contact_no="`+contact_no+`"
                     data-address_id="`+address_id+`"
                     data-address_type="`+address_type+`"
                     data-country_id="`+country+`"
                     data-state_id="`+states+`"
                     data-city_id="`+city+`"
                     data-company_name="`+full.company_name+`"
                     data-licence="`+full.licence+`"
                     data-gst_no="`+full.gst_no+`"
                     data-status="`+full.status+`"
                     class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" id="company-delete" data-id="`+full.id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">
                        <i class="la la-trash"></i>
                    </a>`;
                },
            },
            
        ],
        
    });
}

jQuery(document).on("click","#company-edit",function(){
     $("#id").val($(this).attr('data-id'));
     $("#address_id").val($(this).attr('data-address_id'));
     $("#address_1").val($(this).attr('data-address_1'));
     $("#address_2").val($(this).attr('data-address_2'));
     $("#email").val($(this).attr('data-email'));
     $("#contact_no").val($(this).attr('data-contact_no'));
     $("#country_id").val($(this).attr('data-country_id'));
     $("#state_id").val($(this).attr('data-state_id'));
     $("#city_id").val($(this).attr('data-city_id'));
     $("#company_name").val($(this).attr('data-company_name'));
     $("#licence").val($(this).attr('data-licence'));
     $("#gst_no").val($(this).attr('data-gst_no'));
     $("#status").val($(this).attr('data-status'));
     $("#address_type").val($(this).attr('data-address_type'));
     
     
     $.when(get_country_to_state('state',$(this).attr('data-country_id'),'#state_id',".col-lg-4",$(this).attr('data-state_id')))
     .then(get_state_to_city('city',$(this).attr('data-state_id'),'#city_id',".col-lg-4",$(this).attr('data-city_id')));
     
     $("#hide-show-form").show(600);
     $('html, body').animate({
        scrollTop:  20 //#DIV_ID is an example. Use the id of your destination on the page
    }, 'slow');
});

jQuery(document).on("click","#company-delete",function(){
    CallSweetalertDelete("Company","/admin/company-delete/",$(this).attr('data-id'));
});


jQuery(document).on("click",".hide-show-button",function(){
    $("#form-submit")[0].reset();
    $("#id").val("");
    
    var isVisible = $("#hide-show-form").is(':visible');
    if(isVisible){
        $("#hide-show-form").hide(600);
    }else{
        jQuery('#state_id').empty();
        jQuery('#state_id').html('');
        jQuery('#city_id').empty();
        jQuery('#city_id').html('');
        $("#hide-show-form").show(600);
    }
   });


   jQuery(document).on("change","#country_id",function(){
       get_country_to_state('state',$(this).val(),'#state_id',".col-lg-4",'','');
   });

   jQuery(document).on("change","#state_id",function(){
      get_state_to_city("city",$(this).val(),'#city_id',".col-lg-4",'','');
   });

   
function get_country_to_state(type,relation_id="",id,loader_id="",selected_id='') {
    loader_id = $(id).closest(loader_id);
    jQuery(id).empty();
    jQuery(id).html('');
    KTApp.block(loader_id, {
        overlayColor: '#74788d',
        type: 'v2',
        state: 'success'
    });

    jQuery.ajax({
        url: ajax_url+"/get-state",
        type: "GET",
        data: {
            'relation_id': relation_id,
            'type':type,
        },
        success: function(a) {
            jQuery(id).empty();
            jQuery(id).html('');
            var b = JSON.parse(a);
            if (b.success) {
                KTApp.unblock(loader_id);

                $(id).append($("<option></option>")
                .attr("value", "")
                .text("Select"));

                $.each(b.datas, function(index, value) {
                    if (selected_id == value.id) {
                        $(id).append($("<option></option>")
                                .attr("value", value.id)
                                .attr("selected", 'selected')
                                .text(value.name));
                    } else {
                        $(id)
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.name));
                    }
                });
            } else {
                KTApp.unblock(loader_id);
            }
        },
        error: function (data) {
               var b = jQuery.parseJSON(data.responseText);
               KTApp.unblock(loader_id);
        }
    });
}



function get_state_to_city(type,relation_id="",id,loader_id="",selected_id='') {
    loader_id = $(id).closest(loader_id);
    jQuery(id).empty();
    jQuery(id).html('');
    KTApp.block(loader_id, {
        overlayColor: '#74788d',
        type: 'v2',
        state: 'success'
    });
    jQuery.ajax({
        url: ajax_url+"/get-state",
        type: "GET",
        data: {
            'relation_id': relation_id,
            'type':type,
        },
        success: function(a) {
            jQuery(id).empty();
            jQuery(id).html('');
            var b = JSON.parse(a);
            if (b.success) {
                KTApp.unblock(loader_id);

                $(id).append($("<option></option>")
                .attr("value", "")
                .text("Select"));

                $.each(b.datas, function(index, value) {
                    if (selected_id == value.id) {
                        $(id).append($("<option></option>")
                                .attr("value", value.id)
                                .attr("selected", 'selected')
                                .text(value.name));
                    } else {
                        $(id)
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.name));
                    }
                });
            } else {
                KTApp.unblock(loader_id);
            }
        },
        error: function (data) {
               var b = jQuery.parseJSON(data.responseText);
               KTApp.unblock(loader_id);
        }
    });
}