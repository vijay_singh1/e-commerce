/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');



$('[data-toggle="offcanvas"]').on('click', function () {
  $('.offcanvas-collapse').toggleClass('open')
})