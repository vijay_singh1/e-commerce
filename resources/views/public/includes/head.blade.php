<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link href="{{ URL::asset('public/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::asset('public/assets/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />

	<link href="{{ URL::asset('public/assets/css/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::asset('public/assets/css/popuo-box.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::asset('public/assets/css/jquery-ui1.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::asset('public/assets/css/flexslider.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::asset('public/assets/css/easy-responsive-tabs.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
	<link rel="shortcut icon" href="{{ URL::asset('admin/assets/media/logos/favicon.ico') }}" />
