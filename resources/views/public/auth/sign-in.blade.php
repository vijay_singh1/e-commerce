
  
  <!--

    Wow! What do you want to build?

    You have so many possibilities right now... Looking for something to kick off?
    How about a simple folio page to show off everything you're going to make!
   
  -->
  <div class="container">
    <div class="store-signin-container">
      <div class="store-signin-header mt-5">
        <img class="img-responsive store-logo" src="https://i.imgur.com/nQuieQQ.png"/>
      </div>
      <hr/>
      <div class="store-signin-body">
        <h4>SIGN IN</h4>
        <p class="mt-40">Get Access to your Orders,Waitlist and Recommendations</p>
        <div class="input-box mt-40">
          <div class="email-label">Email Address</div>
          <input class="field-input"  placeholder="Email" type="text" id="store-signin-email" />
        </div>
         <div class="input-box mt-40">
          <div class="password-label">Password</div>
          <input class="field-input" placeholder="Password" type="password" id="store-signin-password" />
        </div>
        <div class="mt-40">
          <input class="store-signin-checkbox" type="checkbox" id="store-signin-loggedin" /> <span>Keep me logged in </span>
        </div>
        <div class="mt-40"> 
          <button class="store-signin-login-box">Log In </button>
        </div>
        <div class="mt-40 footer">
          <p>Don't have an account yet? <a href="#">Sign Up</a></p>
          <p>Need Help? </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Scripts -->
  
