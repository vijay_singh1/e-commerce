<!doctype html>
<html>
<!-- <title>E-commerce - @yield('title')</title> -->
<title>E-commerce</title>
<head>
    @include('public.includes.head')
    @yield('extra-head-style')
</head>
<?php $store_user_data = Session::get('store_user_data'); ?>
<body>
     
            
            @yield('content')
           
        @include('public.includes.footer')

        
    
    @include('public.includes.footer-scripts')
    @yield('extra-footer-scripts')
  
</body>

</html>