@extends('layouts.seller.checkout-template')
<?php  $store_detail = SellerStoreDomainDetails();
$store_theme_details = SellerStoreLogo();
$url = isset($_REQUEST['redirect'])? '?redirect='.$_REQUEST['redirect']:'';
$redirect = isset($_REQUEST['redirect'])? $_REQUEST['redirect']:''; 

$sellers = GetMasterSetting('site_setting');
$default_pages=array();
if (isset($sellers['setting_value']) && !empty($sellers['setting_value'])) {
$default_pages= DefaultPagesUrl($sellers['setting_value']);
}
?>
@section('container_main')


<div class="login_form_set layout-set">
        <form  id="buyer-register-form">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="logo">
                            <img src="<?php echo isset($store_theme_details['logo'])?$store_theme_details['logo']:""; ?>" onerror="buyoneImageNotLoad(this)">
                        </div>
                    </div>

                    <div class="login_form all_address">
                        <div class=" ">

                            <div class="col-lg-12">
                                <h6>Sign Up</h6>
                                <p>Welcome! We are happy to invite you on our store.</p>


                               

                                <div class="form-group row">
                                        <label for="example-text-input" class="col-12">Name</label>
                                        <div class="col-12">
                                                <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                                        </div>
                                    </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-12">Email</label>
                                    <div class="col-12">
                                            <input id="email" type="email" class="form-control" name="email" value="" required>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-12">Password</label>
                                    <div class="col-12">
                                            <input id="password" type="password" class="form-control" name="password" required>
                                            <input id="seller_id" type="hidden" class="form-control" name="seller_id" value="<?php echo isset($store_detail->seller_id)?$store_detail->seller_id:''; ?>">
                                            <input id="store_id" type="hidden" class="form-control" name="store_id" value="<?php echo isset($store_detail->id)?$store_detail->id:''; ?>">
                                            <input id="store_name" type="hidden" class="form-control" name="store_name" value="<?php echo isset($store_detail->store_name)?$store_detail->store_name:''; ?>">
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-12">Confirm Password</label>
                                    <div class="col-12">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  required>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class=" ">
                            <div class="col-12">
                          <button type="submit" class="btn btn-primary order_address">Sign Up</button>
                            </div>
                            <div class="col-lg-12 text-center">
                                <div class="login_footer">
                                    <p><a href="/login<?php echo $url; ?>">Back to login</a></p>
                                </div>
                            </div>

                            <p class="mt-6 sc-bxivhb lhGCii">By clicking Sign up you agree to 
                                <?php echo isset($store_detail->store_name)?$store_detail->store_name:''; ?>
                                 <a class="" target="_blank" href="<?php echo isset($default_pages['page_terms_conditions_buyer'])?$default_pages['page_terms_conditions_buyer']:"#"; ?>">Terms of Service</a>,
                                  <a class="" target="_blank" href="<?php echo isset($default_pages['page_intellectual_property_policy_buyer'])?$default_pages['page_intellectual_property_policy_buyer']:"#"; ?>">Privacy Policy</a> and
                                   <a class="" target="_blank" href="<?php echo isset($default_pages['page_privacy_policy_buyer'])?$default_pages['page_privacy_policy_buyer']:"#"; ?>">Intellectual Property Policy</a>
                                </p>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>


 @stop
 
  @section('scripts')
 <script>
     jQuery("#buyer-register-form").submit(function (a) {
        a.preventDefault();
        jQuery('.preloader-custom-gif').show();
        jQuery('.preloader').show();
        var b = new FormData(this);
      
        if(jQuery("#password").val() != jQuery("#password-confirm").val()){
                jQuery(".alert.alert-danger").show();
                jQuery(".alert.alert-danger span").html("Password does not match!");
                 jQuery('.preloader-custom-gif').hide();
                 jQuery('.preloader').hide();
                setTimeout(function () {
                    jQuery(".alert.alert-danger").hide();
                }, 2000);
            return false;
        }
        jQuery.ajax({
            url: '{!! url('signup-submit') !!}',
            type: "POST",
            contentType: "json",
            data: b,
            contentType: !1,
            processData: !1,
            success: function (a) {
                 var b = JSON.parse(a);
            if(b.success){
                jQuery(".alert.alert-success").show();
                jQuery(".alert.alert-success span").html(b.message);
                 jQuery('.preloader-custom-gif').hide();
                 jQuery('.preloader').hide();
                setTimeout(function () {
                    jQuery(".alert.alert-success").hide();
                    <?php if(!empty($redirect)){ ?>
                    window.location.href = '<?php echo $redirect; ?>';
                   <?php }else{ ?>
                      window.location.href = '/';  
                  <?php } ?>
                }, 2000);
            }else{
              jQuery(".alert.alert-danger").show();
                jQuery(".alert.alert-danger span").html(b.message);
                 jQuery('.preloader-custom-gif').hide();
                 jQuery('.preloader').hide();
                setTimeout(function () {
                    jQuery(".alert.alert-danger").hide();
                }, 2000);  
            }
            }
        });
    });
          </script>    
                 @stop