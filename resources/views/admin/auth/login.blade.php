@extends('admin.layouts.auth-default')
@section('title', 'Login')
<?php

//  $store_detail = SellerStoreDomainDetails();
// $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
// $store_theme_details = SellerStoreLogo();
// $url = isset($_REQUEST['redirect']) ? '?redirect=' . $_REQUEST['redirect'] : '';
 $redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : '';

// $sellers = GetMasterSetting('site_setting');
// $default_pages=array();
// if (isset($sellers['setting_value']) && !empty($sellers['setting_value'])) {
//    $default_pages= DefaultPagesUrl($sellers['setting_value']);
// }
?>
  @section('extra-head-style')
  <link href="{{ URL::asset('admin/assets/css/pages/login/login-6.css') }}" rel="stylesheet" type="text/css" />
  @stop

@section('content')
<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
					<div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
						<div class="kt-login__wrapper">
							<div class="kt-login__container">
								<div class="kt-login__body">
									<div class="kt-login__logo">
										<a href="#">
											<img src="./assets/media/company-logos/logo-2.png">
										</a>
									</div>
									<div class="kt-login__signin">
										<div class="kt-login__head">
											<h3 class="kt-login__title">Sign In To Admin</h3>
										</div>
										<div class="kt-login__form">
											<form class="kt-form" id="admin-login-form">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
												</div>
												<div class="form-group">
													<input class="form-control form-control-last" type="password" placeholder="Password" name="password">
												</div>
												<div class="kt-login__extra">
													<label class="kt-checkbox">
														<input type="checkbox" name="remember"> Remember me
														<span></span>
													</label>
													<a href="javascript:;" id="kt_login_forgot">Forget Password ?</a>
												</div>
												<div class="kt-login__actions">
													<button type="submit" class="btn btn-brand btn-pill btn-elevate">Sign In</button>
												</div>
											</form>
										</div>
									</div>
								
									<div class="kt-login__forgot">
										<div class="kt-login__head">
											<h3 class="kt-login__title">Forgotten Password ?</h3>
											<div class="kt-login__desc">Enter your email to reset your password:</div>
										</div>
										<div class="kt-login__form">
											<form class="kt-form" action="">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
												</div>
												<div class="kt-login__actions">
													<button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Request</button>
													<button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url(./assets/media//bg/bg-4.jpg);">
						<div class="kt-login__section">
							<div class="kt-login__block">
								<h3 class="kt-login__title">Join Our Community</h3>
								<div class="kt-login__desc">
									Lorem ipsum dolor sit amet, coectetuer adipiscing
									<br>elit sed diam nonummy et nibh euismod
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



 @stop

 
  @section('extra-footer-scripts')
 <script>
     jQuery("#admin-login-form").submit(function (a) {

		KTApp.block('#admin-login-form', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: 'Please wait...'
			});
			
		a.preventDefault();
        jQuery('.preloader-custom-gif').show();
        jQuery('.preloader').show();
        var b = new FormData(this);
        jQuery.ajax({
            url: ajax_url+"/admin/login-submit",
            type: "POST",
            contentType: "json",
            data: b,
            contentType: !1,
            processData: !1,
            success: function (a) {
                 var b = JSON.parse(a);
                if(b.success){
                jQuery(".alert.alert-success").show();
                jQuery(".alert.alert-success span").html(b.message);
				
                setTimeout(function () {
					jQuery(".alert.alert-success").hide();
					KTApp.unblock('#admin-login-form');
                    <?php if (!empty($redirect)) {?>
                    window.location.href = '<?php echo $redirect; ?>';
                   <?php } else {?>
                      window.location.href = '/admin/dashboard';
                  <?php }?>
                }, 2000);
            }else{
              jQuery(".alert.alert-danger").show();
                jQuery(".alert.alert-danger span").html(b.message);
                 jQuery('.preloader-custom-gif').hide();
                 jQuery('.preloader').hide();
                setTimeout(function () {
                    jQuery(".alert.alert-danger").hide();
                }, 2000);
            }
            }
        });
    });
</script>
<script src="{{ URL::asset('admin/assets/js/pages/login/login-general.js') }}" type="text/javascript"></script>
@stop