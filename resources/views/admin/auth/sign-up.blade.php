
  <div class="container">
    <div class="store-signin-container">
      <div class="store-signin-header mt-5">
        <img class="img-responsive store-logo" src="https://i.imgur.com/nQuieQQ.png"/>
      </div>
      <hr/>
      <div class="store-signin-b <input class="form-check-input" type="checkbox" name="remember" id="remember">ody">
        <h4>SIGN UP</h4>
        <p class="mt-40">Welcome! We are happy to invite you on our store</p>
        
        <div class="input-box mt-40">
          <div class="email-label">First Name</div>
          <input class="field-input"  placeholder="First name" type="text" id="store-signin-fname" />
        </div>
        
        <div class="input-box mt-40">
          <div class="email-label">Last Name</div>
          <input class="field-input"  placeholder="Last name" type="text" id="store-signin-lname" />
        </div>
        
        <div class="input-box mt-40">
          <div class="email-label">Email Address</div>
          <input class="field-input"  placeholder="Email" type="text" id="store-signin-email" />
        </div>
         <div class="input-box mt-40">
          <div class="password-label">Password</div>
          <input class="field-input" placeholder="Password" type="password" id="store-signin-password" />
        </div>
        
        <div class="input-box mt-40">
          <div class="password-label">Confirm Password</div>
          <input class="field-input" placeholder="Password" type="password" id="store-signin-password" />
        </div>
        
          <div class="input-box mt-40">
          <div class="email-label"> Phone</div>
          <input class="field-input"  placeholder="Enter phone" type="number" id="store-signin-phone" />
        </div>
        
         <div class="mt-40">
          <input class="store-signin-checkbox" type="checkbox" id="store-signin-loggedin" /> <span>I agree terms and conditions </span>
        </div>
        
         <div class="mt-40">
          <input class="store-signin-checkbox" type="checkbox" id="store-signin-loggedin" /> <span>Yes send me newsletter and offers </span>
        </div>
        
        <div class="mt-40"> 
          <button class="store-signin-login-box">Sign Up </button>
        </div>
        <div class="mt-40 text-center"><a href="#">Back to Login</a></div>
      </div>
    </div>
  </div>
