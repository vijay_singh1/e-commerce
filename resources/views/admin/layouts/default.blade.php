<!doctype html>
<html>
<title>E-commerce - @yield('title')</title>
<head>
    @include('admin.includes.head')
    @yield('extra-head-style')
</head>
<?php $store_user_data = Session::get('store_user_data'); ?>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    <div>
    @include('admin.includes.sidebar')  


        @include('admin.includes.header',['user_session_details' => $store_user_data])
            
            @yield('content')
           
        @include('admin.includes.footer')

            </div>
		</div>
    </div>
    
    
    @include('admin.includes.footer-scripts')
    @yield('extra-footer-scripts')
  
</body>

</html>