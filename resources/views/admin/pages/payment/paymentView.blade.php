@extends('admin.layouts.default')
 @section('title', 'Payment')
   @section('extra-head-style')
   <!-- <link href="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" /> -->
   @stop

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
   
   <div class="kt-subheader  kt-grid__item" id="kt_subheader">
      <div class="kt-container  kt-container--fluid ">
         <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Reports</h3>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
               <input type="text" class="form-control" placeholder="Search order..."
                  id="generalSearch">
               <span class="kt-input-icon__icon kt-input-icon__icon--right">
               <span><i class="flaticon2-search-1"></i></span>
               </span>
            </div>
         </div>
      </div>
   </div>
   
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-bottom: 50px;">
<ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{!! url('/admin/dashboard') !!}">Dashboard</a></li>
         <li class="breadcrumb-item active" aria-current="page">Payment</li>
      </ol>
      </nav>
   <div class="kt-portlet kt-portlet--mobile">
     
      <div class="kt-portlet__body">
      <h3 class="head text-center">Payment</h3>
      <img src="{{ URL::asset('admin/assets/media/upcoming.png') }}">
      </div>
   </div>
</div>
@stop

@section('extra-footer-scripts')
<!-- <script src="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/js/pages/banner/banner-list.js') }}" type="text/javascript"></script> -->
@stop