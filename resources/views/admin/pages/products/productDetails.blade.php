@extends('admin.layouts.default')
 @section('title', 'Product Details')
   @section('extra-head-style')
   <link href="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
   @stop

@section('content')

<input type="hidden" value="<?php echo isset($_REQUEST['pid'])?$_REQUEST['pid']:""; ?>" id="pid-data">
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-bottom: 50px;">
<div class="tab-panel">

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{!! url('/admin/dashboard') !!}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{!! url('/admin/products') !!}">Products</a></li>
    <li class="breadcrumb-item active" aria-current="page">Product Details</li>
  </ol>
</nav>
         <div class="board">
            <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
            <div class="board-inner">
               <ul class="nav nav-tabs" id="myTab">
                  <div class="liner"></div>
                  <li class="active">
                     <a href="#basic-information" data-toggle="tab" title="Basic Information" >
                     <span class="round-tabs one">
                     <i class="glyphicon glyphicon-th"></i>
                     </span> 
                     </a>
                  </li>
                  <li><a href="#variations" data-toggle="tab" title="Variations">
                     <span class="round-tabs two">
                     <i class="glyphicon glyphicon-retweet"></i>
                     </span> 
                     </a>
                  </li>
                  <li><a href="#product-description" data-toggle="tab" title="Product Description">
                     <span class="round-tabs three">
                     <i class="glyphicon glyphicon-random"></i>
                     </span> </a>
                  </li>
                  <li><a href="#additional-info" data-toggle="tab" title="Additional Info">
                     <span class="round-tabs four">
                     <i class="glyphicon glyphicon-open-file"></i>
                     </span> 
                     </a>
                  </li>
                 
               </ul>
            </div>
            <div class="tab-content">
               <div class="basic-info-tab tab-pane fade in active show" id="basic-information">
               <div class="card">
		
				<div class="row">
					<div class="preview col-md-6">
						
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"></div>
						  <!-- <div class="tab-pane" id="pic-2"><img src="http://placekitten.com/400/252" /></div> -->
						</div>
						<!-- <ul class="preview-thumbnail nav nav-tabs">
						  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						</ul> -->
						
					</div>
					<div class="details col-md-6">
                  <h3 class="product-title" id="product-title">men's shoes fashion</h3>
                  <h4 class="price">SKU: <span id="supplier-sku">12343</span></h4>
						<div class="rating">
							<div class="stars">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
							<span class="review-no">41 reviews</span>
                  </div>
                  <h4 class="price">Category: <span id="category">Apparel</span></h4>
						<p class="product-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
						<h4 class="price">current price: <span>$10</span></h4>
						
					
						
					</div>
				</div>
			
		       </div>
               </div>
               <div class="tab-pane fade in " id="variations">
                 
               <h3 class="head text-center">variations</h3>
               <img src="{{ URL::asset('admin/assets/media/upcoming.png') }}">

               </div>
               <div class="tab-pane fade in " id="product-description">
               <h3 class="head text-center">Product Description</h3>
                  <img src="{{ URL::asset('admin/assets/media/upcoming.png') }}">

               </div>
              
               <div class="tab-pane fade in " id="additional-info">
               <h3 class="head text-center">Additional Information</h3>
               <img src="{{ URL::asset('admin/assets/media/upcoming.png') }}">


               </div>
               </div>
               
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop

@section('extra-footer-scripts')
<script src="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/js/pages/products/products-details.js') }}" type="text/javascript"></script>
@stop