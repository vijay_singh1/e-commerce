@extends('admin.layouts.default')
 @section('title', 'Banner Details')
   @section('extra-head-style')
   <link href="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
   @stop

@section('content')

<input type="hidden" value="<?php echo isset($_REQUEST['id'])?$_REQUEST['id']:""; ?>" id="catid-data">
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-bottom: 50px;">
<div class="tab-panel">

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{!! url('/admin/dashboard') !!}">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{!! url('/admin/banner') !!}">Banner</a></li>
    <li class="breadcrumb-item active" aria-current="page">Banner Details</li>
  </ol>
</nav>
         <div class="board">
            <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
            <div class="board-inner">
               <ul class="nav nav-tabs" id="myTab">
                  <div class="liner"></div>
                  <li class="active">
                     <a href="#basic-information" data-toggle="tab" title="Basic Information" >
                     <span class="round-tabs one">
                     <i class="glyphicon glyphicon-th"></i>
                     </span> 
                     </a>
                  </li>
                  
               </ul>
            </div>
            <div class="tab-content">
            <div class="tab-pane fade in active show" id="basic-information">
                 
                 <h3 class="head text-center">Basic Information</h3>
                 <img src="{{ URL::asset('admin/assets/media/upcoming.png') }}">
  
                 </div>
               
               </div>
               
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop

@section('extra-footer-scripts')
<script src="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/js/pages/banner/banner-details.js') }}" type="text/javascript"></script>
@stop