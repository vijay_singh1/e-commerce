@extends('admin.layouts.default')
 @section('title', 'Banner')
   @section('extra-head-style')
   <link href="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
   @stop

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
   
   <div class="kt-subheader  kt-grid__item" id="kt_subheader">
      <div class="kt-container  kt-container--fluid ">
         <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Banner</h3>
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
               <input type="text" class="form-control" placeholder="Search order..."
                  id="generalSearch">
               <span class="kt-input-icon__icon kt-input-icon__icon--right">
               <span><i class="flaticon2-search-1"></i></span>
               </span>
            </div>
         </div>
      </div>
   </div>
   <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" id="hide-show-form" style="display:none;">
      <div class="row">
         
         <div class="col-lg-12">

         <nav aria-label="breadcrumb">


            <div class="kt-portlet">
               <div class="kt-portlet__head">
                  <div class="kt-portlet__head-label">
                     <h3 class="kt-portlet__head-title">
                        Save Banner
                     </h3>
                  </div>
               </div>
               <form class="kt-form kt-form--label-right" id="form-submit">
                  <div class="kt-portlet__body">
                     <div class="form-group row">
                        <div class="col-lg-4">
                           <label></label>
						   <input type="text" name="name" id="name" class="form-control" placeholder="Enter full name" required="required">
						   <input type="hidden" name="id" id="id">
                        </div>
                        <div class="col-lg-4 button-position">
                           <button type="submit" class="btn btn-primary">Submit</button>
                           <button type="button" class="btn btn-secondary hide-show-button">Cancel</button>
                        </div>
                     </div>
                  </div>
                  <div class="kt-portlet__foot"></div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-bottom: 50px;">
<ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{!! url('/admin/dashboard') !!}">Dashboard</a></li>
         <li class="breadcrumb-item active" aria-current="page">Banner</li>
      </ol>
      </nav>
   <div class="kt-portlet kt-portlet--mobile">
      <div class="kt-portlet__head kt-portlet__head--lg">
         <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
            <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
               List of Banner
            </h3>
         </div>
         <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
               <div class="kt-portlet__head-actions">
                  &nbsp;
                  <a href="javascript:void(0)" class="btn btn-brand btn-elevate btn-icon-sm hide-show-button">
                  <i class="la la-plus"></i>
                  New Record
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="kt-portlet__body">
         <table class="table table-striped- table-bordered table-hover table-checkable" id="table-init">
            <thead>
               <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Location</th>
              <th>Status</th>
               </tr>
            </thead>
         </table>
      </div>
   </div>
</div>
@stop

@section('extra-footer-scripts')
<script src="{{ URL::asset('admin/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('admin/assets/js/pages/banner/banner-list.js') }}" type="text/javascript"></script>
@stop